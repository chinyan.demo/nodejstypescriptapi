"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const aql_1 = require("arangojs/aql");
const _1 = __importDefault(require("."));
class ArangoRepository {
    constructor(collection) {
        this.collection = collection;
    }
    /**
     * create
     * @param {*} doc
     * @param {Transaction} txn
     * @returns {Promise<ICreate>}
     */
    create(doc, txn) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield _1.default.getInstance();
                if (txn) {
                    return yield txn.step(() => __awaiter(this, void 0, void 0, function* () {
                        const res = yield db.collection(this.collection).save(doc);
                        return { res, err: undefined };
                    }));
                }
                else {
                    const res = yield db.collection(this.collection).save(doc);
                    return { res, err: undefined };
                }
            }
            catch (err) {
                return { res: undefined, err };
            }
        });
    }
    /**
     * fetch
     * @param {string} docID
     * @param {Transaction} txn
     * @returns {Promise<IFetch>}
     */
    fetch(docID, txn) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield _1.default.getInstance();
                if (txn) {
                    return yield txn.step(() => __awaiter(this, void 0, void 0, function* () {
                        const doc = yield db
                            .collection(this.collection)
                            .document({ _id: docID });
                        return { res: doc, err: undefined };
                    }));
                }
                else {
                    const doc = yield db
                        .collection(this.collection)
                        .document({ _id: docID });
                    return { res: doc, err: undefined };
                }
            }
            catch (err) {
                return { res: undefined, err };
            }
        });
    }
    /**
     * update
     * @param {string} docID
     * @param {*} doc
     * @param {Transaction} txn
     * @returns {Promise<IUpdate>}
     */
    update(docID, doc, txn) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield _1.default.getInstance();
                if (txn) {
                    return yield txn.step(() => __awaiter(this, void 0, void 0, function* () {
                        const res = yield db
                            .collection(this.collection)
                            .update({ _id: docID }, doc);
                        return { res, err: undefined };
                    }));
                }
                else {
                    const res = yield db
                        .collection(this.collection)
                        .update({ _id: docID }, doc);
                    return { res, err: undefined };
                }
            }
            catch (err) {
                return { res: undefined, err };
            }
        });
    }
    /**
     * delete
     * @param {string} docID
     * * @param {Transaction} txn
     * @returns {Promise<IDelete>}
     */
    delete(docID, txn) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield _1.default.getInstance();
                if (txn) {
                    return yield txn.step(() => __awaiter(this, void 0, void 0, function* () {
                        const res = yield db
                            .collection(this.collection)
                            .remove({ _id: docID });
                        return { res, err: undefined };
                    }));
                }
                else {
                    const res = yield db
                        .collection(this.collection)
                        .remove({ _id: docID });
                    return { res, err: undefined };
                }
            }
            catch (err) {
                return { res: undefined, err };
            }
        });
    }
    /**
    * deleteByFilter
    * @param {{[key:string]:any}} filter
    * * @param {Transaction} txn
    * @returns {Promise<IDelete>}
    */
    deleteByFilter(filter, txn) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield _1.default.getInstance();
                const keys = Object.keys(filter);
                if (keys.length == 0)
                    throw "filter is required";
                const collection = db.collection(this.collection);
                const filterQuery = keys.reduce((acc, next) => acc + `FILTER c.${next} == "${filter[next]}" `, "");
                const query = (0, aql_1.aql) `
                FOR c IN ${collection}
                {{filterQuery}}
                REMOVE c IN ${collection} 
                RETURN OLD
            `;
                query.query = query.query.replace(/{{filterQuery}}/, filterQuery);
                if (keys.length == 0)
                    throw "filter is required";
                if (txn) {
                    return yield txn.step(() => __awaiter(this, void 0, void 0, function* () {
                        const res = yield (yield db.query(query)).all();
                        return { res, err: undefined };
                    }));
                }
                else {
                    const res = yield (yield db.query(query)).all();
                    return { res, err: undefined };
                }
            }
            catch (err) {
                return { res: undefined, err };
            }
        });
    }
    /**
     * query
     * @param {string|AqlLiteral} query
     * @param {Transaction} txn
     * @returns {Promise<IQuery>}
     */
    query(query, txn) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield _1.default.getInstance();
                if (txn) {
                    return yield txn.step(() => __awaiter(this, void 0, void 0, function* () {
                        const crusor = yield db.query(query);
                        const res = yield crusor.all();
                        return { res, err: undefined };
                    }));
                }
                else {
                    const crusor = yield db.query(query);
                    const res = yield crusor.all();
                    return { res, err: undefined };
                }
            }
            catch (err) {
                // txn?.abort()
                return { res: undefined, err };
            }
        });
    }
    /**
     * fetchMany
     * @param {string|AqlLiteral} query
     * @param {Transaction} txn
     * @returns {Promise<IQuery>}
     */
    fetchMany(txn) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield _1.default.getInstance();
                const collection = db.collection(this.collection);
                const string = yield (0, aql_1.aql) `
                        FOR c in ${collection}
                        RETURN c
                    `;
                if (txn) {
                    return yield txn.step(() => __awaiter(this, void 0, void 0, function* () {
                        const crusor = yield db.query(string);
                        const res = yield crusor.all();
                        return { res, err: undefined };
                    }));
                }
                else {
                    const crusor = yield db.query(string);
                    const res = yield crusor.all();
                    return { res, err: undefined };
                }
            }
            catch (err) {
                // txn?.abort()
                return { res: undefined, err };
            }
        });
    }
}
exports.default = ArangoRepository;
