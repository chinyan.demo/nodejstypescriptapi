"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const collection_enum_1 = require("../../../collection.enum");
const FriedshipGraph = {
    name: "friendship",
    edgeDefinition: [{
            collection: collection_enum_1.EDGE_COLLECTION.FRIENDS,
            from: collection_enum_1.COLLECTION.PROFILES,
            to: collection_enum_1.COLLECTION.PROFILES,
        }]
};
exports.default = FriedshipGraph;
