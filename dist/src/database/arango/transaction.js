"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useTransaction = exports.getTransaction = void 0;
const _1 = __importDefault(require("."));
const getTransaction = (collections) => __awaiter(void 0, void 0, void 0, function* () {
    const db = yield _1.default.getInstance();
    const txn = yield db.beginTransaction(collections);
    return txn;
});
exports.getTransaction = getTransaction;
const useTransaction = (collections, callback) => __awaiter(void 0, void 0, void 0, function* () {
    const txn = yield (0, exports.getTransaction)(collections);
    try {
        // console.table({ txn });
        const { res, err } = yield callback(txn);
        if (err)
            throw err;
        yield txn.commit();
        return { res };
    }
    catch (error) {
        // console.table({ error, status: (await txn.get()).status });
        yield txn.abort();
        return { err: error };
    }
});
exports.useTransaction = useTransaction;
