"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const arangojs_1 = require("arangojs");
const config_1 = __importDefault(require("../../constant/config"));
let instance;
const url = String(config_1.default.ARANGODB_URL);
const databaseName = String(config_1.default.ARANGODB_DBNAME);
const username = String(config_1.default.ARANGODB_USERNAME);
const password = String(config_1.default.ARANGODB_PASSWORD);
class ArangoDB {
    constructor(url, databaseName, username, password) {
        this.credential = { url, auth: { username, password } };
        this.databaseName = databaseName;
    }
    getInstance() {
        return __awaiter(this, void 0, void 0, function* () {
            let db;
            if (!instance) {
                const tempDB = new arangojs_1.Database(this.credential);
                const allDBs = yield tempDB.listDatabases();
                if (!allDBs.find(name => name == this.databaseName)) {
                    db = yield tempDB.createDatabase(this.databaseName);
                }
                else {
                    db = tempDB.database(this.databaseName);
                }
                instance = db;
                // Object.freeze(instance);
            }
            return instance;
        });
    }
    setupCollections(collections, edge_collections) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const db = yield this.getInstance();
                for (const collection of collections) {
                    try {
                        yield db.collection(collection).get();
                    }
                    catch (notExistException) {
                        yield db.createCollection(collection);
                    }
                }
                for (const edge_collection of edge_collections) {
                    try {
                        yield db.collection(edge_collection).get();
                    }
                    catch (notExistException) {
                        yield db.createEdgeCollection(edge_collection);
                    }
                }
            }
            catch (error) {
                console.log(JSON.stringify(error, undefined, 2));
            }
        });
    }
    createSearchView(searchViews) {
        return __awaiter(this, void 0, void 0, function* () {
            const db = yield this.getInstance();
            for (const searchView of searchViews) {
                yield db.createView(searchView, { type: "arangosearch" });
            }
        });
    }
    createGraph(graphs) {
        return __awaiter(this, void 0, void 0, function* () {
            const db = yield this.getInstance();
            for (const graph of graphs) {
                if (graph) {
                    const { name, edgeDefinition } = graph;
                    if (!(yield db.graph(name).exists()))
                        yield db.createGraph(name, edgeDefinition);
                }
            }
        });
    }
    seedData(seedMatrices) {
        return __awaiter(this, void 0, void 0, function* () {
            const db = yield this.getInstance();
            for (const seedMatrix of seedMatrices) {
                const { collection, data } = seedMatrix;
                const col = db.collection(collection);
                for (const doc of data) {
                    yield col.save(doc);
                }
            }
        });
    }
}
const arangoDB = new ArangoDB(url, databaseName, username, password);
exports.default = arangoDB;
