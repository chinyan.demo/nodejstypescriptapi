"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.seedData = exports.setupCollection = void 0;
const collection_enum_1 = require("./collection.enum");
const index_1 = __importDefault(require("./index"));
const graph_1 = require("./seed/dev/graph");
const setupCollection = () => __awaiter(void 0, void 0, void 0, function* () {
    const collection = Object.values(collection_enum_1.COLLECTION);
    const edge = Object.values(collection_enum_1.EDGE_COLLECTION);
    yield index_1.default.setupCollections(collection, edge);
    const graphDefs = yield (0, graph_1.getGraphDef)();
    yield index_1.default.createGraph(graphDefs);
});
exports.setupCollection = setupCollection;
const seedData = (data) => __awaiter(void 0, void 0, void 0, function* () {
    if (data) {
        const db = yield index_1.default.getInstance();
        const { collection, documents } = data;
        if (collection) {
            const col = db.collection(collection);
            if (documents && documents.length > 0) {
                documents.forEach((doc) => __awaiter(void 0, void 0, void 0, function* () {
                    if (doc._id && doc._key) {
                        try {
                            // const query = aql`
                            // UPSERT { _key: ${doc._key} }
                            // INSERT ${ doc }
                            // UPDATE ${ doc }
                            // IN ${col} OPTIONS { keepNull: false }`;
                            // const res = await db.query(query)
                            // await res.all();
                            if ((yield col.documentExists(doc._key)) == false) {
                                yield col.save(doc);
                            }
                        }
                        catch (error) {
                            console.log(error);
                            throw error;
                        }
                    }
                }));
            }
        }
    }
});
exports.seedData = seedData;
