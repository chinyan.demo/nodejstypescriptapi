"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EDGE_COLLECTION = exports.COLLECTION = void 0;
var COLLECTION;
(function (COLLECTION) {
    COLLECTION["PROFILES"] = "profiles";
    COLLECTION["ACCOUNTS"] = "accounts";
    COLLECTION["POSTS"] = "posts";
    COLLECTION["SETTING"] = "settings";
    COLLECTION["CONVERSATIONS"] = "conversations";
    COLLECTION["MESSAGES"] = "messages";
})(COLLECTION || (exports.COLLECTION = COLLECTION = {}));
var EDGE_COLLECTION;
(function (EDGE_COLLECTION) {
    EDGE_COLLECTION["FRIENDS"] = "friends";
})(EDGE_COLLECTION || (exports.EDGE_COLLECTION = EDGE_COLLECTION = {}));
