"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getRouteMatricesV1 = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const getRouteMatricesV1 = () => {
    const v1Path = path_1.default.resolve(__dirname, "./v1.0");
    const routeMatricesV1 = [];
    const v1Dir = fs_1.default.readdirSync(v1Path);
    v1Dir.forEach((dir) => __awaiter(void 0, void 0, void 0, function* () {
        const files = fs_1.default.readdirSync(path_1.default.resolve(__dirname, `./v1.0/${dir}`));
        files.forEach((file) => __awaiter(void 0, void 0, void 0, function* () {
            if (/.routes./.test(file)) {
                let routerPath = path_1.default.resolve(__dirname, `./v1.0/${dir}/${file}`);
                const routeMatrices = require(routerPath)["default"];
                routeMatricesV1.push(...routeMatrices);
            }
        }));
    }));
    return routeMatricesV1;
};
exports.getRouteMatricesV1 = getRouteMatricesV1;
