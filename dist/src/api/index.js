"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RouterV1 = void 0;
const koa_router_1 = __importDefault(require("koa-router"));
const routeMatrices_1 = require("./routeMatrices");
const http_constant_1 = require("../constant/http.constant");
const cryptography_1 = require("../helper/cryptography");
const repository_1 = __importDefault(require("../database/arango/repository"));
const transaction_1 = require("../database/arango/transaction");
const JwtValidator = (ctx, next) => {
    try {
        const bearerToken = ctx.request.headers["authorization"];
        if (!bearerToken)
            throw "Authorization token is required";
        const BearerRegexp = new RegExp(/Bearer /);
        if (!BearerRegexp.test(bearerToken))
            throw "invalid token header";
        const token = bearerToken.replace(/Bearer /, "").replace(/ /, "");
        if (!token || token.length == 0)
            throw "token is required";
        const { res: JwtData, err: JwtErr } = (0, cryptography_1.validateJWT)(token);
        if (JwtErr)
            throw "token mal-form";
        const { iat, clientIP } = JwtData;
        if ((iat - new Date().getTime()) <= 0)
            throw "token expired";
        const xForwardedFor = ctx.request.headers['x-forwarded-for'];
        const clientIPAdrs = (typeof xForwardedFor == "undefined") ? ctx.socket.remoteAddress : (Array.isArray(xForwardedFor)) ? xForwardedFor.shift() : xForwardedFor;
        if (clientIP != clientIPAdrs)
            throw "invalid clientIP"; // create security alert
        // console.log(JwtData)
        return { jwtData: JwtData, err: undefined };
    }
    catch (error) {
        return { jwtData: undefined, err: error };
    }
};
const RolePermissionValidator = (jwtData, permission) => {
    try {
        if (permission.length == 0)
            return { err: undefined };
        if (!permission.find(doc => doc == jwtData.role))
            throw "invalid role";
        return { err: undefined };
    }
    catch (err) {
        return { err };
    }
};
const DataAccessControl = (jwtData, payload, access, ctx) => {
    try {
        if (access.length == 0)
            return { err: undefined };
        for (const acc of access) {
            if (!("payloadPath" in acc) && !("fetched" in acc))
                throw "invalid access(Backed not checking)";
            const { tokenPath } = acc;
            const tokenPremises = tokenPath.split(".");
            let jwtDataToBeChecked = JSON.parse(JSON.stringify(jwtData));
            tokenPremises.forEach(premise => { jwtDataToBeChecked = jwtDataToBeChecked[premise]; });
            if (("payloadPath" in acc) && (!acc.payloadPath || acc.payloadPath.length <= 0)) {
                const { payloadPath } = acc;
                if (payloadPath) {
                    const payloadPremises = payloadPath.split(".");
                    let payloadToBeChecked = JSON.parse(JSON.stringify(payload));
                    payloadPremises.forEach(premise => { payloadToBeChecked = payloadToBeChecked[premise]; });
                    if (!jwtDataToBeChecked || !payloadToBeChecked)
                        throw "invalid access(Backed config error)";
                    // if(jwtDataToBeChecked != jwtDataToBeChecked) throw "invalid access";
                    if (Array.isArray(jwtDataToBeChecked)) {
                        const found = jwtDataToBeChecked.find(payloadData => payloadData == payloadToBeChecked);
                        if (!found)
                            throw "invalid access";
                    }
                    else {
                        console.table({ jwtDataToBeChecked, payloadToBeChecked });
                        if (jwtDataToBeChecked != payloadToBeChecked)
                            throw "invalid access";
                    }
                }
            }
            else if (("fetched" in acc) && acc.fetched) {
                const { fetched } = acc;
                if (fetched) {
                    const { path, fetchedDataCollection } = fetched;
                    const fetchDataContext = JSON.parse(JSON.stringify(ctx.fetched));
                    const fetchDoc = fetchDataContext.filter(data => new RegExp(fetchedDataCollection, "i").test(data["_id"]))[0];
                    const fetchedPremises = path.split(".");
                    let fetchedDataToBeChecked = JSON.parse(JSON.stringify(fetchDoc));
                    fetchedPremises.forEach(premise => { fetchedDataToBeChecked = fetchedDataToBeChecked[premise]; });
                    if (Array.isArray(fetchedDataToBeChecked)) {
                        const found = fetchedDataToBeChecked.find(fetchedData => fetchedData == jwtDataToBeChecked);
                        if (!found)
                            throw "invalid access";
                    }
                    else {
                        if (fetchedDataToBeChecked != jwtDataToBeChecked)
                            throw "invalid access";
                    }
                    if (!jwtDataToBeChecked || !fetchedDataToBeChecked)
                        throw "invalid access(Backed config error)";
                }
            }
        }
        return { err: undefined };
    }
    catch (error) {
        return { err: error };
    }
};
const IdValidator = (payload) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const fetched = [];
        const fetchedError = [];
        const IdRegExp = new RegExp(/.*(id|ids)$/, "i");
        const keys = Object.keys(payload);
        for (const key of keys) {
            if (IdRegExp.test(key)) {
                try {
                    if (typeof payload[key] == "string") {
                        let collection = payload[key].split("/")[0];
                        const repo = new repository_1.default(collection);
                        const { res: fetchedObj, err: fetchErr } = yield repo.fetch(payload[key]);
                        if (fetchErr)
                            throw fetchErr;
                        fetched.push(fetchedObj);
                    }
                    else if (typeof payload[key] == "object" && payload[key].length > 0) {
                        payload[key].forEach((id) => __awaiter(void 0, void 0, void 0, function* () {
                            let collection = id.split("/")[0];
                            const repo = new repository_1.default(collection);
                            const { res: fetchedObj, err: fetchErr } = yield repo.fetch(id);
                            if (fetchErr)
                                throw fetchErr;
                            fetched.push(fetchedObj);
                        }));
                    }
                }
                catch (error) {
                    fetchedError.push(`invalid ${key} ${payload[key]}`);
                }
            }
        }
        if (fetchedError.length > 0)
            throw fetchedError;
        return { fetched, err: undefined };
    }
    catch (err) {
        return { fetched: [], err };
    }
});
const createMiddleware = (permission, validator, access, transactions) => {
    return (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            //JWT validation
            if (permission.useJwt) {
                const { jwtData, err: JwtValidationErr } = JwtValidator(ctx, next);
                if (JwtValidationErr)
                    throw JwtValidationErr;
                ctx.jwtData = jwtData;
                //Role Permission Validation
                const { err: rolePermissionErr } = RolePermissionValidator(jwtData, permission.roles);
                if (rolePermissionErr)
                    throw rolePermissionErr;
            }
            //Payload Validation
            const body = ctx.request.body;
            const query = ctx.query;
            const param = ctx.params;
            const payload = (Object.keys(body).length != 0) ? body : (Object.keys(query).length != 0) ? ctx.query : param;
            const { value: validatedPayload, error: validatedError } = validator.validate(payload);
            if (validatedError) {
                ctx.status = 400;
                return ctx.body = { success: false, errorObject: validatedError };
            }
            // fetch If ID exist
            const { fetched, err: fetchedErr } = yield IdValidator(payload);
            if (fetchedErr)
                throw fetchedErr;
            if (fetched)
                ctx.fetched = fetched;
            if (transactions && transactions.length > 0) {
                const txn = yield (0, transaction_1.getTransaction)(transactions);
                ctx.txn = txn;
            }
            //Data Access Control 
            if (permission.useJwt) {
                const { err: accessControlErr } = DataAccessControl(ctx.jwtData, payload, access, ctx);
                if (accessControlErr)
                    throw accessControlErr;
            }
            // ctx.set("validatedPayload",validatedPayload)
            // ctx.response.set("validatedPayload",validatedPayload)
            ctx.validatedPayload = validatedPayload;
            yield next();
        }
        catch (error) {
            ctx.status = 401;
            return ctx.body = { success: false, errorObject: error };
        }
    });
};
const responseHandler = (handler) => {
    return (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
        const txn = ctx.txn;
        const { res, err } = yield handler(ctx, next);
        if (err) {
            if (txn)
                yield txn.abort();
            ctx.status = 500;
            ctx.body = { success: false, errorObject: err };
        }
        if (res) {
            if (txn)
                yield txn.commit();
            ctx.body = { success: true, status: "OK", error: null, data: res };
        }
    });
};
const RouterV1 = (app) => __awaiter(void 0, void 0, void 0, function* () {
    const routerV1 = new koa_router_1.default();
    const routeMatricesV1 = (0, routeMatrices_1.getRouteMatricesV1)();
    routeMatricesV1.forEach((routeMatrix) => __awaiter(void 0, void 0, void 0, function* () {
        const { method, path, permission, validator, controller, transactions, access } = routeMatrix;
        let middleware = createMiddleware(permission, validator, access, transactions);
        const endpoint = `/v1.0${path}`;
        // console.log({[method]:endpoint})
        switch (method) {
            case http_constant_1.HTTP.GET:
                routerV1.get(endpoint, middleware, responseHandler(controller));
                break;
            case http_constant_1.HTTP.POST:
                routerV1.post(endpoint, middleware, responseHandler(controller));
                break;
            case http_constant_1.HTTP.PUT:
                routerV1.put(endpoint, middleware, responseHandler(controller));
                break;
            case http_constant_1.HTTP.DELETE:
                routerV1.delete(endpoint, middleware, responseHandler(controller));
                break;
            default:
                routerV1.all(endpoint, middleware, responseHandler(controller));
        }
    }));
    app.use(routerV1.routes());
});
exports.RouterV1 = RouterV1;
