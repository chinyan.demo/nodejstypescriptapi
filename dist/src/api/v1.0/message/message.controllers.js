"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.listOnlineUserHandler = exports.updateMessageReadBy = exports.listMessageByConversationID = exports.createMessageHandler = exports.createTestMessageHandler = void 0;
const client_1 = require("../../../helper/websocket/client");
const topics_1 = require("../../../helper/websocket/topics");
const onlineUser_1 = __importDefault(require("../../../helper/websocket/handlers/onlineUser"));
const message_1 = __importDefault(require("../../../repository/message"));
const conversation_1 = __importDefault(require("../../../repository/conversation"));
const document_1 = require("../../../helper/document");
const collection_enum_1 = require("../../../database/arango/collection.enum");
const createTestMessageHandler = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payload = ctx.validatedPayload;
        const { receiverProfileID } = payload;
        const { success, error } = (0, client_1.serverWebsocketSend)(topics_1.TOPIC.PRIVATE_MESSAGE, receiverProfileID, payload);
        if (!success)
            throw error;
        return { res: "message created", err: undefined };
    }
    catch (error) {
        return { res: undefined, err: error };
    }
});
exports.createTestMessageHandler = createTestMessageHandler;
const createMessageHandler = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const txn = ctx.txn;
        const message = ctx.validatedPayload;
        const { conversationID, senderProfileID, content } = message;
        const fetched = ctx.fetched;
        const conversation = fetched.find((doc) => doc._id == conversationID);
        const senderProfile = fetched.find((doc) => doc._id == senderProfileID);
        const receiverProfileIDs = conversation === null || conversation === void 0 ? void 0 : conversation.profileIDs.filter(pid => pid != senderProfileID);
        message.senderName = senderProfile.name;
        const { res: createMessageRes, err: createMessageErr } = yield message_1.default.create((0, document_1.docHelper)(message, collection_enum_1.COLLECTION.MESSAGES, senderProfileID), txn);
        if (createMessageErr)
            throw createMessageErr;
        message.messageID = createMessageRes === null || createMessageRes === void 0 ? void 0 : createMessageRes._id;
        if (!conversation.unreadMessagesCount)
            conversation.unreadMessagesCount = {};
        const unreadUsers = conversation.profileIDs.filter((profileID) => profileID != senderProfile._id);
        unreadUsers.forEach(profileID => {
            if (!conversation.unreadMessagesCount[profileID])
                conversation.unreadMessagesCount[profileID] = 0;
            conversation.unreadMessagesCount[profileID]++;
        });
        message.unreadMessagesCount = conversation.unreadMessagesCount;
        const { res: updateConversationRes, err: updateConversationErr } = yield conversation_1.default.update(conversationID, { lastMessage: { sender: senderProfile.name, text: content, datetime: new Date().toISOString() }, unreadMessagesCount: conversation.unreadMessagesCount }, txn);
        if (updateConversationErr)
            throw updateConversationErr;
        const errorStack = [];
        for (const receiverProfileID of receiverProfileIDs) {
            const { success, error } = (0, client_1.serverWebsocketSend)(topics_1.TOPIC.FORWARD_PRIVATE_MESSAGE, receiverProfileID, message);
            if (!success)
                errorStack.push(error);
        }
        return { res: { message: createMessageRes, conversation: updateConversationRes }, err: undefined, info: errorStack };
    }
    catch (error) {
        return { res: undefined, err: error };
    }
});
exports.createMessageHandler = createMessageHandler;
const listMessageByConversationID = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { conversationID, pagination } = ctx.validatedPayload;
        const { res, err } = yield message_1.default.listMessageByConversationID(conversationID, pagination);
        if (err)
            throw err;
        return { res, err: undefined };
    }
    catch (error) {
        return { res: undefined, err: error };
    }
});
exports.listMessageByConversationID = listMessageByConversationID;
const updateMessageReadBy = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const txn = ctx.txn;
        const payload = ctx.validatedPayload;
        const { conversationID, profileID } = payload;
        const fetched = ctx.fetched;
        const conversation = fetched.find((doc) => doc._id == conversationID);
        if (conversation.unreadMessagesCount && conversation.unreadMessagesCount[profileID]) {
            conversation.unreadMessagesCount[profileID] = 0;
            conversation.updatedBy = profileID;
            conversation.updatedAt = new Date().toISOString();
            const { err: updateConversationErr } = yield conversation_1.default.update(conversationID, conversation, txn);
            if (updateConversationErr)
                throw updateConversationErr;
        }
        const { res, err } = yield message_1.default.updateReadBy(conversationID, profileID, txn);
        if (err)
            throw err;
        return { res, err: undefined };
    }
    catch (error) {
        return { err: error, res: undefined };
    }
});
exports.updateMessageReadBy = updateMessageReadBy;
const listOnlineUserHandler = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payload = ctx.validatedPayload;
        const { profileID } = payload;
        const { success, error } = (0, client_1.serverWebsocketSend)(topics_1.TOPIC.ONLINE, profileID, onlineUser_1.default.getAllOnlineUser());
        if (!success)
            throw error;
        return { res: onlineUser_1.default.getAllOnlineUser(), err: undefined };
    }
    catch (error) {
        return { res: undefined, err: error };
    }
});
exports.listOnlineUserHandler = listOnlineUserHandler;
