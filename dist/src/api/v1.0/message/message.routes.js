"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_constant_1 = require("../../../constant/http.constant");
const message_controllers_1 = require("./message.controllers");
const message_validators_1 = require("./message.validators");
const permission_1 = require("../../../constant/permission");
const collection_enum_1 = require("../../../database/arango/collection.enum");
const createRes_json_1 = __importDefault(require("../../../helper/swagger/data/messages/createRes.json"));
const listRes_json_1 = __importDefault(require("../../../helper/swagger/data/messages/listRes.json"));
const listRes_json_2 = __importDefault(require("../../../helper/swagger/data/messages/listRes.json"));
const MessageRouteMatrices = [
    {
        method: http_constant_1.HTTP.POST,
        path: "/message/test/create",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: true },
        access: [{ tokenPath: "_id", payloadPath: "senderProfileID" }],
        validator: message_validators_1.createTestMessageReq,
        controller: message_controllers_1.createTestMessageHandler,
        description: "test web socket message db no store",
    }, {
        method: http_constant_1.HTTP.POST,
        path: "/message/create",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: true },
        transactions: [collection_enum_1.COLLECTION.CONVERSATIONS, collection_enum_1.COLLECTION.MESSAGES],
        access: [{ tokenPath: "_id", payloadPath: "senderProfileID" }],
        validator: message_validators_1.createMessageReq,
        controller: message_controllers_1.createMessageHandler,
        description: "create messages",
        sampleResponse: createRes_json_1.default
    }, {
        method: http_constant_1.HTTP.PUT,
        path: "/message/readby/update",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: true },
        transactions: [collection_enum_1.COLLECTION.CONVERSATIONS, collection_enum_1.COLLECTION.MESSAGES],
        access: [{ tokenPath: "_id", payloadPath: "profileID" }],
        validator: message_validators_1.updateReadByReq,
        controller: message_controllers_1.updateMessageReadBy,
        description: "update messages",
        sampleResponse: listRes_json_2.default
    }, {
        method: http_constant_1.HTTP.GET,
        path: "/message/online/user/list",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: true },
        access: [{ tokenPath: "_id", payloadPath: "profileID" }],
        validator: message_validators_1.getOnlineUserReq,
        controller: message_controllers_1.listOnlineUserHandler,
        description: "list online user (get from socket)",
        sampleResponse: [
            { profileID: "profiles/ce6e6581-69a4-4245-b6ca-c46ce271ffd2", name: "Nurul Ain" },
            { profileID: "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8", name: "Hong Wei" }
        ]
    }, {
        method: http_constant_1.HTTP.POST,
        path: "/message/by/conversationID/list",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: true },
        access: [{ tokenPath: "_id", fetched: { fetchedDataCollection: collection_enum_1.COLLECTION.CONVERSATIONS, path: "profileIDs" } }],
        validator: message_validators_1.listMessageByConversationIDReq,
        controller: message_controllers_1.listMessageByConversationID,
        description: "list message base on conversationID with pagination",
        sampleResponse: listRes_json_1.default
    }
];
exports.default = MessageRouteMatrices;
