"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateReadByReq = exports.listMessageByConversationIDReq = exports.getOnlineUserReq = exports.createMessageReq = exports.createTestMessageReq = void 0;
const joi_1 = __importDefault(require("joi"));
const profile_model_1 = require("../../../models/profile.model");
const message_model_1 = require("../../../models/message.model");
const conversation_model_1 = require("../../../models/conversation.model");
exports.createTestMessageReq = joi_1.default.object().keys({
    senderProfileID: profile_model_1.profileID.required(),
    senderName: joi_1.default.string().required(),
    text: joi_1.default.string().required(),
    receiverProfileID: profile_model_1.profileID.required(),
    receiverName: joi_1.default.string().required()
});
exports.createMessageReq = joi_1.default.object().keys(message_model_1.messageFields);
exports.getOnlineUserReq = joi_1.default.object().keys({
    profileID: profile_model_1.profileID.required()
});
exports.listMessageByConversationIDReq = joi_1.default.object().keys({
    conversationID: conversation_model_1.conversationID.required(),
    pagination: joi_1.default.object().keys({
        offset: joi_1.default.number().default(0),
        limit: joi_1.default.number().default(100)
    }).required()
});
exports.updateReadByReq = joi_1.default.object().keys({
    conversationID: conversation_model_1.conversationID.required(),
    profileID: profile_model_1.profileID.required()
});
