"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.listConversationHandler = exports.createGroupHandler = void 0;
const conversation_1 = __importDefault(require("../../../repository/conversation"));
const document_1 = require("../../../helper/document");
const collection_enum_1 = require("../../../database/arango/collection.enum");
const createGroupHandler = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const validatedPayload = ctx.validatedPayload;
        const jwtData = ctx.jwtData;
        const { _id: senderProfileID } = jwtData;
        validatedPayload.unreadMessagesCount = {};
        const unreadUsers = validatedPayload.profileIDs.filter((profileID) => profileID != senderProfileID);
        unreadUsers.forEach(profileID => {
            if (!validatedPayload.unreadMessagesCount[profileID])
                validatedPayload.unreadMessagesCount[profileID] = 0;
            validatedPayload.unreadMessagesCount[profileID]++;
        });
        const { err, res } = yield conversation_1.default.create((0, document_1.docHelper)(validatedPayload, collection_enum_1.COLLECTION.CONVERSATIONS, senderProfileID));
        if (err)
            throw err;
        return { res, err: undefined };
    }
    catch (error) {
        return { err: error, res: undefined };
    }
});
exports.createGroupHandler = createGroupHandler;
const listConversationHandler = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { profileID } = ctx.validatedPayload;
        const { res, err } = yield conversation_1.default.list(profileID);
        if (err)
            throw err;
        return { res, err: undefined };
    }
    catch (error) {
        return { err: error, res: undefined };
    }
});
exports.listConversationHandler = listConversationHandler;
