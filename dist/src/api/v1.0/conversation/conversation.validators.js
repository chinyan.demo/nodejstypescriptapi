"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.listConversationReq = exports.createGroupReq = void 0;
const joi_1 = __importDefault(require("joi"));
const conversation_model_1 = require("../../../models/conversation.model");
const profile_model_1 = require("../../../models/profile.model");
exports.createGroupReq = joi_1.default.object().keys(Object.assign(Object.assign({}, conversation_model_1.conversationFields), { type: joi_1.default.string().valid("group_message").required() }));
exports.listConversationReq = joi_1.default.object().keys({
    profileID: profile_model_1.profileID.required()
});
