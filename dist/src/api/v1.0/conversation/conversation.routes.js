"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_constant_1 = require("../../../constant/http.constant");
const conversation_controllers_1 = require("./conversation.controllers");
const conversation_validators_1 = require("./conversation.validators");
const permission_1 = require("../../../constant/permission");
const createRes_json_1 = __importDefault(require("../../../helper/swagger/data/conversations/createRes.json"));
const listRes_json_1 = __importDefault(require("../../../helper/swagger/data/conversations/listRes.json"));
const ConversationRouteMatrices = [
    {
        method: http_constant_1.HTTP.POST,
        path: "/conversation/group/create",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: true },
        access: [{ tokenPath: "_id", payloadPath: "profileIDs" }],
        validator: conversation_validators_1.createGroupReq,
        controller: conversation_controllers_1.createGroupHandler,
        description: "create new chat group",
        sampleResponse: createRes_json_1.default
    }, {
        method: http_constant_1.HTTP.GET,
        path: "/conversation/list",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: true },
        access: [{ tokenPath: "_id", payloadPath: "profileID" }],
        validator: conversation_validators_1.listConversationReq,
        controller: conversation_controllers_1.listConversationHandler,
        description: "list conversation by profileID",
        sampleResponse: listRes_json_1.default
    }
];
exports.default = ConversationRouteMatrices;
