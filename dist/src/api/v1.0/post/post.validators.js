"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.listPostRequestSchema = exports.createPostRequestSchema = void 0;
const joi_1 = __importDefault(require("joi"));
const post_model_1 = require("../../../models/post.model");
exports.createPostRequestSchema = joi_1.default.object().keys(post_model_1.postFields);
exports.listPostRequestSchema = joi_1.default.object().keys({
    _id: post_model_1.postID.optional(),
});
