"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_constant_1 = require("../../../constant/http.constant");
const permission_1 = require("../../../constant/permission");
const post_validators_1 = require("./post.validators");
const post_controllers_1 = require("./post.controllers");
const PostRouteMatrices = [
    {
        method: http_constant_1.HTTP.POST,
        path: "/post/create",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: post_validators_1.createPostRequestSchema,
        controller: post_controllers_1.createPost,
        description: "create a post",
    },
    {
        method: http_constant_1.HTTP.POST,
        path: "/post/list",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: post_validators_1.listPostRequestSchema,
        controller: post_controllers_1.listPost,
        description: "list all posts",
    },
];
exports.default = PostRouteMatrices;
