"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_constant_1 = require("../../../constant/http.constant");
const profile_controllers_1 = require("./profile.controllers");
const profile_validators_1 = require("./profile.validators");
const permission_1 = require("../../../constant/permission");
const fetchRes_json_1 = __importDefault(require("../../../helper/swagger/data/profile/fetchRes.json"));
const ProfileRouteMatrices = [
    {
        method: http_constant_1.HTTP.GET,
        path: "/profile/fetch",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: true },
        access: [{ tokenPath: "_id", payloadPath: "profileID" }],
        validator: profile_validators_1.fetchReq,
        controller: profile_controllers_1.fetchHandler,
        description: "fetch user profile",
        sampleResponse: fetchRes_json_1.default
    }
];
exports.default = ProfileRouteMatrices;
