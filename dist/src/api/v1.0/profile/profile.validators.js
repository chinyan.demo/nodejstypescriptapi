"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fetchReq = void 0;
const joi_1 = __importDefault(require("joi"));
const profile_model_1 = require("../../../models/profile.model");
exports.fetchReq = joi_1.default.object().keys({
    profileID: profile_model_1.profileID.required()
});
