"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.listFriendRequestSchema = exports.createFriendRequestSchema = void 0;
const joi_1 = __importDefault(require("joi"));
const profile_model_1 = require("../../../models/profile.model");
exports.createFriendRequestSchema = joi_1.default.object().keys({
    currentID: profile_model_1.profileID.required(),
    friendID: profile_model_1.profileID.required()
});
exports.listFriendRequestSchema = joi_1.default.object().keys({
    profileID: profile_model_1.profileID.required()
});
