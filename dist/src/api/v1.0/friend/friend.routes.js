"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_constant_1 = require("../../../constant/http.constant");
const permission_1 = require("../../../constant/permission");
const friend_validators_1 = require("./friend.validators");
const friend_controllers_1 = require("./friend.controllers");
const PostRouteMatrices = [
    {
        method: http_constant_1.HTTP.POST,
        path: "/friend/create",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: friend_validators_1.createFriendRequestSchema,
        controller: friend_controllers_1.createFriend,
        description: "create a friend",
    },
    {
        method: http_constant_1.HTTP.POST,
        path: "/friend/list",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: friend_validators_1.listFriendRequestSchema,
        controller: friend_controllers_1.listFriend,
        description: "list a friend",
    },
    {
        method: http_constant_1.HTTP.POST,
        path: "/friend/delete",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: friend_validators_1.createFriendRequestSchema,
        controller: friend_controllers_1.deleteFriend,
        description: "remove a friend",
    },
];
exports.default = PostRouteMatrices;
