"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteFriend = exports.listFriend = exports.createFriend = void 0;
const transaction_1 = require("../../../database/arango/transaction");
const collection_enum_1 = require("../../../database/arango/collection.enum");
const friend_1 = __importDefault(require("../../../repository/friend"));
const document_1 = require("../../../helper/document");
const conversation_1 = __importDefault(require("../../../repository/conversation"));
const createFriend = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payload = ctx.validatedPayload;
        const { currentID, friendID } = payload;
        const { res, err } = yield friend_1.default.fetch(payload);
        if (res.length != 0)
            throw "This person has already been added as a friend.";
        const { res: createFriendRes, err: transactionErr } = yield (0, transaction_1.useTransaction)([collection_enum_1.EDGE_COLLECTION.FRIENDS, collection_enum_1.COLLECTION.CONVERSATIONS], (txn) => __awaiter(void 0, void 0, void 0, function* () {
            const { res: createFriendTo, err: createFriendToError, } = yield friend_1.default.create((0, document_1.docHelper)({ _from: currentID, _to: friendID }, collection_enum_1.EDGE_COLLECTION.FRIENDS, currentID), txn);
            if (createFriendToError)
                throw createFriendToError;
            const { res: createFriendFrom, err: createFriendFromError, } = yield friend_1.default.create((0, document_1.docHelper)({ _from: friendID, _to: currentID }, collection_enum_1.EDGE_COLLECTION.FRIENDS, currentID), txn);
            if (createFriendFromError)
                throw createFriendFromError;
            const { res: createdConversation, err: createConversationError } = yield conversation_1.default.create((0, document_1.docHelper)({ type: "private_message", profileIDs: [currentID, friendID], unreadMessagesCount: { [currentID]: 0, [friendID]: 0 } }, collection_enum_1.COLLECTION.CONVERSATIONS, currentID), txn);
            if (createConversationError)
                throw createConversationError;
            return {
                res: { createFriendTo, createFriendFrom, createdConversation },
            };
        }));
        if (transactionErr)
            throw transactionErr;
        return { res: createFriendRes, err: undefined };
    }
    catch (error) {
        return { res: undefined, err: error };
    }
});
exports.createFriend = createFriend;
const listFriend = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payload = ctx.validatedPayload;
        const filters = { profileID: payload === null || payload === void 0 ? void 0 : payload.profileID };
        const { res: listFriendRes, err: fetchErr } = yield friend_1.default.fetchManyWithFilters(filters);
        if (fetchErr)
            throw fetchErr;
        return { res: listFriendRes, err: undefined };
    }
    catch (error) {
        return { res: undefined, err: error };
    }
});
exports.listFriend = listFriend;
const deleteFriend = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payload = ctx.validatedPayload;
        const { res: deleteFriendRes, err: transactionErr } = yield (0, transaction_1.useTransaction)([collection_enum_1.EDGE_COLLECTION.FRIENDS], (txn) => __awaiter(void 0, void 0, void 0, function* () {
            const friendFromDoc = yield friend_1.default.fetch(payload);
            if (friendFromDoc.res.length === 0)
                throw { message: "Unable to find document." };
            const { res: deleteFriendTo } = yield friend_1.default.delete(friendFromDoc.res[0]._id, txn);
            const invertPayload = {
                currentID: payload.friendID,
                friendID: payload.currentID,
            };
            const friendToDoc = yield friend_1.default.fetch(invertPayload);
            if (friendToDoc.res.length === 0)
                throw { message: "Unable to find document (invert)." };
            const { res: deleteFriendFrom } = yield friend_1.default.delete(friendToDoc.res[0]._id, txn);
            return {
                res: {
                    message: "Successfully removed friend.",
                },
            };
        }));
        if (transactionErr)
            throw transactionErr;
        return { res: deleteFriendRes, err: undefined };
    }
    catch (error) {
        return { res: undefined, err: error };
    }
});
exports.deleteFriend = deleteFriend;
