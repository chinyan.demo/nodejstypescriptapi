"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.refreshTokenHandler = exports.loginHandler = exports.registerHandler = void 0;
const cryptography_1 = require("../../../helper/cryptography");
const document_1 = require("../../../helper/document");
const collection_enum_1 = require("../../../database/arango/collection.enum");
const profile_1 = __importDefault(require("../../../repository/profile"));
const account_1 = __importDefault(require("../../../repository/account"));
const cryptography_2 = require("../../../helper/cryptography");
const registerHandler = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payload = ctx.validatedPayload;
        const txn = ctx.txn;
        const { name, email, password, role } = payload;
        const { hash, salt } = (0, cryptography_1.createHash)(password);
        const profile = (0, document_1.docHelper)({ name, email, role }, collection_enum_1.COLLECTION.PROFILES);
        const profileID = profile._id;
        const account = (0, document_1.docHelper)({ email, password: { hash, salt }, profileID }, collection_enum_1.COLLECTION.ACCOUNTS, profileID);
        // const {res:createProfileRes,err:transactionErr} = await  useTransaction([COLLECTION.PROFILES,COLLECTION.ACCOUNTS], async txn=>{
        const { res: fetchedProfiles, err: fetchProfilesErr } = yield profile_1.default.fetchByEmail(email, txn);
        if (fetchProfilesErr)
            throw { err: fetchProfilesErr };
        if (fetchedProfiles && fetchedProfiles.length > 0)
            throw { err: "Email Taken" };
        const { res: createProfileRes, err: createProfileErr } = yield profile_1.default.create(profile, txn);
        if (createProfileErr)
            throw { err: createProfileErr };
        const { err: createAccountErr } = yield account_1.default.create(account, txn);
        if (createAccountErr)
            throw { err: createAccountErr };
        // });
        // if(transactionErr) throw transactionErr;
        return { res: createProfileRes, err: null };
    }
    catch (error) {
        console.log({ error });
        return { err: error, res: null };
    }
});
exports.registerHandler = registerHandler;
const loginHandler = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const payload = ctx.validatedPayload;
        const { email, password } = payload;
        const { res: fetchedAccounts, err: fetchAccountsErr } = yield account_1.default.fetchAccByEmail(email);
        if (fetchAccountsErr)
            throw fetchAccountsErr;
        if (fetchedAccounts && fetchedAccounts.length <= 0)
            throw "invalid email";
        const account = fetchedAccounts[0];
        const hash = String(account === null || account === void 0 ? void 0 : account.password.hash);
        const salt = String(account === null || account === void 0 ? void 0 : account.password.salt);
        const profileID = String(account === null || account === void 0 ? void 0 : account.profileID);
        const accountID = String(account === null || account === void 0 ? void 0 : account._id);
        const valid = (0, cryptography_2.validateHash)(hash, salt, password);
        if (!valid)
            throw "invalid password";
        const { res: profile, err: fetchProfileErr } = yield profile_1.default.fetch(profileID);
        if (fetchProfileErr)
            throw fetchProfileErr;
        if (!profile)
            throw "fetch profile failed invalid profile";
        const xForwardedFor = ctx.request.headers['x-forwarded-for'];
        const clientIP = (typeof xForwardedFor == "undefined") ? ctx.socket.remoteAddress : (Array.isArray(xForwardedFor)) ? xForwardedFor.shift() : xForwardedFor;
        //create JWT
        const token = (0, cryptography_1.createJWT)(Object.assign(Object.assign({ accountID }, profile), { clientIP }));
        return { res: { profile, token } };
    }
    catch (error) {
        return { err: error };
    }
});
exports.loginHandler = loginHandler;
const refreshTokenHandler = (ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const jwtData = ctx.jwtData;
        // console.table(jwtData)
        const xForwardedFor = ctx.request.headers['x-forwarded-for'];
        const clientIP = (typeof xForwardedFor == "undefined") ? ctx.socket.remoteAddress : (Array.isArray(xForwardedFor)) ? xForwardedFor.shift() : xForwardedFor;
        const token = (0, cryptography_1.createJWT)(Object.assign(Object.assign({}, jwtData), { clientIP }));
        return { res: { token }, err: undefined };
    }
    catch (error) {
        return { res: undefined, err: error };
    }
});
exports.refreshTokenHandler = refreshTokenHandler;
