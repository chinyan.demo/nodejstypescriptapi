"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_constant_1 = require("../../../constant/http.constant");
const auth_controllers_1 = require("./auth.controllers");
const auth_validators_1 = require("./auth.validators");
const permission_1 = require("../../../constant/permission");
const collection_enum_1 = require("../../../database/arango/collection.enum");
const loginRes_json_1 = __importDefault(require("../../../helper/swagger/data/auth/loginRes.json"));
const registerRes_json_1 = __importDefault(require("../../../helper/swagger/data/auth/registerRes.json"));
const refreshTokenRes_json_1 = __importDefault(require("../../../helper/swagger/data/auth/refreshTokenRes.json"));
const AuthRouteMatrices = [
    {
        method: http_constant_1.HTTP.POST,
        path: "/auth/register",
        permission: { roles: [], useJwt: false },
        access: [],
        transactions: [collection_enum_1.COLLECTION.PROFILES, collection_enum_1.COLLECTION.ACCOUNTS],
        validator: auth_validators_1.RegisterReq,
        controller: auth_controllers_1.registerHandler,
        description: "register new user",
        sampleResponse: registerRes_json_1.default
    }, {
        method: http_constant_1.HTTP.POST,
        path: "/auth/login",
        permission: { roles: [], useJwt: false },
        access: [],
        validator: auth_validators_1.LogInReq,
        controller: auth_controllers_1.loginHandler,
        description: "user login",
        sampleResponse: loginRes_json_1.default
    }, {
        method: http_constant_1.HTTP.POST,
        path: "/auth/refresh/token",
        permission: { roles: permission_1.PERMISSION.MEMBERS_ONLY, useJwt: true },
        access: [{ tokenPath: "_id", payloadPath: "profileID" }],
        validator: auth_validators_1.RefreshTokenReq,
        controller: auth_controllers_1.refreshTokenHandler,
        description: "refresh token",
        sampleResponse: refreshTokenRes_json_1.default
    }
];
exports.default = AuthRouteMatrices;
