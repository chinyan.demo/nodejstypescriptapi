"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefreshTokenReq = exports.LogInReq = exports.RegisterReq = exports.openToAll = void 0;
const joi_1 = __importDefault(require("joi"));
const profile_model_1 = require("../../../models/profile.model");
/***********************
 * validator
 ***********************/
exports.openToAll = joi_1.default.allow();
exports.RegisterReq = joi_1.default.object().keys(Object.assign(Object.assign({}, profile_model_1.profileField), { password: joi_1.default.string().required(), confirmPassword: joi_1.default.string().required().valid(joi_1.default.ref("password")).messages({ 'any.only': 'confirmPassword must match with password' }) }));
exports.LogInReq = joi_1.default.object().keys({
    email: profile_model_1.profileField.email,
    password: joi_1.default.string().required(),
});
exports.RefreshTokenReq = joi_1.default.object().keys({
    profileID: profile_model_1.profileID.required()
});
