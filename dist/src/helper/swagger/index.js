"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.swaggerMiddleware = void 0;
const config_1 = __importDefault(require("../../constant/config"));
const koa2_swagger_ui_1 = require("koa2-swagger-ui");
// import YAML  from "yamljs"
// import serve from "koa-static";
const koa_mount_1 = __importDefault(require("koa-mount"));
const generator_1 = __importDefault(require("./generator"));
const setting_1 = __importDefault(require("../../repository/setting"));
const process_1 = require("process");
const swaggerMiddleware = (app) => __awaiter(void 0, void 0, void 0, function* () {
    // if(config.APP_ENV == "development"||config.APP_ENV == "dev"){
    yield setting_1.default.deleteByFilter({ _key: "swagger" });
    yield (0, generator_1.default)();
    const { res: doc } = yield setting_1.default.fetch("swagger");
    app.use((0, koa_mount_1.default)("/documentation", (ctx) => __awaiter(void 0, void 0, void 0, function* () { ctx.body = doc.documentation; })));
    // app.use(mount("/doc",serve(__dirname)))
    app.use((0, koa2_swagger_ui_1.koaSwagger)({
        routePrefix: '/swagger',
        swaggerOptions: {
            // url: `${config.PROTOCOL}://${config.DOMAIN}:${config.PORT}/documentation`, 
            url: (/devServer.ts/i.test(process_1.argv[process_1.argv.length - 1])) ? `${config_1.default.PROTOCOL}://${config_1.default.DOMAIN}:${config_1.default.PORT}/documentation` : "https://dev.chatbot.drezy.io/documentation"
            // spec:YAML.load(path.join(__dirname, "swagger.yaml"));
            // spec:doc.documentation
        }
    }));
    // }
});
exports.swaggerMiddleware = swaggerMiddleware;
