"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// import fs from "fs"
// import camelCase from "camelcase"
const joi_to_swagger_1 = __importDefault(require("joi-to-swagger"));
// import Path from 'path';
const config_1 = __importDefault(require("../../constant/config"));
const permission_1 = require("../../constant/permission");
const routeMatrices_1 = require("../../api/routeMatrices");
const setting_1 = __importDefault(require("../../repository/setting"));
const data = {
    "swagger": "2.0",
    "info": {
        "description": "API definition for the Logic API",
        "version": "1.0.0",
        "title": "API Documentation for Development",
    },
    "host": "detecting...",
    "basePath": "/main/api/v1.0",
};
let tags = [];
let paths = {};
let definitions = {};
//get last part after split path by /
const getLastAfterSplit = (path) => {
    const parts = path.split("/");
    if (parts.length === 1 || !parts.length)
        return path;
    return (parts[parts.length - 1]);
};
const ArrayToString = (array) => {
    let ret = "";
    for (let i = 0; i < array.length; i++) {
        const g = array[i];
        const type = array[i].type;
        const subType = array[i].subType;
        if (type || subType) {
            // ret+=camelCase(type+"-"+subType,{pascalCase: true});
            ret += type + "-" + subType;
        }
        else {
            // ret+=camelCase(array[i] ,{pascalCase: true});
            ret += array[i];
        }
        if (i < array.length - 1)
            ret += ", ";
    }
    return ret;
};
const generateSwaggerDoc = () => __awaiter(void 0, void 0, void 0, function* () {
    // tags.push({
    //     "name": "PRE-DEFINES",
    //     "description": "Predefined Orgs for test"
    // });
    // paths.Default:Object = {
    //     get: {
    //         tags: ["PRE-DEFINES"],
    //         summary: "useful info to test APIs",
    //         description: "Predefined OWNER Information to test. here is data",
    //         operationId: "Predefined OWNER",
    //         consumes: ["application/json"],
    //         produces: ["application/json"],
    //         parameters: "Default"
    //     }
    // };
    //iterate all routes
    const RouteMatrixes = (0, routeMatrices_1.getRouteMatricesV1)();
    RouteMatrixes.forEach(r => {
        // const matrix = r.Routes;
        let tag = r.path.split("/")[1];
        // if (r.Group.search(/\/dem\//g)>=0) tag = "dem_" + tag;
        // if (r.Group.search(/\/ecom\//g)>=0) tag = "ecom_" + tag;
        // if (r.Group.search(/\/p2u\//g)>=0) tag = "p2u_" + tag;
        tags.push({
            "name": tag,
            "description": r.description
        });
        // for (let i=0;i<matrix.length;i++){
        // const route = matrix[i];
        const route = r;
        const pathDash = route.path.split("/").join("-");
        const p = pathDash + "-payload";
        // const payloadName = camelCase(tag+"-"+p,{pascalCase: false});
        const payloadName = tag + "-" + p;
        const isProtected = (route.permission.useJwt) ? "Yes" : "No";
        let protection_type = "";
        /*if (route.access==Grants.ProtectedByACL) protection_type = "ProtectedByACL";
        else*/ if (route.permission.useJwt)
            protection_type = "ProtectedByToken";
        else
            protection_type = ArrayToString(route.permission.roles);
        const allowedRoles = (route.permission.roles.find(doc => doc == permission_1.OPEN_TO_ALL) == permission_1.OPEN_TO_ALL || route.permission.roles.length == 0) ? "All" : protection_type;
        const description = route.description + "<p><b>protected:</b> " + isProtected + "</p> <p><b>allowed roles:</b> " + allowedRoles + "</p>";
        // const apiSuccessDef = camelCase(tag+"-"+pathDash+"-"+"api-success");
        const apiSuccessDef = tag + "-" + pathDash + "-" + "api-success";
        paths[route.path] = {
            [route.method.toLocaleLowerCase()]: {
                tags: [tag],
                summary: route.description,
                description: description,
                operationId: getLastAfterSplit(route.path),
                consumes: ["application/json"],
                produces: ["application/json"],
                parameters: [
                    Object.assign({ "in": "body", "name": p.split("-").join(" "), "description": p.split("-").join(" ") }, (route.validator) && { "schema": {
                            "$ref": `#/definitions/${payloadName}`
                        } })
                ],
                responses: {
                    "200": {
                        description: "Success object",
                        schema: { "$ref": "#/definitions/" + apiSuccessDef }
                    },
                    "500": {
                        description: "Internal API error or service unavailable",
                        schema: { "$ref": '#/definitions/500Error' }
                    }
                }
            }
        };
        definitions[apiSuccessDef] = {
            require: ["success", "data", "error", "status"],
            properties: {
                success: { "type": "boolean", "example": "true" },
                error: { "type": "object", "example": "null" },
                status: { "type": "string", "example": "OK" },
                data: { "type": "object", "example": route.sampleResponse }
            }
        };
        if (route.validator) {
            const swagger = (0, joi_to_swagger_1.default)(route.validator).swagger;
            definitions[payloadName] = swagger;
        }
        // }   
    });
    definitions["apiSuccess"] = {
        require: ["success", "data", "error", "status"],
        properties: {
            success: { "type": "boolean", "example": "true" },
            error: { "type": "object", "example": "null" },
            status: { "type": "string", "example": "OK" },
            data: { "type": "object", "example": { "field1": "...", "field2": "...", "fieldN": "..." } }
        }
    };
    definitions["400Error"] = {
        require: ["success", "data", "error", "status"],
        properties: {
            error: { "$ref": '#/definitions/400ErrorData' },
            status: { "type": "string", "example": "error" },
            data: { "type": "object", "example": "null", "properties": {} }
        }
    };
    definitions["422Error"] = {
        require: ["success", "data", "error", "status"],
        properties: {
            success: { "type": "boolean", "example": "false" },
            error: { "$ref": '#/definitions/422ErrorData' },
            status: { "type": "string", "example": "error" },
            data: { "type": "object", "example": "null", "properties": {} }
        }
    };
    definitions["403Error"] = {
        require: ["success", "data", "error", "status"],
        properties: {
            success: { "type": "boolean", "example": "false" },
            error: { "$ref": '#/definitions/403ErrorData' },
            status: { "type": "string", "example": "error" },
            data: { "type": "object", "example": "null", "properties": {} }
        }
    };
    definitions["500Error"] = {
        require: ["success", "data", "error", "status"],
        properties: {
            success: { "type": "boolean", "example": "false" },
            error: { "$ref": '#/definitions/500ErrorData' },
            status: { "type": "string", "example": "error" },
            data: { "type": "object", "example": "null", "properties": {} }
        }
    };
    definitions["400ErrorData"] = {
        require: ["code", "message", "type"],
        properties: {
            code: { "type": "integer", "example": '1001' },
            message: { "type": "string", "example": "Joi validation error message here" },
            type: { "type": "string", "example": "REQUEST_PAYLOAD_VALIDATION" }
        }
    };
    definitions["403ErrorData"] = {
        require: ["code", "message", "type"],
        properties: {
            code: { "type": "integer", "example": '2001' },
            message: { "type": "string", "example": "Invalid email or password (or JWT fail)" },
            type: { "type": "string", "example": "FORBIDDEN" }
        }
    };
    definitions["422ErrorData"] = {
        require: ["code", "message", "type"],
        properties: {
            code: { "type": "integer", "example": '3001' },
            message: { "type": "string", "example": "The provided email is pending registration with another account." },
            type: { "type": "string", "example": "ACCOUNT_REGISTRY_PENDING" }
        }
    };
    definitions["500ErrorData"] = {
        require: ["code", "message", "type"],
        properties: {
            code: { "type": "integer", "example": '5001' },
            message: { "type": "string", "example": "Internal server error or service unavailable" },
            type: { "type": "string", "example": "INTERNAL_SERVER_ERROR" }
        }
    };
    data.tags = tags;
    data.schemes = [config_1.default.PROTOCOL];
    data.paths = paths;
    data.definitions = definitions;
    //let public_ip = "detecting..."
    //publicIp.v4().then(ip => {public_ip = ip});
    const host_ip = config_1.default.DOMAIN || "127.0.0.1";
    data["host"] = config_1.default.PORT ? `${host_ip}:${config_1.default.PORT}` : host_ip;
    // let jsonString = JSON.stringify(data,undefined,2);
    // const filePath = Path.resolve(__dirname,'./doc/swaggerDoc.json');
    //ColorLog.Blue("filePath : ",filePath)
    // fs.writeFileSync(filePath, jsonString);
    yield setting_1.default.upsertSwagger(data);
});
exports.default = generateSwaggerDoc;
