"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class OnlineUsersTracker {
    constructor() {
        this.socketIDs = {};
        this.onlineUser = {};
    }
    addOnlineUser(profileID, socketID, name) {
        if (profileID && profileID != "backend") {
            if (!this.socketIDs[profileID])
                this.socketIDs[profileID] = [];
            this.socketIDs[profileID].push(socketID);
            this.onlineUser[socketID] = { profileID, name };
            console.log("a user connected.");
            // console.table({socketID,profileID,name});
        }
    }
    getProfileBySocketID(socketID) {
        return (this.onlineUser[socketID]) ? JSON.parse(JSON.stringify(this.onlineUser[socketID])) : undefined;
    }
    removeOnlineUser(socketID) {
        if (this.onlineUser[socketID]) {
            const profileID = this.onlineUser[socketID].profileID;
            if (profileID && profileID != "backend") {
                this.socketIDs[profileID] = this.socketIDs[profileID].filter(id => id != socketID);
                delete this.onlineUser[socketID];
                if (this.socketIDs[profileID].length == 0) {
                    delete this.socketIDs[profileID];
                }
            }
        }
    }
    getSocketIDs(profileID) {
        return this.socketIDs[profileID];
    }
    getAllOnlineUser() {
        return Object.values(this.onlineUser);
    }
}
const onlineUsersTracker = new OnlineUsersTracker();
exports.default = onlineUsersTracker;
