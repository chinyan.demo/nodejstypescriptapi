"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConferenceHandler = void 0;
const topics_1 = require("../topics");
const onlineUser_1 = __importDefault(require("./onlineUser"));
/**
WebRTC is a set of protocols that allow applications, typically running on Web browsers, to exchange media (audio, video, data) with other entities.

Before media can flow, however, the WebRTC entities need to discover what type of connection is possible, and among the possible connections,
what’s the best to be used. This needs to happen as fast as possible, so that users can perceive the service as instantaneous as possible.
WebRTC includes protocols like STUN and TURN that are designed to facilitate the establishment of connections when a direct connection is not possible.
The typical case is a computer inside a home or office network, with a private IP address, and able to reach the public
Internet only through an address translation (NAT).

STUN helps in discovering the IP address and port from where a computer enters the Internet,
and in some circumstances that IP address and port can be used by other entities to reach that computer.
STUN is also used for keeping such bindings alive.

TURN provides a way for two entities to communicate when they are behind two different symmetric NATs,
or when one is behind a firewall that restricts outbound traffic to only some UDP or TCP ports.
TURN uses STUN as the underlying protocol, adding requests, responses and indications to accomplish media relay.

STUN and TURN play a role in the ICE negotiation process.
ICE,  Interactive Connectivity Establishment,
is a protocol that allows the dynamic discovery of the best way to establish a connection for entities that may be behind NAT.

All WebRTC clients use ICE before media can flow.

There are three main phases:
    1. the gathering of candidates,
    2. the connectivity checks,
    3. and the nomination of the candidate pairs to be used.
 */
const ConferenceHandler = (socket) => {
    // Conference Room
    socket.on(topics_1.TOPIC.CONFERENCE_INVITATION, (profileIDs, conversationID, hostProfileID, message) => {
        profileIDs === null || profileIDs === void 0 ? void 0 : profileIDs.forEach(pid => {
            const socketIDs = onlineUser_1.default.getSocketIDs(pid);
            socketIDs === null || socketIDs === void 0 ? void 0 : socketIDs.forEach((sid) => {
                socket.to(sid).emit(topics_1.TOPIC.CONFERENCE_INVITATION, { conversationID, message, hostProfileID });
            });
        });
    });
    socket.on(topics_1.TOPIC.CONFERENCE_INVITATION_ACCEPTED, ({ hostProfileID, profileID, name }) => {
        var _a;
        (_a = onlineUser_1.default.getSocketIDs(hostProfileID)) === null || _a === void 0 ? void 0 : _a.forEach(sid => {
            socket.to(sid).emit(topics_1.TOPIC.CONFERENCE_INVITATION_ACCEPTED, { profileID, name });
        });
    });
    socket.on(topics_1.TOPIC.CONFERENCE_PEER_OFFERING, ({ conversationID, profileID, name, signal }) => {
        console.table({ topic: topics_1.TOPIC.CONFERENCE_PEER_OFFERING, payload: { conversationID, profileID, name } });
        socket.to(conversationID).emit(topics_1.TOPIC.CONFERENCE_PEER_OFFERING, { conversationID, profileID, name, signal });
    });
    socket.on(topics_1.TOPIC.CONFERENCE_PEER_ANSWERING, ({ conversationID, offererProfileID, profileID, name, signal }) => {
        var _a;
        console.table({ topic: topics_1.TOPIC.CONFERENCE_PEER_ANSWERING, payload: { conversationID, offererProfileID, profileID, name } });
        (_a = onlineUser_1.default.getSocketIDs(offererProfileID)) === null || _a === void 0 ? void 0 : _a.forEach(sid => {
            socket.to(sid).emit(topics_1.TOPIC.CONFERENCE_PEER_ANSWERING, { conversationID, profileID, name, signal });
        });
    });
    socket.on(topics_1.TOPIC.JOIN_CONFERENCE, (conversationID, profileID, name) => {
        socket.join(conversationID); // Join the room
        // console.table({conversationID,profileID,name})
        // Notify user joined conference
        socket.to(conversationID).emit(topics_1.TOPIC.JOIN_CONFERENCE, {
            conversationID,
            profileID,
            name,
            socketIDs: onlineUser_1.default.getSocketIDs(profileID)
        });
        //Notify FE user disconnected
        socket.on(topics_1.TOPIC.DISCONNECT, () => {
            socket.to(conversationID).emit(topics_1.TOPIC.PEER_LEFT_CONFERENCE, {
                socketID: socket.id,
                profileID: onlineUser_1.default.getProfileBySocketID(socket.id)
            });
        });
    });
};
exports.ConferenceHandler = ConferenceHandler;
