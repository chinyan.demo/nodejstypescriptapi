"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.peerEventHandler = void 0;
const client_1 = require("../client");
const peerEventHandler = (data) => {
    const { senderProfileID, topic, payload, receiverProfileID } = data;
    (0, client_1.websocketSend)(receiverProfileID, topic, payload, senderProfileID);
};
exports.peerEventHandler = peerEventHandler;
