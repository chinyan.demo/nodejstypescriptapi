"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.p2pConnectionRequest = void 0;
const joi_1 = __importDefault(require("joi"));
exports.p2pConnectionRequest = joi_1.default.object().keys({
    senderProfileID: joi_1.default.string().required(),
    topic: joi_1.default.string().required(),
    payload: joi_1.default.object().required(),
    receiverProfileID: joi_1.default.string().required()
});
