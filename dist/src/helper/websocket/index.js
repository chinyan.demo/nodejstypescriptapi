"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.websocketMiddleware = void 0;
const topics_1 = require("./topics");
const cryptography_1 = require("../cryptography");
const onlineUser_1 = __importDefault(require("./handlers/onlineUser"));
const client_1 = require("./client");
const conference_1 = require("./handlers/conference");
const jwtMiddleware = (socket, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { token, profileID } = socket.handshake.auth;
    // verify token
    const { res, err } = (0, cryptography_1.validateJWT)(token);
    if (err) {
        console.log({ err });
        socket.emit(topics_1.TOPIC.AUTH_ERROR, { code: 401, error: "Unauthorized", msg: "token mul-formed" });
        socket.disconnect();
        next(new Error(JSON.stringify({ code: 401, error: "Unauthorized", msg: "token mul-formed" })));
    }
    if (res) {
        if (res._id == "backend") {
            // console.log({res})
            next();
        }
        const xForwardedFor = socket.handshake.headers['x-forwarded-for'];
        const clientIP = (typeof xForwardedFor == "undefined") ? socket.handshake.address : (Array.isArray(xForwardedFor)) ? xForwardedFor.shift() : xForwardedFor;
        if ((res === null || res === void 0 ? void 0 : res.clientIP) != clientIP && res._id != "backend") {
            console.log("wrong IP", `${res === null || res === void 0 ? void 0 : res.clientIP}!=${clientIP}`);
            socket.emit(topics_1.TOPIC.AUTH_ERROR, { code: 401, error: "Unauthorized", msg: "Invalid IP" });
            socket.disconnect();
            next(new Error(JSON.stringify({ code: 401, error: "Unauthorized", msg: "Invalid IP" })));
        }
        if (res._id == profileID) {
            // console.log("valid user : ",res._id)
            next();
        }
        else {
            socket.emit(topics_1.TOPIC.AUTH_ERROR, { code: 401, error: "Unauthorized", msg: "invalid user or token" });
            next(new Error(JSON.stringify({ code: 401, error: "Unauthorized", msg: "invalid user or token" })));
            socket.disconnect();
        }
    }
});
const websocketMiddleware = (io) => {
    io.use(jwtMiddleware);
    io.on(topics_1.TOPIC.CONNECT, (socket) => {
        const { profileID, name } = socket.handshake.auth;
        // if(profileID!="backend")console.table({
        //     profileID,
        //     transport:socket.conn.transport.name,
        //     socketID:socket.id
        // }); 
        if (profileID != "backend") {
            onlineUser_1.default.addOnlineUser(profileID, socket.id, name);
            // Broadcast To everyone other than connected user
            socket.broadcast.emit(topics_1.TOPIC.ONLINE, onlineUser_1.default.getAllOnlineUser());
            // Broadcast To connected user
            socket.emit(topics_1.TOPIC.ONLINE, onlineUser_1.default.getAllOnlineUser());
            socket.emit(topics_1.TOPIC.SOCKET_ID, socket.id);
        }
        //PRIVATE MESSAGE
        socket.on(topics_1.TOPIC.FORWARD_PRIVATE_MESSAGE, (data) => {
            const { content, receiverProfileID } = data;
            const socketIDs = onlineUser_1.default.getSocketIDs(receiverProfileID);
            console.log({ socketIDs });
            if (socketIDs) {
                socketIDs.forEach(socketID => {
                    socket.to(socketID).emit(topics_1.TOPIC.PRIVATE_MESSAGE, content);
                });
            }
        });
        //Test video call
        socket.on(topics_1.TOPIC.CALL_USER, data => {
            const { signalData, callerProfileID, callerName, userToCall } = data;
            console.log(data);
            const sockets = onlineUser_1.default.getSocketIDs(userToCall);
            console.log(sockets);
            if (sockets)
                sockets.forEach(sktID => {
                    io.to(sktID).emit(topics_1.TOPIC.CALL_USER, { signalData, callerProfileID, callerName, profileID: userToCall });
                });
        });
        socket.on(topics_1.TOPIC.ANSWER_CALL, data => {
            const { to, signal } = data;
            console.log({ [topics_1.TOPIC.ANSWER_CALL]: data });
            const sockets = onlineUser_1.default.getSocketIDs(to);
            if (sockets)
                sockets.forEach(sktID => {
                    io.to(sktID).emit(topics_1.TOPIC.CALL_ACCEPTED, signal);
                });
        });
        //Online Conference
        (0, conference_1.ConferenceHandler)(socket);
        //Disconnect
        socket.on(topics_1.TOPIC.DISCONNECT, (reason) => {
            console.log(`Socket ${socket.id} disconnected, reason : ${reason}`);
            const profile = onlineUser_1.default.getProfileBySocketID(socket.id);
            onlineUser_1.default.removeOnlineUser(socket.id);
            socket.broadcast.emit(topics_1.TOPIC.USER_DISCONNECTED, profile);
        });
    });
    client_1.socket.connect();
    // connectWebSocketClient()
};
exports.websocketMiddleware = websocketMiddleware;
