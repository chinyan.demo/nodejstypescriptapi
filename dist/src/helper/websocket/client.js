"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.serverWebsocketSend = exports.websocketSend = exports.socket = void 0;
const socket_io_client_1 = require("socket.io-client");
const config_1 = __importDefault(require("../../constant/config"));
const topics_1 = require("./topics");
const cryptography_1 = require("../cryptography");
const { PROTOCOL, DOMAIN, PORT } = config_1.default;
exports.socket = (0, socket_io_client_1.io)(`${PROTOCOL}://${DOMAIN}:${PORT}`, {
    path: "/websocket",
    autoConnect: false,
    transports: ["websocket"],
    auth: {
        token: (0, cryptography_1.createJWT)({ _id: "backend" }).accessToken,
        profileID: "backend",
        name: "backend"
    }
});
exports.socket.on(topics_1.TOPIC.CONNECT, () => {
    console.log('Connected to the server');
});
exports.socket.on(topics_1.TOPIC.DISCONNECT, (reason) => {
    console.log('Disconnected from the server:', reason);
});
/**
 * websocketSend
 * @param {string} topic
 * @param {object} message
 * @return {{error:any}}
 */
const websocketSend = (receiverProfileID, topic, message, senderProfileID) => {
    try {
        if (exports.socket.disconnected)
            exports.socket.connect();
        exports.socket.emit(receiverProfileID, { topic, message, senderProfileID, receiverProfileID });
        // console.log({emitted:true})
        exports.socket.on(topics_1.TOPIC.ERROR, (error) => { throw error; });
        return { error: undefined };
    }
    catch (error) {
        return { error };
    }
};
exports.websocketSend = websocketSend;
/**
 * serverWebsocketSend
 * @param topic
 * @param conversationID
 * @param content
 * @returns
 */
const serverWebsocketSend = (topic, receiverProfileID, content) => {
    try {
        if (exports.socket.disconnected)
            exports.socket.connect();
        // console.log({[topic]:{content,receiverProfileID}})
        exports.socket.emit(topic, { content, receiverProfileID });
        // console.log({emitted:true})
        return { success: true };
    }
    catch (error) {
        return { success: false, error };
    }
};
exports.serverWebsocketSend = serverWebsocketSend;
