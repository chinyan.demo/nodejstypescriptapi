"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TOPIC = void 0;
var TOPIC;
(function (TOPIC) {
    TOPIC["CONNECT"] = "connection";
    TOPIC["ONLINE"] = "online";
    TOPIC["INFO"] = "info";
    TOPIC["PRIVATE_MESSAGE"] = "private message";
    TOPIC["FORWARD_PRIVATE_MESSAGE"] = "FORWARD_PRIVATE_MESSAGE";
    TOPIC["DISCONNECT"] = "disconnect";
    TOPIC["CONNECTION_FAILED"] = "connect_failed";
    TOPIC["ERROR"] = "error";
    TOPIC["AUTH_ERROR"] = "auth error";
    TOPIC["CONNECT_ERROR"] = "connect_error";
    TOPIC["JOIN_CONFERENCE"] = "user-joined-conference";
    TOPIC["PEER_LEFT_CONFERENCE"] = "user-left-conference";
    TOPIC["CONFERENCE_INVITATION"] = "conference-invitation";
    TOPIC["CONFERENCE_INVITATION_ACCEPTED"] = "conference-invitation-accepted";
    TOPIC["CONFERENCE_PEER_OFFERING"] = "conference-peer-offering";
    TOPIC["CONFERENCE_PEER_ANSWERING"] = "conference-peer-answering";
    TOPIC["CONFERENCE_ICE_CANDIDATE"] = "conference-ice-candidates";
    TOPIC["CONFERENCE_ROOM_ONLINE"] = "conference-room-online";
    TOPIC["CONFERENCE_ROOM_BROADCAST"] = "conference-room-broadcast";
    TOPIC["CONFERENCE_ROOM_EMIT"] = "conference-room-emit";
    TOPIC["USER_CONNECT"] = "user-connected";
    TOPIC["USER_DISCONNECTED"] = "user-disconnected";
    TOPIC["VIDEO_CALL"] = "video call";
    TOPIC["JOIN_VIDEO_CALL"] = "join video call";
    TOPIC["BROADCAST"] = "broadcast";
    TOPIC["SOCKET_ID"] = "socketID";
    TOPIC["CALL_USER"] = "callUser";
    TOPIC["ANSWER_CALL"] = "answerCall";
    TOPIC["CALL_ACCEPTED"] = "callAccepted";
})(TOPIC || (exports.TOPIC = TOPIC = {}));
