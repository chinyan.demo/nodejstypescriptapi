"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.docHelper = void 0;
const uuid_1 = require("uuid");
const docHelper = (doc, collection, profileID) => {
    const _key = (0, uuid_1.v4)();
    const _id = `${collection}/${_key}`;
    return Object.assign(Object.assign({ _id,
        _key }, doc), { active: true, createdBy: profileID || _id, createdAt: new Date().toISOString(), updatedBy: profileID || _id, updatedAt: new Date().toISOString() });
};
exports.docHelper = docHelper;
