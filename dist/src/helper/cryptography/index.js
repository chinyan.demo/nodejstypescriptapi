"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateJWT = exports.createJWT = exports.hash = exports.validateHash = exports.createHash = exports.generateSalt = exports.base64String = exports.generateOTP = void 0;
const crypto_1 = __importDefault(require("crypto"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../../constant/config"));
/**
 * generateOTP
 * @returns {string}
 */
const generateOTP = () => {
    return String(Math.floor(Math.random() * 1000)).padStart(4, "0");
};
exports.generateOTP = generateOTP;
/**
 * base64String
 * @param {string} str
 * @returns {string}
 */
const base64String = (str) => {
    return Buffer.from(str).toString("base64");
};
exports.base64String = base64String;
/**
 * generateSalt
 * @returns {string}
 */
const generateSalt = () => {
    const randStr = (0, exports.generateOTP)();
    return (0, exports.base64String)(randStr);
};
exports.generateSalt = generateSalt;
/**
 * Hash
 * @param {string} str
 * @returns {{hash:string,salt:string}}
 */
const createHash = (str) => {
    const salt = (0, exports.generateSalt)();
    const saltedStr = `${salt}${str}`;
    const hash = crypto_1.default.createHash('sha256').update(saltedStr).digest('hex');
    return { hash, salt };
};
exports.createHash = createHash;
/**
 * validateHash
 * @param {string} hash
 * @param {string} str
 * @param {string} salt
 * @returns {bool}
 */
const validateHash = (hash, salt, str) => {
    const saltedStr = `${salt}${str}`;
    return hash === crypto_1.default.createHash('sha256').update(saltedStr).digest('hex');
};
exports.validateHash = validateHash;
const hash = (str) => crypto_1.default.createHash('sha256').update(str).digest('hex');
exports.hash = hash;
/**
 * createJWT
 * @param {object} payload
 * @returns {string}
 */
const createJWT = (payload) => {
    const secret = String(config_1.default.JWT_SECRET);
    const iat = parseInt(String(config_1.default.JWT_LIFESPAN)) + new Date().getTime();
    return { accessToken: jsonwebtoken_1.default.sign(Object.assign(Object.assign({}, payload), { iat }), secret), refreshToken: jsonwebtoken_1.default.sign(Object.assign(Object.assign({}, payload), { iat: 2 * iat }), secret) };
};
exports.createJWT = createJWT;
/**
 * validateJWT
 * @param {string} token
 * @returns {JwtPayload|string}
 */
const validateJWT = (token) => {
    try {
        const secret = String(config_1.default.JWT_SECRET);
        const res = jsonwebtoken_1.default.verify(token, secret);
        return { res, err: undefined };
    }
    catch (error) {
        return { res: undefined, err: error };
    }
};
exports.validateJWT = validateJWT;
