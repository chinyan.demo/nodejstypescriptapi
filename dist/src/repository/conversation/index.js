"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const repository_1 = __importDefault(require("../../database/arango/repository"));
const collection_enum_1 = require("../../database/arango/collection.enum");
const arango_1 = __importDefault(require("../../database/arango"));
const arangojs_1 = require("arangojs");
class ConversationRepository extends repository_1.default {
    constructor(collection) {
        super(collection);
    }
    list(profileID, txn) {
        const _super = Object.create(null, {
            query: { get: () => super.query }
        });
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const conversationCollection = (yield arango_1.default.getInstance()).collection(collection_enum_1.COLLECTION.CONVERSATIONS);
                const query = (0, arangojs_1.aql) `
                FOR c IN ${conversationCollection}
                FILTER ${profileID} IN c.profileIDs
                LET users = (
                    FOR p IN c.profileIDs FILTER p!= ${profileID}
                    RETURN {
                        profileID: p,
                        name: DOCUMENT(p).name
                    })
                LET name = (c.name)? c.name :(FOR p IN c.profileIDs FILTER p!=${profileID} RETURN DOCUMENT(p).name)[0]
                RETURN MERGE(c,{name:name,users:users})
            `;
                const result = yield _super.query.call(this, query, txn);
                return result;
            }
            catch (error) {
                return { err: error, res: undefined };
            }
        });
    }
}
const conversationRepo = new ConversationRepository(collection_enum_1.COLLECTION.CONVERSATIONS);
exports.default = conversationRepo;
