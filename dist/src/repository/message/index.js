"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const repository_1 = __importDefault(require("../../database/arango/repository"));
const collection_enum_1 = require("../../database/arango/collection.enum");
const arangojs_1 = require("arangojs");
const arango_1 = __importDefault(require("../../database/arango"));
class MessageRepository extends repository_1.default {
    constructor(collection) {
        super(collection);
    }
    list(filter, txn) {
        const _super = Object.create(null, {
            query: { get: () => super.query }
        });
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { conversationID } = filter;
                const messageCollection = (yield arango_1.default.getInstance()).collection(collection_enum_1.COLLECTION.MESSAGES);
                const query = (0, arangojs_1.aql) `
                FOR m IN ${messageCollection}
                FILTER m.conversationID == ${conversationID}
                RETURN m    
            `;
                const { res, err } = yield _super.query.call(this, query, txn);
                if (err)
                    throw err;
                return { res, err: undefined };
            }
            catch (error) {
                return { err: error, res: undefined };
            }
        });
    }
    updateReadBy(conversationID, profileID, txn) {
        const _super = Object.create(null, {
            query: { get: () => super.query }
        });
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const messageCollection = (yield arango_1.default.getInstance()).collection(collection_enum_1.COLLECTION.MESSAGES);
                const query = (0, arangojs_1.aql) `
                FOR m IN ${messageCollection}
                FILTER m.conversationID == ${conversationID}
                LET readBy = (m.readBy) ? m.readBy : []
                LET updatedReadBy = APPEND(readBy,${profileID},true)
                UPDATE m WITH {readBy:updatedReadBy}
                IN ${messageCollection}
                RETURN NEW    
            `;
                const { res, err } = yield _super.query.call(this, query, txn);
                if (err)
                    throw err;
                return { res, err: undefined };
            }
            catch (error) {
                return { err: error };
            }
        });
    }
    listMessageByConversationID(conversationID, pagination, txn) {
        const _super = Object.create(null, {
            query: { get: () => super.query }
        });
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { offset, limit } = pagination;
                let _offset = offset | 0;
                let _limit = limit | 100;
                const messageCollection = (yield arango_1.default.getInstance()).collection(collection_enum_1.COLLECTION.MESSAGES);
                const query = (0, arangojs_1.aql) `
                FOR data IN (
                    FOR m IN ${messageCollection}
                    FILTER m.conversationID == ${conversationID}
                    SORT m.createdAt DESC
                    LIMIT ${_offset} , ${_limit}
                    RETURN merge(m, {
                        sender:{
                            name: DOCUMENT(m.senderProfileID).name, 
                            profileID: m.senderProfileID
                        }
                    })
                )
                SORT data.createdAt ASC
                RETURN data
            `;
                const { res, err } = yield _super.query.call(this, query, txn);
                if (err)
                    throw err;
                return { res, err: undefined };
            }
            catch (error) {
                return { res: undefined, err: error };
            }
        });
    }
}
const messageRepo = new MessageRepository(collection_enum_1.COLLECTION.MESSAGES);
exports.default = messageRepo;
