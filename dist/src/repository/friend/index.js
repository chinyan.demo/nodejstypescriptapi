"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const repository_1 = __importDefault(require("../../database/arango/repository"));
const collection_enum_1 = require("../../database/arango/collection.enum");
const arangojs_1 = require("arangojs");
class FriendRepository extends repository_1.default {
    constructor(collection) {
        super(collection);
    }
    fetch(payload, txn) {
        const _super = Object.create(null, {
            query: { get: () => super.query }
        });
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const q = (0, arangojs_1.aql) `
            FOR f in friends
            FILTER f._from == ${payload.currentID} AND f._to == ${payload.friendID}
            RETURN f
                    `;
                const result = yield _super.query.call(this, q, txn);
                return result;
            }
            catch (err) {
                return { res: undefined, err };
            }
        });
    }
    fetchManyWithFilters(filters, txn) {
        const _super = Object.create(null, {
            query: { get: () => super.query }
        });
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { profileID } = filters;
                const q = (0, arangojs_1.aql) `
                FOR friend
                    IN 1..1
                    OUTBOUND ${profileID}
                    GRAPH friends
                RETURN friend
                `;
                const result = yield _super.query.call(this, q, txn);
                return result;
            }
            catch (err) {
                return { res: undefined, err };
            }
        });
    }
}
const friendRepo = new FriendRepository(collection_enum_1.EDGE_COLLECTION.FRIENDS);
exports.default = friendRepo;
