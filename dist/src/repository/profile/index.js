"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const repository_1 = __importDefault(require("../../database/arango/repository"));
const collection_enum_1 = require("../../database/arango/collection.enum");
const arangojs_1 = require("arangojs");
const arango_1 = __importDefault(require("../../database/arango"));
class ProfileRepository extends repository_1.default {
    constructor(collection) {
        super(collection);
    }
    fetchByEmail(email, txn) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const profileCol = (yield arango_1.default.getInstance()).collection(collection_enum_1.COLLECTION.PROFILES);
                const query = (0, arangojs_1.aql) `FOR p IN ${profileCol} FILTER p.email == ${email} return p`;
                const { res, err } = yield this.query(query, txn);
                if (err)
                    throw err;
                return { res: res, err: undefined };
            }
            catch (err) {
                return { res: [], err };
            }
        });
    }
}
const profileRepo = new ProfileRepository(collection_enum_1.COLLECTION.PROFILES);
exports.default = profileRepo;
