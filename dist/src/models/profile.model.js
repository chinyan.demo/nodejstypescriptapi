"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProfileModel = exports.profileID = exports.profileField = void 0;
const joi_1 = __importDefault(require("joi"));
const base_1 = require("./base");
const collection_enum_1 = require("../database/arango/collection.enum");
const permission_1 = require("../constant/permission");
exports.profileField = {
    name: joi_1.default.string().required(),
    email: joi_1.default.string().required(),
    role: joi_1.default.string().valid(permission_1.ROLES.MEMBERS, permission_1.ROLES.PREMIUM).default(permission_1.ROLES.MEMBERS),
};
exports.profileID = joi_1.default.string().pattern(new RegExp(`${collection_enum_1.COLLECTION.PROFILES}/${base_1.uuidv4Regex}`, "i"));
exports.ProfileModel = base_1.baseModel.append(exports.profileField);
