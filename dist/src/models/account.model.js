"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.accountModel = exports.accountID = exports.accountFields = void 0;
const joi_1 = __importDefault(require("joi"));
const base_1 = require("./base");
const collection_enum_1 = require("../database/arango/collection.enum");
const base_2 = require("./base");
exports.accountFields = {
    email: joi_1.default.string().required(),
    password: joi_1.default.object().keys({
        hash: joi_1.default.string().required(),
        salt: joi_1.default.string().required(),
    }),
    profileID: joi_1.default.string().required(),
};
exports.accountID = joi_1.default.string().pattern(new RegExp(`${collection_enum_1.COLLECTION.ACCOUNTS}/${base_2.uuidv4Regex}`, "i"));
exports.accountModel = base_1.baseModel.append(exports.accountFields);
