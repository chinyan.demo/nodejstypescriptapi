"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.conversationModel = exports.conversationID = exports.conversationFields = void 0;
const joi_1 = __importDefault(require("joi"));
const base_1 = require("./base");
const profile_model_1 = require("./profile.model");
const collection_enum_1 = require("../database/arango/collection.enum");
const base_2 = require("./base");
exports.conversationFields = {
    type: joi_1.default.string().valid("private_message", "group_message").required(),
    profileIDs: joi_1.default.array().items(profile_model_1.profileID).required(),
    name: joi_1.default.when("type", {
        is: "group_message",
        then: joi_1.default.string().required(),
        otherwise: joi_1.default.forbidden()
    }),
    adminProfileIDs: joi_1.default.when("type", {
        is: "group_message",
        then: joi_1.default.array().items(profile_model_1.profileID),
        otherwise: joi_1.default.forbidden()
    }),
    unreadMessagesCount: joi_1.default.object().optional(),
    lastMessage: joi_1.default.string().optional(),
};
exports.conversationID = joi_1.default.string().pattern(new RegExp(`${collection_enum_1.COLLECTION.CONVERSATIONS}/${base_2.uuidv4Regex}`, "i"));
exports.conversationModel = base_1.baseModel.append(exports.conversationFields);
