"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.messageModel = exports.messageID = exports.messageFields = void 0;
const joi_1 = __importDefault(require("joi"));
const base_1 = require("./base");
const profile_model_1 = require("./profile.model");
const conversation_model_1 = require("./conversation.model");
const collection_enum_1 = require("../database/arango/collection.enum");
const base_2 = require("./base");
exports.messageFields = {
    conversationID: conversation_model_1.conversationID.required(),
    content: joi_1.default.string().required(),
    senderProfileID: profile_model_1.profileID.required(),
    readBy: joi_1.default.array().items(profile_model_1.profileID).optional()
};
exports.messageID = joi_1.default.string().pattern(new RegExp(`${collection_enum_1.COLLECTION.MESSAGES}/${base_2.uuidv4Regex}`, "i"));
exports.messageModel = base_1.baseModel.append(exports.messageFields);
