"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postModel = exports.postID = exports.postFields = void 0;
const joi_1 = __importDefault(require("joi"));
const base_1 = require("./base");
const base_2 = require("./base");
const collection_enum_1 = require("../database/arango/collection.enum");
exports.postFields = {
    content: joi_1.default.string().required(),
};
exports.postID = joi_1.default.string().pattern(new RegExp(`${collection_enum_1.COLLECTION.POSTS}/${base_2.uuidv4Regex}`, "i"));
exports.postModel = base_1.baseModel.append(exports.postFields);
