"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.baseModel = exports.uuidv4Regex = void 0;
const joi_1 = __importDefault(require("joi"));
exports.uuidv4Regex = "[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}";
exports.baseModel = joi_1.default.object().keys({
    _id: joi_1.default.string().required(),
    _key: joi_1.default.string().required().pattern(new RegExp(exports.uuidv4Regex, "i")),
    active: joi_1.default.boolean().required(),
    createdAt: joi_1.default.string().required(),
    createdBy: joi_1.default.string().required(),
    updatedAt: joi_1.default.string().required(),
    updatedBy: joi_1.default.string().required(),
});
