"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.startServerWithCluster = exports.startServer = void 0;
const koa_1 = __importDefault(require("koa"));
const koa_router_1 = __importDefault(require("koa-router"));
const cors_1 = __importDefault(require("@koa/cors"));
const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
require("dotenv/config");
const api_1 = require("./api");
const setup_1 = require("./database/arango/setup");
const http_1 = require("http");
const socket_io_1 = require("socket.io");
const swagger_1 = require("./helper/swagger");
const websocket_1 = require("./helper/websocket");
const config_1 = __importDefault(require("./constant/config"));
const cluster_adapter_1 = require("@socket.io/cluster-adapter");
const sticky_1 = require("@socket.io/sticky");
const { PORT, DOMAIN } = config_1.default;
const getServer = () => __awaiter(void 0, void 0, void 0, function* () {
    const app = new koa_1.default({ proxy: true });
    app.use((0, cors_1.default)());
    app.use((0, koa_bodyparser_1.default)());
    const router = new koa_router_1.default();
    router.get("", (ctx) => __awaiter(void 0, void 0, void 0, function* () {
        // const params = ctx.params
        ctx.body = {
            code: 200,
            msg: "up and running ...",
            //finger print variable
            IP: ctx.request.headers['x-forwarded-for'],
            UserAgent: ctx.request.headers['user-agent'],
            secChUa: ctx.request.headers['sec-ch-ua'],
            AcceptLanguage: ctx.request.headers['accept-language'],
            upgradeInsecureRequests: ctx.request.headers['upgrade-insecure-requests'],
            // params
        };
    }));
    app.use(router.routes());
    yield (0, setup_1.setupCollection)();
    yield (0, api_1.RouterV1)(app);
    yield (0, swagger_1.swaggerMiddleware)(app);
    return new http_1.Server(app.callback());
});
const startServer = () => __awaiter(void 0, void 0, void 0, function* () {
    const httpServer = yield getServer();
    const io = new socket_io_1.Server(httpServer, {
        cors: {
            origin: "*",
            methods: ["GET", "POST"],
            // transports:["websocket"]
        },
        path: '/websocket'
    });
    (0, websocket_1.websocketMiddleware)(io);
    httpServer.listen(parseInt(String(PORT)), DOMAIN, () => {
        console.log(`app started at ${DOMAIN}:${PORT}`);
    });
});
exports.startServer = startServer;
const startServerWithCluster = () => __awaiter(void 0, void 0, void 0, function* () {
    const httpServer = yield getServer();
    const io = new socket_io_1.Server(httpServer, {
        cors: {
            origin: "*",
            methods: ["GET", "POST"],
            // transports:["websocket"]
        },
        path: '/websocket'
    });
    io.adapter((0, cluster_adapter_1.createAdapter)());
    (0, sticky_1.setupWorker)(io);
    (0, websocket_1.websocketMiddleware)(io);
    httpServer.listen(parseInt(String(PORT)), DOMAIN, () => {
        console.log(`app started at ${DOMAIN}:${PORT}`);
    });
});
exports.startServerWithCluster = startServerWithCluster;
exports.default = getServer;
