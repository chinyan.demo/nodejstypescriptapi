"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PERMISSION = exports.ROLES = exports.OPEN_TO_ALL = void 0;
exports.OPEN_TO_ALL = "OPEN_TO_ALL";
var ROLES;
(function (ROLES) {
    ROLES["PUBLIC"] = "PUBLIC";
    ROLES["MEMBERS"] = "MEMBERS";
    ROLES["PREMIUM"] = "PREMIUM";
})(ROLES || (exports.ROLES = ROLES = {}));
exports.PERMISSION = {
    MEMBERS_ONLY: [ROLES.MEMBERS]
};
