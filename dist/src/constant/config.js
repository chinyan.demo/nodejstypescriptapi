"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const joi_1 = __importDefault(require("joi"));
const configModel = joi_1.default.object().keys({
    APP_ENV: joi_1.default.string().required().default("development"),
    PROTOCOL: joi_1.default.string().required().default("http"),
    DOMAIN: joi_1.default.string().required().default("127.0.0.1"),
    PORT: joi_1.default.string().required().default("8891"),
    ARANGODB_URL: joi_1.default.string().required().default("http://127.0.0.1:8529"),
    ARANGODB_DBNAME: joi_1.default.string().required().default("video_chat_db"),
    ARANGODB_USERNAME: joi_1.default.string().required().default("root"),
    ARANGODB_PASSWORD: joi_1.default.string().required().default("root"),
    JWT_SECRET: joi_1.default.string().required().default("MDI5MQ=="),
    JWT_LIFESPAN: joi_1.default.number().required().default(3600000),
    NO_OF_CLUSTER_WORKERS: joi_1.default.number().optional()
});
const envVar = {
    APP_ENV: process.env["APP_ENV"],
    PROTOCOL: process.env["PROTOCOL"],
    DOMAIN: process.env["DOMAIN"],
    PORT: process.env["PORT"],
    ARANGODB_URL: process.env["ARANGODB_URL"],
    ARANGODB_DBNAME: process.env["ARANGODB_DBNAME"],
    ARANGODB_USERNAME: process.env["ARANGODB_USERNAME"],
    ARANGODB_PASSWORD: process.env["ARANGODB_PASSWORD"],
    JWT_SECRET: process.env["JWT_SECRET"],
    JWT_LIFESPAN: process.env["JWT_LIFESPAN"],
};
const { value: sanitizedConfig, error: getConfigErr } = configModel.validate(envVar);
if (getConfigErr) {
    console.log(getConfigErr);
    process.exit(0);
}
const config = sanitizedConfig;
exports.default = config;
