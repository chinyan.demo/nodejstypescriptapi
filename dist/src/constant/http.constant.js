"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HTTP = void 0;
var HTTP;
(function (HTTP) {
    HTTP["POST"] = "POST";
    HTTP["GET"] = "GET";
    HTTP["PUT"] = "PUT";
    HTTP["PATCH"] = "PATCH";
    HTTP["DELETE"] = "DELETE";
})(HTTP || (exports.HTTP = HTTP = {}));
