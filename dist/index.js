"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = __importStar(require("./src/server"));
const cluster_1 = __importDefault(require("cluster"));
const os_1 = require("os");
const process_1 = require("process");
const sticky_1 = require("@socket.io/sticky");
const cluster_adapter_1 = require("@socket.io/cluster-adapter");
const isCluster = process_1.argv.find((argument) => argument.trim() == "cluster") == "cluster";
if (isCluster) {
    if (cluster_1.default.isPrimary) {
        void (() => __awaiter(void 0, void 0, void 0, function* () {
            const httpServer = yield (0, server_1.default)();
            (0, sticky_1.setupMaster)(httpServer, {
                loadBalancingMethod: "least-connection", // either "random", "round-robin" or "least-connection"
            });
            (0, cluster_adapter_1.setupPrimary)();
            (0, os_1.cpus)().forEach(() => { cluster_1.default.fork(); });
            cluster_1.default.setupPrimary({
                serialization: "advanced",
            });
            cluster_1.default.on('online', (worker) => console.log(`Worker ${worker.process.pid} is online`));
            cluster_1.default.on('exit', (worker, exitCode) => {
                console.log(`Worker ${worker.process.pid} exited with code ${exitCode}`);
                console.log(`Starting a new worker`);
                cluster_1.default.fork();
            });
        }))();
    }
    else if (cluster_1.default.isWorker) {
        void (() => __awaiter(void 0, void 0, void 0, function* () { yield (0, server_1.startServerWithCluster)(); }))();
    }
}
else {
    void (() => __awaiter(void 0, void 0, void 0, function* () { yield (0, server_1.startServer)(); }))();
}
