import Repository from "../../database/arango/repository"
import {COLLECTION} from "../../database/arango/collection.enum"
import { Transaction } from "arangojs/transaction";
import { aql } from "arangojs";
import { IProfile } from "../../models/profile.model";
import arangoDB from "../../database/arango";

class ProfileRepository extends Repository{
    constructor(collection:string){
        super(collection)
    }

    async fetchByEmail(email:string,txn?:Transaction):Promise<{res:Array<IProfile>,err:any}>{
        try{
            const profileCol = (await arangoDB.getInstance()).collection(COLLECTION.PROFILES)
            const query = aql`FOR p IN ${profileCol} FILTER p.email == ${email} return p`;
            const {res,err} = await this.query(query,txn)
            if(err) throw err
            return {res:res as Array<any>,err:undefined}
        }catch(err){
            return {res:[],err}
        }
    }
}

const profileRepo = new ProfileRepository(COLLECTION.PROFILES)
export default profileRepo;