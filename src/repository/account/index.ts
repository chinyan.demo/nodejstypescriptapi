import Repository from "../../database/arango/repository"
import {COLLECTION} from "../../database/arango/collection.enum"
import { aql } from "arangojs"
import arangoDB from "../../database/arango"
import { Transaction } from "arangojs/transaction"
import { IAccount } from "../../models/account.model"

class AccountRepository extends Repository{
    constructor(collection:string){
        super(collection)
    }

    async fetchAccByEmail(email:string,txn?:Transaction):Promise<{res:Array<IAccount>,err:any}>{
        try{
            const accountsCol = (await arangoDB.getInstance()).collection(COLLECTION.ACCOUNTS)
            const query = aql`FOR acc IN ${accountsCol} FILTER acc.email == ${email} RETURN acc`
            const {res,err} = await this.query(query,txn)
            if(err) throw err
            return {res: res as Array<any>,err:undefined}
        }catch(error){
            return {res:[],err:error}
        }
    }
}

const accountRepo = new AccountRepository(COLLECTION.ACCOUNTS)
export default accountRepo;