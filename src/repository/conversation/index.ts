import Repository from "../../database/arango/repository"
import {COLLECTION} from "../../database/arango/collection.enum"
import arangoDB from "../../database/arango"
import { Transaction } from "arangojs/transaction"
import { aql } from "arangojs"

class ConversationRepository extends Repository{
    constructor(collection:string){
        super(collection)
    }

    async list(profileID:string,txn?:Transaction){
        try{
            const conversationCollection = (await arangoDB.getInstance()).collection(COLLECTION.CONVERSATIONS)
            const query = aql`
                FOR c IN ${conversationCollection}
                FILTER ${profileID} IN c.profileIDs
                LET users = (
                    FOR p IN c.profileIDs FILTER p!= ${profileID}
                    RETURN {
                        profileID: p,
                        name: DOCUMENT(p).name
                    })
                LET name = (c.name)? c.name :(FOR p IN c.profileIDs FILTER p!=${profileID} RETURN DOCUMENT(p).name)[0]
                RETURN MERGE(c,{name:name,users:users})
            `

            const result = await super.query(query,txn)
            return result;
        }catch(error){
            return {err:error, res:undefined}
        }
    }
}

const conversationRepo = new ConversationRepository(COLLECTION.CONVERSATIONS)
export default conversationRepo;