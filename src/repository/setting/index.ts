import Repository from "../../database/arango/repository"
import {COLLECTION} from "../../database/arango/collection.enum"
import { Transaction } from "arangojs/transaction";
import { aql } from "arangojs";
import { IProfile } from "../../models/profile.model";
import arangoDB from "../../database/arango";

class SettingsRepository extends Repository{
    constructor(collection:string){
        super(collection)
    }

    async upsertSwagger(doc:any,txn?:Transaction):Promise<{res:Array<{[key:string]:any}>,err:any}>{
        try{
            const settingsCol = (await arangoDB.getInstance()).collection(COLLECTION.SETTING)
            const query = aql`UPSERT { _key: "swagger" }
            INSERT { _key: "swagger", documentation: ${doc} }
            UPDATE {  documentation: ${doc} }
            IN ${settingsCol} OPTIONS { keepNull: false }`;
            const {res,err} = await this.query(query,txn)
            if(err) throw err
            return {res:res as Array<any>,err:undefined}
        }catch(err){
            return {res:[],err}
        }
    }
}

const settingsRepo = new SettingsRepository(COLLECTION.SETTING)
export default settingsRepo;