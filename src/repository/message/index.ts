import Repository from "../../database/arango/repository"
import {COLLECTION} from "../../database/arango/collection.enum"
import { Transaction } from "arangojs/transaction"
import { aql } from "arangojs";
import arangoDB from "../../database/arango";

class MessageRepository extends Repository{
    constructor(collection:string){
        super(collection)
    }

    async list(filter:{conversationID:string},txn:Transaction){
        try{
            const {conversationID} = filter;
            const messageCollection = (await arangoDB.getInstance()).collection(COLLECTION.MESSAGES)
            const query = aql`
                FOR m IN ${messageCollection}
                FILTER m.conversationID == ${conversationID}
                RETURN m    
            `;

            const {res,err} = await super.query(query,txn)
            if(err) throw err
            return {res,err:undefined}
        }catch(error){
            return {err:error,res:undefined}
        }
    }

    async updateReadBy(conversationID:string,profileID:string,txn?:Transaction){
        try{
            const messageCollection = (await arangoDB.getInstance()).collection(COLLECTION.MESSAGES)
            const query = aql`
                FOR m IN ${messageCollection}
                FILTER m.conversationID == ${conversationID}
                LET readBy = (m.readBy) ? m.readBy : []
                LET updatedReadBy = APPEND(readBy,${profileID},true)
                UPDATE m WITH {readBy:updatedReadBy}
                IN ${messageCollection}
                RETURN NEW    
            `;

            const {res,err} = await super.query(query,txn)
            if(err) throw err;

            return {res,err:undefined}
        }catch(error){
            return {err:error}
        }
    }

    async listMessageByConversationID(conversationID:string,pagination:{offset:number,limit:number},txn?:Transaction){
        try{
            const {offset,limit} = pagination
            let _offset = offset | 0;
            let _limit = limit | 100;
            const messageCollection = (await arangoDB.getInstance()).collection(COLLECTION.MESSAGES)
            const query = aql`
                FOR data IN (
                    FOR m IN ${messageCollection}
                    FILTER m.conversationID == ${conversationID}
                    SORT m.createdAt DESC
                    LIMIT ${_offset} , ${_limit}
                    RETURN merge(m, {
                        sender:{
                            name: DOCUMENT(m.senderProfileID).name, 
                            profileID: m.senderProfileID
                        }
                    })
                )
                SORT data.createdAt ASC
                RETURN data
            `

            const {res,err} = await super.query(query,txn);
            if(err) throw err;

            return {res,err:undefined}
        }catch(error){
            return {res:undefined,err:error}
        }
    }
}

const messageRepo = new MessageRepository(COLLECTION.MESSAGES)
export default messageRepo;