import Repository from "../../database/arango/repository";
import { COLLECTION } from "../../database/arango/collection.enum";
import { Transaction } from "arangojs/transaction";
import { aql } from "arangojs";
import { IPosts } from "../../models/post.model";
import arangoDB from "../../database/arango";

class PostRepository extends Repository {
    constructor(collection: string) {
        super(collection);
    }

    async fetchByEmail(
        email: string,
        txn?: Transaction
    ): Promise<{ res: Array<IPosts>; err: any }> {
        try {
            const profileCol = (await arangoDB.getInstance()).collection(
                COLLECTION.PROFILES
            );
            const query = aql`FOR p IN ${profileCol} FILTER p.email == ${email} return p`;
            const { res, err } = await this.query(query, txn);
            if (err) throw err;
            return { res: res as Array<any>, err: undefined };
        } catch (err) {
            return { res: [], err };
        }
    }

    /**
     * fetchMany
     * @param {string|AqlLiteral} query
     * @param {Transaction} txn
     * @returns {Promise<IQuery>}
     */
    async fetchManyWithFilters(filters: any, txn?: Transaction): Promise<any> {
        try {
            const postCol = (await arangoDB.getInstance()).collection(
                COLLECTION.POSTS
            );
            // const db = await arangoDB.getInstance();
            // const collection = db.collection(this.collection);
            const q = await aql`
                        FOR c in ${postCol}
                        {{filters}}
                        RETURN c
                    `;
            if (filters) {
                q.query = q.query.replace(
                    "{{filters}}",
                    `FILTER c._id == "${filters}"`
                );
            } else {
                q.query = q.query.replace("{{filters}}", "");
            }
            if (txn) {
                return await txn.step(async () => {
                    const result = await super.query(q);
                    return { res: result, err: undefined };
                });
            } else {
                const result = await super.query(q);
                return { res: result, err: undefined };
            }
        } catch (err) {
            // txn?.abort()
            return { res: undefined, err };
        }
    }
}

const postRepo = new PostRepository(COLLECTION.POSTS);
export default postRepo;
