import Repository from "../../database/arango/repository";
import { EDGE_COLLECTION } from "../../database/arango/collection.enum";
import { aql } from "arangojs";
import arangoDB from "../../database/arango";
import { Transaction } from "arangojs/transaction";

class FriendRepository extends Repository {
    constructor(collection: string) {
        super(collection);
    }

    async fetch(payload: any, txn?: Transaction): Promise<any> {
        try {
            const q = aql`
            FOR f in friends
            FILTER f._from == ${payload.currentID} AND f._to == ${payload.friendID}
            RETURN f
                    `;

            const result = await super.query(q, txn);
            return result;
        } catch (err) {
            return { res: undefined, err };
        }
    }

    async fetchManyWithFilters(filters: {profileID:string}, txn?: Transaction): Promise<any> {
        try {
            const {profileID} = filters
            const q = aql`
                FOR friend
                    IN 1..1
                    OUTBOUND ${profileID}
                    GRAPH friends
                RETURN friend
                `;

            const result = await super.query(q, txn);
            return result;
        } catch (err) {
            return { res: undefined, err };
        }
    }
}

const friendRepo = new FriendRepository(EDGE_COLLECTION.FRIENDS);
export default friendRepo;
