import { IProfile } from "./profile.model"

export type JwtData =  IProfile&{accountID:string, iat:string, clientIP:string}