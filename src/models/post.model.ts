import joi from "joi";
import { IBaseModel, baseModel } from "./base";
import { uuidv4Regex } from "./base";
import { COLLECTION } from "../database/arango/collection.enum";

export interface IPosts extends IBaseModel {
    content: string;
}

export const postFields = {
    content: joi.string().required(),
};

export const postID = joi.string().pattern(new RegExp(`${COLLECTION.POSTS}/${uuidv4Regex}`,"i"))

export const postModel = baseModel.append(postFields);
