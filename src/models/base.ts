import { Middleware } from "koa";
import joi, { ObjectSchema } from "joi";
import { HTTP } from "../constant/http.constant";

export type FetchedDataControl = {tokenPath:string, fetched: { path:string, fetchedDataCollection:string}}
export type PayloadDataControl = {tokenPath:string, payloadPath:string}
export type AccessControl = PayloadDataControl|FetchedDataControl

export interface IRouteMatrix {
    method: HTTP.GET | HTTP.POST | HTTP.PUT | HTTP.DELETE | HTTP.PATCH;
    path: string;
    permission: { roles: Array<string>; useJwt: boolean };
    access: Array<AccessControl>;
    validator: ObjectSchema;
    transactions?:Array<string>;
    controller: Middleware;
    description: string;
    sampleResponse?: any;
}

export interface IGraphDefinition {
    name:string,
    edgeDefinition:Array<{collection:string,from:string,to:string}>
}

export interface IBaseModel {
    _id: string;
    _key: string;

    active: boolean;
    createdAt: string;
    createdBy: string;
    updatedAt: string;
    updatedBy: string;
}

export const uuidv4Regex ="[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}";

export const baseModel = joi.object().keys({
    _id: joi.string().required(),
    _key: joi.string().required().pattern(new RegExp(uuidv4Regex, "i")),

    active: joi.boolean().required(),
    createdAt: joi.string().required(),
    createdBy: joi.string().required(),
    updatedAt: joi.string().required(),
    updatedBy: joi.string().required(),
});
