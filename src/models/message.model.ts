import joi from "joi"
import { IBaseModel, baseModel } from "./base";
import { profileID } from "./profile.model";
import { conversationID } from "./conversation.model";
import { COLLECTION } from "../database/arango/collection.enum";
import { uuidv4Regex } from "./base";

export interface IMessage extends IBaseModel {
    messageID?: string,
    senderName?:string,
    conversationID: string;
    content: string;
    senderProfileID: string;
    readBy?: Array<string>
}

export const messageFields = {
    conversationID: conversationID.required(),
    content: joi.string().required(),
    senderProfileID: profileID.required(),
    readBy: joi.array().items(profileID).optional()
};

export const messageID = joi.string().pattern(new RegExp(`${COLLECTION.MESSAGES}/${uuidv4Regex}`,"i"))

export const messageModel = baseModel.append(messageFields);