import joi from "joi";
import { IBaseModel, baseModel } from "./base";
import { COLLECTION } from "../database/arango/collection.enum";
import { uuidv4Regex } from "./base";

export interface IAccount extends IBaseModel {
    email: string;
    password: { hash: string; salt: string };
    profileID: string;
}

export const accountFields = {
    email: joi.string().required(),
    password: joi.object().keys({
        hash: joi.string().required(),
        salt: joi.string().required(),
    }),
    profileID: joi.string().required(),
};

export const accountID = joi.string().pattern(new RegExp(`${COLLECTION.ACCOUNTS}/${uuidv4Regex}`,"i"))

export const accountModel = baseModel.append(accountFields);
