import joi from "joi"
import { IBaseModel, baseModel, uuidv4Regex } from "./base"
import { COLLECTION } from "../database/arango/collection.enum"
import { ROLES } from "../constant/permission"

export interface IProfile extends IBaseModel{
    name:string
    email:string
    role:string
}

export const profileField = {
    name:joi.string().required(),
    email:joi.string().required(),
    role: joi.string().valid(ROLES.MEMBERS,ROLES.PREMIUM).default(ROLES.MEMBERS),
}

export const profileID = joi.string().pattern(new RegExp(`${COLLECTION.PROFILES}/${uuidv4Regex}`,"i"))

export const ProfileModel = baseModel.append(profileField)