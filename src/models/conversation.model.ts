import joi, { number } from "joi";
import { IBaseModel, baseModel } from "./base";
import { profileID } from "./profile.model";
import { COLLECTION } from "../database/arango/collection.enum";
import { uuidv4Regex } from "./base";

type conversationType = "private_message"|"group_message"

export interface IConversation extends IBaseModel {
    type: conversationType;
    profileIDs:Array<string>
    name?:string
    adminProfileIDs?: Array<string>
    unreadMessagesCount:{ [key:string] : number},
    lastMessage?:string
}

export const conversationFields = {
    type: joi.string().valid("private_message","group_message").required(),
    profileIDs: joi.array().items(profileID).required(),
    name: joi.when("type",{
        is: "group_message",
        then: joi.string().required(),
        otherwise: joi.forbidden()
    }),
    adminProfileIDs: joi.when("type",{
        is: "group_message",
        then: joi.array().items(profileID),
        otherwise: joi.forbidden()
    }),
    unreadMessagesCount: joi.object().optional(),
    lastMessage: joi.string().optional(),
}

export const conversationID = joi.string().pattern(new RegExp(`${COLLECTION.CONVERSATIONS}/${uuidv4Regex}`,"i"))

export const conversationModel = baseModel.append(conversationFields);

