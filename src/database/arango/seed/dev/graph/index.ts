import fs from "fs";
import { IGraphDefinition } from "../../../../../models/base";

export const getGraphDef = async() => {
    const dir = fs.readdirSync(__dirname)

    const graphDefinitions:Array<IGraphDefinition> = []
    dir.forEach(async (file:string) =>{
        if(/.graph./.test(file)){ 
            const graphDef = require(`./${file.replace(/.ts/,"").replace(/.js/,"")}`)["default"]
            graphDefinitions.push(graphDef)
        }
    });
    return graphDefinitions
}
