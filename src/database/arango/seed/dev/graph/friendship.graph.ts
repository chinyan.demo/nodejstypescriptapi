import { IGraphDefinition } from "../../../../../models/base";
import { COLLECTION, EDGE_COLLECTION } from "../../../collection.enum";

const FriedshipGraph:IGraphDefinition = {
    name:"friendship",
    edgeDefinition:[{
        collection: EDGE_COLLECTION.FRIENDS,
        from: COLLECTION.PROFILES,
        to: COLLECTION.PROFILES,
    }]
}

export default FriedshipGraph