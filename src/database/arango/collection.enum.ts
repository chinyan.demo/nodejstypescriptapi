export enum COLLECTION {
  PROFILES = "profiles",
  ACCOUNTS = "accounts",
  POSTS = "posts",
  SETTING = "settings",
  CONVERSATIONS = "conversations",
  MESSAGES = "messages"
}

export enum EDGE_COLLECTION {
  FRIENDS = "friends",
}
