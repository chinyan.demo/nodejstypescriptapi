import {COLLECTION,EDGE_COLLECTION} from "./collection.enum"
import arangoDB from "./index"
import { getGraphDef } from "./seed/dev/graph" 
import { Database, aql } from "arangojs"
import { useTransaction } from "./transaction"



export const setupCollection = async():Promise<undefined> => {
    const collection = Object.values(COLLECTION)
    const edge = Object.values(EDGE_COLLECTION)
    await arangoDB.setupCollections(collection,edge)
    const graphDefs = await getGraphDef()
    await arangoDB.createGraph(graphDefs)
}

export const seedData = async(data:{collection:string,documents:Array<any&{_id:string,_key:string}>}):Promise<void> =>{
    if(data){
        const db: Database = await arangoDB.getInstance();
        const {collection,documents} = data;
        if(collection){
            const col = db.collection(collection)
            if(documents&&documents.length>0){
              
                    documents.forEach( async doc => {
                        if(doc._id&& doc._key){
                            try{

                                    // const query = aql`
                                    // UPSERT { _key: ${doc._key} }
                                    // INSERT ${ doc }
                                    // UPDATE ${ doc }
                                    // IN ${col} OPTIONS { keepNull: false }`;
                     
                                        // const res = await db.query(query)
                                        // await res.all();
                                        if(await col.documentExists(doc._key) == false){
                                            await col.save(doc)
                                        }
                     
                            }catch(error){
                                console.log(error)
                                throw error
                            }
                        }
                    });
               
            }
        }
    }
}