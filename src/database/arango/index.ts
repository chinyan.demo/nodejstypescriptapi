import  { Database } from "arangojs"
import { EdgeDefinitionOptions } from "arangojs/graph";
import config from "../../constant/config";

let instance:Database|undefined;
const url:string = String(config.ARANGODB_URL)
const databaseName:string =  String(config.ARANGODB_DBNAME)
const username:string =  String(config.ARANGODB_USERNAME)
const password:string =  String(config.ARANGODB_PASSWORD)



class ArangoDB{
    private credential:{url:string,auth:{username:string,password:string}}
    private databaseName:string

    constructor(url:string,databaseName:string,username:string,password:string){
        this.credential = {url,auth:{username,password}}
        this.databaseName = databaseName;
    }

    async getInstance():Promise<Database>{
        let db;
        if(!instance){
            const tempDB = new Database(this.credential)
            const allDBs = await tempDB.listDatabases()
            if(!allDBs.find(name=>name==this.databaseName)){
                db = await tempDB.createDatabase(this.databaseName)
            }else{
                db = tempDB.database(this.databaseName)
            }
            instance = db;
            // Object.freeze(instance);
        }
        return instance
    }

    async setupCollections(collections:Array<string>,edge_collections:Array<string>):Promise<undefined>{
        try{
            const db = await this.getInstance();
            for(const collection of collections){
                try{
                    await db.collection(collection).get()
                }catch(notExistException){
                     await db.createCollection(collection)
                }
            } 
            for(const edge_collection of edge_collections){
                try{
                    await db.collection(edge_collection).get()
                }catch(notExistException){
                     await db.createEdgeCollection(edge_collection)
                }
            } 
        }catch(error){
            console.log(JSON.stringify(error,undefined,2))
        }
    }

    async createSearchView(searchViews:Array<any>){
        const db = await this.getInstance();
        for(const searchView of searchViews){
            await db.createView(searchView,{type:"arangosearch"})
        }
    }

    async createGraph(graphs:Array<{name:string,edgeDefinition:Array<EdgeDefinitionOptions>}>){
        const db = await this.getInstance();
        for(const graph of graphs){
            if(graph){
                const {name,edgeDefinition} = graph
                if(!await db.graph(name).exists())await db.createGraph(name,edgeDefinition)
            }
        } 
    }

    async seedData(seedMatrices:Array<any>){
        const db = await this.getInstance();
        for(const seedMatrix of seedMatrices){
            const {collection,data} = seedMatrix;
            const col = db.collection(collection)
            for(const doc of data){
                await col.save(doc);
            }
        }
    }
}


const arangoDB = new ArangoDB(url,databaseName,username,password);
export default arangoDB


