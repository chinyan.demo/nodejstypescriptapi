import { Database } from "arangojs";
import { DocumentMetadata, Document } from "arangojs/documents";
import { Transaction } from "arangojs/transaction";
import {  AqlQuery, aql } from "arangojs/aql";
import arangoDB from ".";

interface ICreate {
    res: DocumentMetadata | undefined;
    err: any;
}

interface IUpdate {
    res:
        | DocumentMetadata
        | (undefined & {
              new?: Document;
              old?: Document;
          })
        | undefined;
    err: any;
}

interface IDelete {
    res:
        | DocumentMetadata
        | (undefined & {
              old?: Document;
          })
        | undefined;
    err: any;
}

interface IFetch {
    res: Document;
    err: any;
}

interface IQuery {
    res: Array<Document> | undefined;
    err: any;
}

class ArangoRepository {
    // private db:Database;
    private collection: string;
    constructor(collection: string) {
        this.collection = collection;
    }

    /**
     * create
     * @param {*} doc
     * @param {Transaction} txn
     * @returns {Promise<ICreate>}
     */
    async create(doc: any, txn?: Transaction): Promise<ICreate> {
        try {
            const db: Database = await arangoDB.getInstance();
            if (txn) {
                return await txn.step(async () => {
                    const res = await db.collection(this.collection).save(doc);
                    return { res, err: undefined };
                });
            } else {
                const res = await db.collection(this.collection).save(doc);
                return { res, err: undefined };
            }
        } catch (err) {
            return { res: undefined, err };
        }
    }

    /**
     * fetch
     * @param {string} docID
     * @param {Transaction} txn
     * @returns {Promise<IFetch>}
     */
    async fetch(docID: string, txn?: Transaction): Promise<IFetch> {
        try {
            const db: Database = await arangoDB.getInstance();
            if (txn) {
                return await txn.step(async () => {
                    const doc = await db
                        .collection(this.collection)
                        .document({ _id: docID });
                    return { res: doc, err: undefined };
                });
            } else {
                const doc = await db
                    .collection(this.collection)
                    .document({ _id: docID });
                return { res: doc, err: undefined };
            }
        } catch (err) {
            return { res: undefined, err };
        }
    }

    /**
     * update
     * @param {string} docID
     * @param {*} doc
     * @param {Transaction} txn
     * @returns {Promise<IUpdate>}
     */
    async update(docID: string, doc: any, txn?: Transaction): Promise<IUpdate> {
        try {
            const db: Database = await arangoDB.getInstance();
            if (txn) {
                return await txn.step(async () => {
                    const res = await db
                        .collection(this.collection)
                        .update({ _id: docID }, doc);
                    return { res, err: undefined };
                });
            } else {
                const res = await db
                    .collection(this.collection)
                    .update({ _id: docID }, doc);
                return { res, err: undefined };
            }
        } catch (err) {
            return { res: undefined, err };
        }
    }

    /**
     * delete
     * @param {string} docID
     * * @param {Transaction} txn
     * @returns {Promise<IDelete>}
     */
    async delete(docID: string, txn?: Transaction): Promise<IDelete> {
        try {
            const db: Database = await arangoDB.getInstance();
            if (txn) {
                return await txn.step(async () => {
                    const res = await db
                        .collection(this.collection)
                        .remove({ _id: docID });
                    return { res, err: undefined };
                });
            } else {
                const res = await db
                    .collection(this.collection)
                    .remove({ _id: docID });
                return { res, err: undefined };
            }
        } catch (err) {
            return { res: undefined, err };
        }
    }

     /**
     * deleteByFilter
     * @param {{[key:string]:any}} filter
     * * @param {Transaction} txn
     * @returns {Promise<IDelete>}
     */
     async deleteByFilter(filter: {[key:string]:any}, txn?: Transaction): Promise<{res?:any,err?:any}> {
        try {
            const db: Database = await arangoDB.getInstance();
            const keys = Object.keys(filter);
            if(keys.length==0) throw "filter is required";
            const collection = db.collection(this.collection)
            const filterQuery = keys.reduce((acc,next)=> acc+`FILTER c.${next} == "${filter[next]}" `,"")
            const query = aql`
                FOR c IN ${collection}
                {{filterQuery}}
                REMOVE c IN ${collection} 
                RETURN OLD
            `
            query.query = query.query.replace(/{{filterQuery}}/,filterQuery)
            if(keys.length==0) throw "filter is required";
            if (txn) {
                return await txn.step(async () => {
                    const res = await (await db.query(query)).all()
                    return { res, err: undefined };
                });
            } else {
                const res = await (await db.query(query)).all()
                return { res, err: undefined };
            }
        } catch (err) {
            return { res: undefined, err };
        }
    }

    /**
     * query
     * @param {string|AqlLiteral} query
     * @param {Transaction} txn
     * @returns {Promise<IQuery>}
     */
    async query(query: AqlQuery, txn?: Transaction): Promise<IQuery> {
        try {
            const db: Database = await arangoDB.getInstance();
            if (txn) {
                return await txn.step(async () => {
                    const crusor = await db.query(query);
                    const res = await crusor.all();
                    return { res, err: undefined };
                });
            } else {
                const crusor = await db.query(query);
                const res = await crusor.all();
                return { res, err: undefined };
            }
        } catch (err) {
            // txn?.abort()
            return { res: undefined, err };
        }
    }

    /**
     * fetchMany
     * @param {string|AqlLiteral} query
     * @param {Transaction} txn
     * @returns {Promise<IQuery>}
     */
    async fetchMany(txn?: Transaction): Promise<IQuery> {
        try {
            const db: Database = await arangoDB.getInstance();
            const collection = db.collection(this.collection);
            const string = await aql`
                        FOR c in ${collection}
                        RETURN c
                    `;

            if (txn) {
                return await txn.step(async () => {
                    const crusor = await db.query(string);
                    const res = await crusor.all();
                    return { res, err: undefined };
                });
            } else {
                const crusor = await db.query(string);
                const res = await crusor.all();
                return { res, err: undefined };
            }
        } catch (err) {
            // txn?.abort()
            return { res: undefined, err };
        }
    }
}

export default ArangoRepository;
