import { Transaction } from "arangojs/transaction";
import arangoDB from ".";

export const getTransaction = async (
    collections: Array<string>
): Promise<Transaction> => {
    const db = await arangoDB.getInstance();
    const txn = await db.beginTransaction(collections);
    return txn;
};

export const useTransaction = async (
    collections: Array<string>,
    callback: (txn: Transaction) => Promise<any>
) => {
    const txn = await getTransaction(collections);
    try {
        // console.table({ txn });
        const { res, err } = await callback(txn);
    
        if (err) throw err;
        await txn.commit();
    
        return { res };
    } catch (error) {
        // console.table({ error, status: (await txn.get()).status });
        await txn.abort();
        return { err: error };
    }
};
