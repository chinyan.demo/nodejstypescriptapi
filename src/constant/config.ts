import "dotenv/config"
import joi from "joi"

interface IConfig {
    APP_ENV:string,
    PROTOCOL: string,
    DOMAIN: string,
    PORT: string,
    ARANGODB_URL: string,
    ARANGODB_DBNAME: string,
    ARANGODB_USERNAME: string,
    ARANGODB_PASSWORD: string,
    JWT_SECRET: string,
    JWT_LIFESPAN: number,
    NO_OF_CLUSTER_WORKERS?: number
}

const configModel = joi.object().keys({
    APP_ENV: joi.string().required().default("development"),
    PROTOCOL: joi.string().required().default("http"),
    DOMAIN:joi.string().required().default("127.0.0.1"),
    PORT:joi.string().required().default("8891"),
    ARANGODB_URL:joi.string().required().default("http://127.0.0.1:8529"),
    ARANGODB_DBNAME:joi.string().required().default("video_chat_db"),
    ARANGODB_USERNAME:joi.string().required().default("root"),
    ARANGODB_PASSWORD:joi.string().required().default("root"),
    JWT_SECRET:joi.string().required().default("MDI5MQ=="),
    JWT_LIFESPAN:joi.number().required().default(3600000),
    NO_OF_CLUSTER_WORKERS:joi.number().optional()
})

const envVar = {
    APP_ENV:process.env["APP_ENV"],
    PROTOCOL: process.env["PROTOCOL"],
    DOMAIN: process.env["DOMAIN"],
    PORT: process.env["PORT"],
    ARANGODB_URL: process.env["ARANGODB_URL"],
    ARANGODB_DBNAME: process.env["ARANGODB_DBNAME"],
    ARANGODB_USERNAME: process.env["ARANGODB_USERNAME"],
    ARANGODB_PASSWORD: process.env["ARANGODB_PASSWORD"],
    JWT_SECRET: process.env["JWT_SECRET"],
    JWT_LIFESPAN: process.env["JWT_LIFESPAN"],
}


const {value:sanitizedConfig, error:getConfigErr} = configModel.validate(envVar)
if(getConfigErr){ console.log(getConfigErr);process.exit(0)}
const config:IConfig = sanitizedConfig as IConfig;

export default config