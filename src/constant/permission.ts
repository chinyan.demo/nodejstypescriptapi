export const OPEN_TO_ALL = "OPEN_TO_ALL"

export enum ROLES {
    PUBLIC = "PUBLIC",
    MEMBERS = "MEMBERS",
    PREMIUM = "PREMIUM"
}

export const PERMISSION  = {
    MEMBERS_ONLY : [ROLES.MEMBERS]
}