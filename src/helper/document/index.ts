import { IBaseModel } from "../../models/base";
import {v4 as uuidv4 } from "uuid"

export const docHelper = <T>(doc:T,collection:string,profileID?:string|undefined): T &IBaseModel =>{
    const _key = uuidv4()
    const _id = `${collection}/${_key}`
    return {
        _id,
        _key,
        ...doc,
        active:true,
        createdBy: profileID||_id,
        createdAt: new Date().toISOString(),
        updatedBy: profileID||_id,
        updatedAt: new Date().toISOString(),
    }

}