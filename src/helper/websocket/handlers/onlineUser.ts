interface ISocketIDs{
    [profileID:string]:Array<string>
}
interface IUsers{
    [socketID:string]:{profileID:string,name:string}
}

class OnlineUsersTracker{
    socketIDs:ISocketIDs;
    onlineUser:IUsers;

    constructor(){
        this.socketIDs = {};
        this.onlineUser = {};
    }

    addOnlineUser(profileID:string,socketID:string,name:string){
        if(profileID&&profileID!="backend"){
            if(!this.socketIDs[profileID]) this.socketIDs[profileID] = [];
            this.socketIDs[profileID].push(socketID);
            this.onlineUser[socketID] = {profileID,name};
            console.log("a user connected.")
            // console.table({socketID,profileID,name});
        }
    }

    getProfileBySocketID(socketID:string){
        return (this.onlineUser[socketID])? JSON.parse(JSON.stringify(this.onlineUser[socketID])) : undefined;
    }

    removeOnlineUser(socketID:string){
        if(this.onlineUser[socketID]){
            const profileID = this.onlineUser[socketID].profileID;
            if(profileID&&profileID!="backend"){
                this.socketIDs[profileID] = this.socketIDs[profileID].filter(id=>id!=socketID);
                delete this.onlineUser[socketID];
                if(this.socketIDs[profileID].length==0){
                    delete this.socketIDs[profileID];
                }
            }
        }
    }

    getSocketIDs(profileID:string){
        return  this.socketIDs[profileID];
    }

    getAllOnlineUser(){
        return Object.values(this.onlineUser)
    }
}

const onlineUsersTracker = new OnlineUsersTracker();
export default onlineUsersTracker;