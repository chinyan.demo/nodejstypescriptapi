import { websocketSend } from "../client"
import { p2pConnectionRequest } from "../validates/peer"

export const peerEventHandler = (data:{senderProfileID:string,topic:string,payload:any,receiverProfileID:string}) => {
        const {senderProfileID,topic,payload,receiverProfileID}= data
        websocketSend(receiverProfileID,topic,payload,senderProfileID)
}