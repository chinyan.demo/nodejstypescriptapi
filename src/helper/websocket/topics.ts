export enum TOPIC  {
    CONNECT = "connection",
    ONLINE="online",
    INFO = "info",
    PRIVATE_MESSAGE = "private message",
    FORWARD_PRIVATE_MESSAGE = "FORWARD_PRIVATE_MESSAGE",
    DISCONNECT = "disconnect",
    CONNECTION_FAILED = "connect_failed",
    ERROR = "error",
    AUTH_ERROR = "auth error",
    CONNECT_ERROR = "connect_error",
    
    JOIN_CONFERENCE = "user-joined-conference",

    PEER_LEFT_CONFERENCE = "user-left-conference",
    CONFERENCE_INVITATION = "conference-invitation",
    CONFERENCE_INVITATION_ACCEPTED = "conference-invitation-accepted",
    CONFERENCE_PEER_OFFERING = "conference-peer-offering",
    CONFERENCE_PEER_ANSWERING = "conference-peer-answering",
    CONFERENCE_ICE_CANDIDATE = "conference-ice-candidates",
    CONFERENCE_ROOM_ONLINE = "conference-room-online",
    CONFERENCE_ROOM_BROADCAST = "conference-room-broadcast",
    CONFERENCE_ROOM_EMIT = "conference-room-emit",
    

    USER_CONNECT = "user-connected",
    USER_DISCONNECTED = "user-disconnected",
    VIDEO_CALL="video call",
    JOIN_VIDEO_CALL="join video call",
    BROADCAST = "broadcast",

    SOCKET_ID = "socketID",
    CALL_USER = "callUser",
    ANSWER_CALL = "answerCall",
    CALL_ACCEPTED = "callAccepted"


}

