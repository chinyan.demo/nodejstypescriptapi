import Joi from "joi"

export const p2pConnectionRequest = Joi.object().keys({
    senderProfileID:Joi.string().required(),
    topic:Joi.string().required(),
    payload: Joi.object().required(),
    receiverProfileID: Joi.string().required()
})