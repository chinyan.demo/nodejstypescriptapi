import { Server as SocketIO, Socket} from "socket.io"
import { TOPIC } from "./topics"
import { validateJWT } from "../cryptography";
import onlineUsersTracker from "./handlers/onlineUser";
import {socket as client } from "./client"
import {ConferenceHandler} from "./handlers/conference"


const jwtMiddleware = async(socket:Socket, next:any) => {
    const {token,profileID} = socket.handshake.auth;
    // verify token
    const  {res,err} = validateJWT(token)
    if(err){
        console.log({err})
        socket.emit(TOPIC.AUTH_ERROR,{code:401, error:"Unauthorized", msg:"token mul-formed"})
        socket.disconnect()
        next(new Error(JSON.stringify({code:401, error:"Unauthorized", msg:"token mul-formed"})))
    }
    
    if(res){
        if(res._id=="backend"){
            // console.log({res})
            next()
        }
        const xForwardedFor = socket.handshake.headers['x-forwarded-for'] 
        const clientIP = (typeof xForwardedFor == "undefined")? socket.handshake.address : (Array.isArray(xForwardedFor)) ? xForwardedFor.shift(): xForwardedFor
        
        if(res?.clientIP!=clientIP&&res._id!="backend"){
            console.log("wrong IP", `${res?.clientIP}!=${clientIP}`)
            socket.emit(TOPIC.AUTH_ERROR,{code:401, error:"Unauthorized", msg:"Invalid IP"})
            socket.disconnect()
            next(new Error(JSON.stringify({code:401, error:"Unauthorized", msg:"Invalid IP"})))
        }
        if(res._id==profileID){
            // console.log("valid user : ",res._id)
            next()
        }else{
            socket.emit(TOPIC.AUTH_ERROR,{code:401, error:"Unauthorized", msg:"invalid user or token"})
            next(new Error(JSON.stringify({code:401, error:"Unauthorized", msg:"invalid user or token"})))
            socket.disconnect()
        }
    }
  };

export const websocketMiddleware = (io:SocketIO) => {

    io.use(jwtMiddleware)

    io.on(TOPIC.CONNECT, (socket:Socket) => {
        const {profileID,name} = socket.handshake.auth;
        // if(profileID!="backend")console.table({
        //     profileID,
        //     transport:socket.conn.transport.name,
        //     socketID:socket.id
        // }); 
        if(profileID!="backend") {
            onlineUsersTracker.addOnlineUser(profileID,socket.id,name);
            // Broadcast To everyone other than connected user
            socket.broadcast.emit(TOPIC.ONLINE,onlineUsersTracker.getAllOnlineUser());
            // Broadcast To connected user
            socket.emit(TOPIC.ONLINE,onlineUsersTracker.getAllOnlineUser());
            socket.emit(TOPIC.SOCKET_ID,socket.id);
        }

        //PRIVATE MESSAGE
        socket.on(TOPIC.FORWARD_PRIVATE_MESSAGE, (data:{content:any,receiverProfileID:string})=>{
            const {content,receiverProfileID} = data
            const socketIDs = onlineUsersTracker.getSocketIDs(receiverProfileID)
            console.log({socketIDs})
            if(socketIDs) {
                socketIDs.forEach( socketID => {
                    socket.to(socketID).emit(TOPIC.PRIVATE_MESSAGE,content);
                })
            }
        });

        //Test video call
        socket.on(TOPIC.CALL_USER, data => {
            const {signalData, callerProfileID , callerName, userToCall} = data;
            console.log(data)
            const sockets = onlineUsersTracker.getSocketIDs(userToCall);
            console.log(sockets)
            if(sockets) sockets.forEach(sktID=>{
                io.to(sktID).emit(TOPIC.CALL_USER,{signalData, callerProfileID , callerName, profileID:userToCall});
            })
        })

        socket.on(TOPIC.ANSWER_CALL,data=>{
            const {to, signal} = data
            console.log({[TOPIC.ANSWER_CALL]:data})
            const sockets = onlineUsersTracker.getSocketIDs(to);
            if(sockets) sockets.forEach(sktID=>{
                io.to(sktID).emit(TOPIC.CALL_ACCEPTED,signal)
            })
        })

        //Online Conference
        ConferenceHandler(socket)

        

        //Disconnect
        socket.on(TOPIC.DISCONNECT, (reason) => {
            console.log(`Socket ${socket.id} disconnected, reason : ${reason}`);
            const profile = onlineUsersTracker.getProfileBySocketID(socket.id);
            onlineUsersTracker.removeOnlineUser(socket.id);
            socket.broadcast.emit(TOPIC.USER_DISCONNECTED,profile)
        })

    }); 
    client.connect()
    // connectWebSocketClient()
}
