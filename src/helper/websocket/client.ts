import  {io as client } from "socket.io-client"
import config from "../../constant/config";
import {TOPIC} from "./topics"
import { createJWT } from "../cryptography";

const {PROTOCOL,DOMAIN,PORT} = config;
export const socket = client(`${PROTOCOL}://${DOMAIN}:${PORT}`,{
    path:"/websocket",
    autoConnect: false,      
    transports:["websocket"],
    auth:{
        token:createJWT({_id:"backend"}).accessToken,
        profileID:"backend",
        name:"backend"
    }
});

socket.on(TOPIC.CONNECT, () => {
    console.log('Connected to the server');
  });
  
socket.on(TOPIC.DISCONNECT, (reason) => {
    console.log('Disconnected from the server:', reason);
});

/**
 * websocketSend
 * @param {string} topic
 * @param {object} message
 * @return {{error:any}}
 */
export const websocketSend = (receiverProfileID:string,topic:string,message:any,senderProfileID?:string):{error:any} => {
    try{
        if(socket.disconnected) socket.connect()
        
        socket.emit(receiverProfileID,{topic,message,senderProfileID,receiverProfileID});
        // console.log({emitted:true})
        socket.on(TOPIC.ERROR,(error)=>{ throw error})

        return {error:undefined}
    }catch(error){
        return {error}
    }
}

/**
 * serverWebsocketSend
 * @param topic 
 * @param conversationID 
 * @param content 
 * @returns 
 */
export const serverWebsocketSend = (topic:string,receiverProfileID:string,content:any)=> {
    try{
        if(socket.disconnected) socket.connect()
        
        // console.log({[topic]:{content,receiverProfileID}})
        socket.emit(topic,{content,receiverProfileID});
        // console.log({emitted:true})
       
        return {success:true}
    }catch(error){
        return {success:false,error}
    }
}
