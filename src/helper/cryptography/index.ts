import crypto from "crypto"
import jwt, { JwtPayload } from "jsonwebtoken"
import config from "../../constant/config"

/**
 * generateOTP
 * @returns {string}
 */
export const generateOTP = ():string => {
    return String(Math.floor(Math.random()*1000)).padStart(4,"0")
}

/**
 * base64String
 * @param {string} str 
 * @returns {string}
 */
export const base64String = (str:string):string => {
    return Buffer.from(str).toString("base64")
}

/**
 * generateSalt
 * @returns {string}
 */
export const generateSalt = ():string => {
    const randStr = generateOTP();
    return base64String(randStr)
}

/**
 * Hash
 * @param {string} str 
 * @returns {{hash:string,salt:string}}
 */
export const createHash = (str:string):{hash:string,salt:string} => {
    const salt = generateSalt();
    const saltedStr = `${salt}${str}`
    const hash = crypto.createHash('sha256').update(saltedStr).digest('hex')
    return {hash, salt}
}

/**
 * validateHash
 * @param {string} hash 
 * @param {string} str 
 * @param {string} salt 
 * @returns {bool}
 */
export const validateHash = (hash:string,salt:string,str:string):boolean => {
    const saltedStr = `${salt}${str}`
    return hash === crypto.createHash('sha256').update(saltedStr).digest('hex')
}

export const hash = (str:string) => crypto.createHash('sha256').update(str).digest('hex')

/**
 * createJWT
 * @param {object} payload 
 * @returns {string}
 */
export const createJWT = (payload:object,):{accessToken:string,refreshToken:string} => {
    const secret = String(config.JWT_SECRET)
    const iat = parseInt(String(config.JWT_LIFESPAN)) + new Date().getTime()
    return {accessToken:jwt.sign({...payload,iat},secret), refreshToken: jwt.sign({...payload,iat:2*iat},secret) }
}

/**
 * validateJWT
 * @param {string} token 
 * @returns {JwtPayload|string}
 */
export const validateJWT = (token:string):{res:any,err:any} => {
    try{
        const secret = String(config.JWT_SECRET)
        const res =  jwt.verify(token,secret)
        return {res,err:undefined}
    }catch(error){
        return {res:undefined,err:error}
    }
}