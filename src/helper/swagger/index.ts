import config from "../../constant/config";
import { koaSwagger }  from "koa2-swagger-ui";
// import path from "path"
import Koa, { Context } from "koa"
// import YAML  from "yamljs"
// import serve from "koa-static";
import mount from "koa-mount";
import generateSwaggerDoc from "./generator"
import settingsRepo from "../../repository/setting";
import { argv } from "process";

export const swaggerMiddleware = async(app:Koa) => {
    // if(config.APP_ENV == "development"||config.APP_ENV == "dev"){
        await settingsRepo.deleteByFilter({_key:"swagger"})
        await generateSwaggerDoc()

        const {res:doc} = await settingsRepo.fetch("swagger")
        app.use(mount("/documentation",async(ctx:Context)=>{ ctx.body = doc.documentation}))
        // app.use(mount("/doc",serve(__dirname)))
        app.use(koaSwagger({
            routePrefix: '/swagger',
            swaggerOptions: {
                // url: `${config.PROTOCOL}://${config.DOMAIN}:${config.PORT}/documentation`, 
                url: (/devServer.ts/i.test(argv[argv.length-1]))? `${config.PROTOCOL}://${config.DOMAIN}:${config.PORT}/documentation` : "https://dev.chatbot.drezy.io/documentation"
                // spec:YAML.load(path.join(__dirname, "swagger.yaml"));
                // spec:doc.documentation
            }
        }));
    // }
}