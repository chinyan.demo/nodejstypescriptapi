// import fs from "fs"
// import camelCase from "camelcase"
import j2s from "joi-to-swagger"
// import Path from 'path';
import config from "../../constant/config";
import {OPEN_TO_ALL} from "../../constant/permission"
import {getRouteMatricesV1} from "../../api/routeMatrices"
import settingsRepo from "../../repository/setting";

interface dataInterface{
    [key: string]:Object
}

interface pathInterFace{
    [key: string]: {
        [key: string]:Object
    };
}

interface definitionsInterface{
    [key: string]:Object
}


const data:dataInterface = {
    "swagger": "2.0",
    "info": {
      "description": "API definition for the Logic API",
      "version": "1.0.0",
      "title": "API Documentation for Development",
    },
    "host": "detecting...",
    "basePath": "/main/api/v1.0",
}

let tags:Array<any> = [];
let paths:pathInterFace = {};
let definitions:definitionsInterface = {};

//get last part after split path by /
const getLastAfterSplit = (path:string) => {
    const parts = path.split("/");
    if (parts.length===1 || !parts.length) return path;
    return (parts[parts.length-1]);
}

const ArrayToString = (array:Array<any>) => {
    let ret = "";
    for (let i=0;i<array.length; i++){
        const g = array[i];
        const type = array[i].type;
        const subType = array[i].subType;
        if (type || subType){
            // ret+=camelCase(type+"-"+subType,{pascalCase: true});
            ret+=type+"-"+subType
        }else{
            // ret+=camelCase(array[i] ,{pascalCase: true});
            ret+=array[i]
        }
        if (i<array.length-1) ret+=", ";
    }
    return ret;
}

const generateSwaggerDoc = async() =>{

    // tags.push({
    //     "name": "PRE-DEFINES",
    //     "description": "Predefined Orgs for test"
    // });
    // paths.Default:Object = {
    //     get: {
    //         tags: ["PRE-DEFINES"],
    //         summary: "useful info to test APIs",
    //         description: "Predefined OWNER Information to test. here is data",
    //         operationId: "Predefined OWNER",
    //         consumes: ["application/json"],
    //         produces: ["application/json"],
    //         parameters: "Default"
    //     }
    // };

  
    //iterate all routes
    const RouteMatrixes = getRouteMatricesV1();
    RouteMatrixes.forEach(r => {
        // const matrix = r.Routes;
        let tag = r.path.split("/")[1]
        // if (r.Group.search(/\/dem\//g)>=0) tag = "dem_" + tag;
        // if (r.Group.search(/\/ecom\//g)>=0) tag = "ecom_" + tag;
        // if (r.Group.search(/\/p2u\//g)>=0) tag = "p2u_" + tag;
        tags.push({
            "name": tag,
            "description": r.description
        });

        // for (let i=0;i<matrix.length;i++){
            // const route = matrix[i];
            const route = r
            
            const pathDash = route.path.split("/").join("-");
            const p = pathDash+"-payload";
            // const payloadName = camelCase(tag+"-"+p,{pascalCase: false});
            const payloadName = tag+"-"+p
            const isProtected = (route.permission.useJwt)? "Yes" : "No";
            let protection_type = "";
            /*if (route.access==Grants.ProtectedByACL) protection_type = "ProtectedByACL";
            else*/ if (route.permission.useJwt) protection_type = "ProtectedByToken";
            else protection_type = ArrayToString(route.permission.roles);
            const allowedRoles = (route.permission.roles.find(doc=>doc==OPEN_TO_ALL) == OPEN_TO_ALL||route.permission.roles.length == 0) ? "All" : protection_type;
            const description = route.description +"<p><b>protected:</b> " + isProtected + "</p> <p><b>allowed roles:</b> " + allowedRoles +"</p>";
            
            // const apiSuccessDef = camelCase(tag+"-"+pathDash+"-"+"api-success");
            const apiSuccessDef = tag+"-"+pathDash+"-"+"api-success"

            paths[route.path] = {
                [route.method.toLocaleLowerCase()]: {
                    tags: [tag],
                    summary: route.description,
                    description: description,
                    operationId: getLastAfterSplit(route.path),
                    consumes: ["application/json"],
                    produces: ["application/json"],
                    parameters: [
                        {
                            "in": "body",
                            "name": p.split("-").join(" "),
                            "description": p.split("-").join(" "),
                            ...(route.validator) && {"schema": {
                                "$ref": `#/definitions/${payloadName}`
                            }}
                        }
                    ],
                    responses:{
                        "200": {
                            description: "Success object",
                            schema: {"$ref": "#/definitions/"+apiSuccessDef}
                        },
                        "500": {
                            description: "Internal API error or service unavailable",
                            schema: {"$ref": '#/definitions/500Error'}
                        }
                    }
                }
            };
            
            definitions[apiSuccessDef] = {
                require: ["success","data","error","status"],
                properties:{
                    success: {"type": "boolean", "example": "true"}, 
                    error: {"type": "object", "example": "null"},
                    status: {"type": "string","example": "OK"},
                    data: { "type": "object", "example": route.sampleResponse}
                }
            }

            if (route.validator){
                const swagger = j2s(route.validator).swagger;
                definitions[payloadName]=swagger;    
            }

        // }   
    })

    definitions["apiSuccess"] = {
        require: ["success","data","error","status"],
        properties:{
            success: {"type": "boolean", "example": "true"}, 
            error: {"type": "object", "example": "null"},
            status: {"type": "string","example": "OK"},
            data: { "type": "object", "example": {"field1":"...","field2":"...","fieldN":"..."}}
        }
    }

    definitions["400Error"] = {
        require: ["success","data","error","status"],
        properties:{
            error: {"$ref": '#/definitions/400ErrorData'},
            status: {"type": "string","example": "error"},
            data: { "type": "object", "example": "null",   "properties": {}}
        }
    }

    definitions["422Error"] = {
        require: ["success","data","error","status"],
        properties:{
            success: {"type": "boolean", "example": "false"},
            error: {"$ref": '#/definitions/422ErrorData'},
            status: {"type": "string","example": "error"},
            data: { "type": "object", "example": "null",   "properties": {}}
        }
    }

    definitions["403Error"] = {
        require: ["success","data","error","status"],
        properties:{
            success: {"type": "boolean", "example": "false"},
            error: {"$ref": '#/definitions/403ErrorData'},
            status: {"type": "string","example": "error"},
            data: { "type": "object", "example": "null",   "properties": {}}
        }
    }

    definitions["500Error"] = {
        require: ["success","data","error","status"],
        properties:{
            success: {"type": "boolean", "example": "false"},
            error: {"$ref": '#/definitions/500ErrorData'},
            status: {"type": "string","example": "error"},
            data: { "type": "object", "example": "null",   "properties": {}}
        }
    }

    definitions["400ErrorData"] = {
        require: ["code","message","type"],
        properties:{
            code: {"type":"integer","example": '1001'},
            message: {"type": "string","example": "Joi validation error message here"},
            type: { "type": "string", "example": "REQUEST_PAYLOAD_VALIDATION"}
        }
    }

    definitions["403ErrorData"] = {
        require: ["code","message","type"],
        properties:{
            code: {"type":"integer","example": '2001'},
            message: {"type": "string","example": "Invalid email or password (or JWT fail)"},
            type: { "type": "string", "example": "FORBIDDEN"}
        }
    }

    definitions["422ErrorData"] = {
        require: ["code","message","type"],
        properties:{
            code: {"type":"integer","example": '3001'},
            message: {"type": "string","example": "The provided email is pending registration with another account."},
            type: { "type": "string", "example": "ACCOUNT_REGISTRY_PENDING"}
        }
    }

    definitions["500ErrorData"] = {
        require: ["code","message","type"],
        properties:{
            code: {"type":"integer","example": '5001'},
            message: {"type": "string","example": "Internal server error or service unavailable"},
            type: { "type": "string", "example": "INTERNAL_SERVER_ERROR"}
        }
    }

    data.tags = tags;
    data.schemes = [config.PROTOCOL];
    data.paths = paths;
    data.definitions = definitions;

    //let public_ip = "detecting..."
    //publicIp.v4().then(ip => {public_ip = ip});
    const host_ip = config.DOMAIN || "127.0.0.1"
    data["host"] = config.PORT ? `${host_ip}:${config.PORT}` : host_ip;

    // let jsonString = JSON.stringify(data,undefined,2);
    // const filePath = Path.resolve(__dirname,'./doc/swaggerDoc.json');
    //ColorLog.Blue("filePath : ",filePath)
    // fs.writeFileSync(filePath, jsonString);
    await settingsRepo.upsertSwagger(data)
}

export default generateSwaggerDoc;
