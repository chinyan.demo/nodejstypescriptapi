import Koa, { Context } from "koa";
import KoaRouter from "koa-router";
import cors from "@koa/cors";
import bodyParser from "koa-bodyparser";
import "dotenv/config";
import { RouterV1 } from "./api";
import { setupCollection } from "./database/arango/setup";
import { Server } from "http";
import { Server as IoServer } from "socket.io";
import { swaggerMiddleware } from "./helper/swagger";
import { websocketMiddleware } from "./helper/websocket";
import config from "./constant/config";
import { createAdapter } from "@socket.io/cluster-adapter";
import { setupWorker } from "@socket.io/sticky";
const {PORT,DOMAIN} = config;


const getServer = async():Promise<Server>=> {
    const app: Koa = new Koa({proxy:true});
    app.use(cors());
    app.use(bodyParser());
    const router: KoaRouter = new KoaRouter();
    router.get("", async (ctx: Context) => {
        // const params = ctx.params
        ctx.body = {
            code: 200,
            msg: "up and running ...",
            //finger print variable
            IP: ctx.request.headers['x-forwarded-for'],
            UserAgent: ctx.request.headers['user-agent'],
            secChUa: ctx.request.headers['sec-ch-ua'],
            AcceptLanguage: ctx.request.headers['accept-language'],
            upgradeInsecureRequests: ctx.request.headers['upgrade-insecure-requests'],
            // params
        }
    });
    app.use(router.routes());
    await setupCollection();
    await RouterV1(app);
    await swaggerMiddleware(app);
    return new Server(app.callback());
}


export const startServer = async() => {
    const httpServer =  await getServer(); 
    const io = new IoServer(httpServer,{
        cors: {
            origin: "*",
            methods: ["GET", "POST"],
            // transports:["websocket"]
        },
        path: '/websocket'
    });

    websocketMiddleware(io);
    httpServer.listen(parseInt(String(PORT)), DOMAIN, () => {
        console.log(`app started at ${DOMAIN}:${PORT}`);
    });
} 

export const startServerWithCluster = async() => {
    const httpServer =  await getServer(); 
    const io = new IoServer(httpServer,{
        cors: {
            origin: "*",
            methods: ["GET", "POST"],
            // transports:["websocket"]
        },
        path: '/websocket'
    });
    io.adapter(createAdapter());
    setupWorker(io);

    websocketMiddleware(io);
    httpServer.listen(parseInt(String(PORT)), DOMAIN, () => {
        console.log(`app started at ${DOMAIN}:${PORT}`);
    });
}

export default getServer
