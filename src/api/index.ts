import KoaRouter from "koa-router"
import {getRouteMatricesV1} from "./routeMatrices"
import { HTTP } from "../constant/http.constant"
import Koa,{ Context, Middleware, Next } from "koa"
import { ObjectSchema } from "joi"
import { validateJWT } from "../helper/cryptography"
import ArangoRepository from "../database/arango/repository"
import { getTransaction } from "../database/arango/transaction"
import { Transaction } from "arangojs/transaction"
import { AccessControl } from "../models/base"
import { JwtData } from "../models/jwt.model"


const JwtValidator = (ctx:Context,next:Next):{jwtData:any,err:any} => {
    try{
        const bearerToken = ctx.request.headers["authorization"]
        if(!bearerToken) throw "Authorization token is required"
        const BearerRegexp = new RegExp(/Bearer /)
        if(!BearerRegexp.test(bearerToken)) throw "invalid token header"

        const token = bearerToken.replace(/Bearer /,"").replace(/ /,"")
        if(!token||token.length==0) throw "token is required"

        const {res:JwtData,err:JwtErr} = validateJWT(token)
        if(JwtErr) throw "token mal-form"
        const {iat,clientIP} = JwtData
        if((iat - new Date().getTime())<=0) throw "token expired"
        const xForwardedFor = ctx.request.headers['x-forwarded-for'] 
        const clientIPAdrs = (typeof xForwardedFor == "undefined")? ctx.socket.remoteAddress : (Array.isArray(xForwardedFor)) ? xForwardedFor.shift(): xForwardedFor
        if(clientIP != clientIPAdrs) throw "invalid clientIP" // create security alert

        // console.log(JwtData)
        return {jwtData:JwtData,err:undefined}
    }catch(error){
        return {jwtData:undefined,err:error}
    }
}

const RolePermissionValidator = (jwtData:JwtData,permission:Array<string>):{err:any} => {
    try{ 
        if(permission.length==0) return {err:undefined}
        if(!permission.find(doc=>doc==jwtData.role)) throw "invalid role"    
        return {err:undefined}
    }catch(err){
        return {err}
    }
}

const DataAccessControl = (jwtData:JwtData,payload:{[key:string]:any},access:Array<AccessControl>,ctx:Context):{err?:any} => {
    try{
        if(access.length==0) return {err:undefined}

        for(const acc of access){
            if(!("payloadPath" in acc)&&!("fetched" in acc)) throw "invalid access(Backed not checking)";
            const {tokenPath} = acc;
            const tokenPremises = tokenPath.split(".")
            let jwtDataToBeChecked = JSON.parse(JSON.stringify(jwtData))
            tokenPremises.forEach(premise=>{ jwtDataToBeChecked = jwtDataToBeChecked[premise]; })

            if(("payloadPath" in acc)&&(!acc.payloadPath||acc.payloadPath.length<=0)) {
                const {payloadPath} = acc;

                if(payloadPath){
                    const payloadPremises = payloadPath.split(".")
                    let payloadToBeChecked = JSON.parse(JSON.stringify(payload))
                    payloadPremises.forEach(premise=>{ payloadToBeChecked = payloadToBeChecked[premise]; })
                    if(!jwtDataToBeChecked || !payloadToBeChecked) throw "invalid access(Backed config error)";
                    // if(jwtDataToBeChecked != jwtDataToBeChecked) throw "invalid access";
                    if(Array.isArray(jwtDataToBeChecked)){
                        const found =  jwtDataToBeChecked.find(payloadData=>payloadData==payloadToBeChecked)
                        if( !found ) throw "invalid access";
                    }else{
                        console.table({jwtDataToBeChecked,payloadToBeChecked})
                        if(jwtDataToBeChecked!=payloadToBeChecked) throw "invalid access";
                    }
                }
            } else if(("fetched" in acc) && acc.fetched){
                const {fetched} = acc;
                if(fetched){
                    const { path, fetchedDataCollection } = fetched
                    const fetchDataContext = JSON.parse(JSON.stringify(ctx.fetched)) as Array<any>
                    const fetchDoc = fetchDataContext.filter(data=>new RegExp(fetchedDataCollection,"i").test(data["_id"]))[0]

                    const fetchedPremises = path.split(".")
                    let fetchedDataToBeChecked = JSON.parse(JSON.stringify(fetchDoc))
                    fetchedPremises.forEach(premise=>{ fetchedDataToBeChecked = fetchedDataToBeChecked[premise]; })

                    if(Array.isArray(fetchedDataToBeChecked)){
                        const found =  fetchedDataToBeChecked.find(fetchedData=>fetchedData==jwtDataToBeChecked)
                        if( !found ) throw "invalid access";
                    }else{
                        if(fetchedDataToBeChecked!=jwtDataToBeChecked) throw "invalid access";
                    }
                    if(!jwtDataToBeChecked || !fetchedDataToBeChecked) throw "invalid access(Backed config error)";
                }
                
            }
        }
        return {err:undefined}
    }catch(error){
        return {err:error}
    }
}

const IdValidator = async(payload:{[key:string]:any}):Promise<{fetched:Array<any>,err:any}> => {
    try{
        const fetched = []
        const fetchedError = []
        const IdRegExp = new RegExp(/.*(id|ids)$/,"i");
        const keys = Object.keys(payload)
        for(const key of keys){
            if(IdRegExp.test(key)){
                try{
                    if(typeof payload[key] == "string"){
                        let collection = payload[key].split("/")[0]
                        const repo = new ArangoRepository(collection)
                        const {res:fetchedObj,err:fetchErr} = await repo.fetch(payload[key])
                        if(fetchErr) throw fetchErr;
                        fetched.push(fetchedObj)
                    }else if(typeof payload[key] == "object" && payload[key].length>0){
                        payload[key].forEach(async (id:string) =>{
                            let collection = id.split("/")[0]
                            const repo = new ArangoRepository(collection)
                            const {res:fetchedObj,err:fetchErr} = await repo.fetch(id)
                            if(fetchErr) throw fetchErr;
                            fetched.push(fetchedObj)
                        })
                    }
                }catch(error){
                    fetchedError.push(`invalid ${key} ${payload[key]}`);
                }
            }
        }
        if(fetchedError.length>0) throw fetchedError;
        return {fetched,err:undefined}
    }catch(err){
        return {fetched:[],err}
    }
}

const createMiddleware = (permission:{roles:Array<string>,useJwt:boolean},validator:ObjectSchema,access:Array<AccessControl>,transactions?:Array<string>):Middleware => {
    return async(ctx:Context,next:Next) =>{
        try{
            //JWT validation
            if(permission.useJwt){
                const {jwtData,err:JwtValidationErr} = JwtValidator(ctx,next)
                if(JwtValidationErr) throw JwtValidationErr
                ctx.jwtData = jwtData;
                
                //Role Permission Validation
                const {err:rolePermissionErr} = RolePermissionValidator(jwtData,permission.roles)
                if(rolePermissionErr) throw rolePermissionErr
            }

            //Payload Validation
            const body:object = ctx.request.body as object
            const query:object = ctx.query
            const param:object = ctx.params
            const payload = (Object.keys(body).length != 0)? body : (Object.keys(query).length != 0)? ctx.query : param

            const {value:validatedPayload,error:validatedError} = validator.validate(payload)
            if(validatedError) {
                ctx.status = 400
                return ctx.body = { success:false, errorObject: validatedError}
            }
            // fetch If ID exist
            const {fetched, err:fetchedErr} = await IdValidator(payload)
            if(fetchedErr) throw fetchedErr
            if(fetched) ctx.fetched = fetched;

            if(transactions && transactions.length>0){
              const txn = await getTransaction(transactions) 
              ctx.txn = txn;
                
            }

            //Data Access Control 
            if(permission.useJwt){
                const {err:accessControlErr} = DataAccessControl(ctx.jwtData as JwtData,payload,access,ctx)
                if(accessControlErr) throw accessControlErr
            }

            // ctx.set("validatedPayload",validatedPayload)
            // ctx.response.set("validatedPayload",validatedPayload)
            ctx.validatedPayload = validatedPayload;
            await next()
        }catch(error){
            ctx.status = 401;
            return ctx.body = { success:false, errorObject: error}
        }
    }
}
const responseHandler = (handler:Middleware) => {
    return async (ctx:Context,next:Next) =>{
        const txn:Transaction = ctx.txn
        const {res,err} = await handler(ctx,next);
        if(err){ 
            if(txn) await txn.abort()
            ctx.status = 500;
            ctx.body = { success:false, errorObject: err} 
        }
        if(res) {
            if(txn) await txn.commit()
            ctx.body = { success:true, status: "OK", error: null, data: res}
        }
    }
}

export const RouterV1 = async(app:Koa) => {
    const routerV1 = new KoaRouter()
    const routeMatricesV1 = getRouteMatricesV1()

    routeMatricesV1.forEach(async routeMatrix =>{
        const {  method,path,permission,validator,controller,transactions,access } = routeMatrix
        let middleware = createMiddleware(permission,validator,access,transactions)
        const endpoint = `/v1.0${path}`;
        // console.log({[method]:endpoint})
        switch(method){
            case HTTP.GET:
                routerV1.get(endpoint,middleware,responseHandler(controller))
                break;
            case HTTP.POST:
                routerV1.post(endpoint,middleware,responseHandler(controller))
                break;
            case HTTP.PUT:
                routerV1.put(endpoint,middleware,responseHandler(controller))
                break;
            case HTTP.DELETE:
                routerV1.delete(endpoint,middleware,responseHandler(controller))
                break;
            default:
                routerV1.all(endpoint,middleware,responseHandler(controller))
        }
    });

    app.use(routerV1.routes())
}

