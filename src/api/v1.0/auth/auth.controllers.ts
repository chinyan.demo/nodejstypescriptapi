import { Context, Next } from "koa"
import { ILoginReq, IRegisterReq } from "./auth.validators"
import {createHash, createJWT} from "../../../helper/cryptography"
import { docHelper } from "../../../helper/document"
import { COLLECTION } from "../../../database/arango/collection.enum"
import profileRepo from "../../../repository/profile"
import accountRepo from "../../../repository/account"
import { validateHash } from "../../../helper/cryptography"
import { Transaction } from "arangojs/transaction"
import { JwtData } from "../../../models/jwt.model"


export const registerHandler = async(ctx:Context,next:Next):Promise<{res:any,err:any}> => {
    try{
        const payload:IRegisterReq = ctx.validatedPayload
        const txn:Transaction = ctx.txn 
        const {name,email,password,role} = payload;

        const {hash,salt} = createHash(password);

        const profile = docHelper({name,email,role},COLLECTION.PROFILES)
        const profileID = profile._id
        const account = docHelper({email,password: { hash, salt }, profileID},COLLECTION.ACCOUNTS,profileID);

        // const {res:createProfileRes,err:transactionErr} = await  useTransaction([COLLECTION.PROFILES,COLLECTION.ACCOUNTS], async txn=>{
            const {res:fetchedProfiles, err:fetchProfilesErr} = await profileRepo.fetchByEmail(email,txn)
            if(fetchProfilesErr) throw {err: fetchProfilesErr}
            if(fetchedProfiles&&fetchedProfiles.length>0) throw {err:"Email Taken"}
            const {res:createProfileRes,err:createProfileErr} = await profileRepo.create(profile,txn)
            if(createProfileErr) throw {err: createProfileErr}
            const {err:createAccountErr} = await accountRepo.create(account,txn)
            if(createAccountErr) throw {err: createAccountErr}
            
        // });
        // if(transactionErr) throw transactionErr;

       
        return {res:createProfileRes, err:null}
    }catch(error){
        console.log({error})
        return {err:error,res:null}
    }
}

export const loginHandler = async(ctx:Context,next:Next):Promise<{res?:any,err?:any}> => {
    try{
        const payload:ILoginReq = ctx.validatedPayload
        const {email,password} = payload;

        const {res:fetchedAccounts, err:fetchAccountsErr} = await accountRepo.fetchAccByEmail(email)
        if(fetchAccountsErr) throw fetchAccountsErr;
        if(fetchedAccounts&&fetchedAccounts.length<=0) throw "invalid email";
        const account = fetchedAccounts[0]
        const hash = String(account?.password.hash)
        const salt = String(account?.password.salt)
        const profileID = String(account?.profileID)
        const accountID = String(account?._id)

        const valid = validateHash(hash,salt,password)
        if(!valid) throw "invalid password"

        const {res:profile,err:fetchProfileErr} = await profileRepo.fetch(profileID);
        if(fetchProfileErr) throw fetchProfileErr
        if(!profile) throw "fetch profile failed invalid profile"

        const xForwardedFor = ctx.request.headers['x-forwarded-for'] 
        const clientIP = (typeof xForwardedFor == "undefined")? ctx.socket.remoteAddress : (Array.isArray(xForwardedFor)) ? xForwardedFor.shift(): xForwardedFor

        //create JWT
        const token = createJWT({accountID,...profile,clientIP})
        return {res:{profile,token}}
    }catch(error){
        return {err:error}
    }
}

export const refreshTokenHandler = async(ctx:Context,next:Next):Promise<{res?:any,err?:any}> => {
    try{
        const jwtData:JwtData = ctx.jwtData;
        // console.table(jwtData)
        const xForwardedFor = ctx.request.headers['x-forwarded-for'] 
        const clientIP = (typeof xForwardedFor == "undefined")? ctx.socket.remoteAddress : (Array.isArray(xForwardedFor)) ? xForwardedFor.shift(): xForwardedFor
        
        const token = createJWT({...jwtData,...{clientIP}})
    
        return {res:{token},err:undefined}
    }catch(error){
        return {res:undefined,err:error}
    }
} 
