import {HTTP} from "../../../constant/http.constant"
import  {registerHandler,loginHandler, refreshTokenHandler} from "./auth.controllers"
import { RegisterReq, LogInReq , RefreshTokenReq} from "./auth.validators"
import { IRouteMatrix } from "../../../models/base"
import { PERMISSION } from "../../../constant/permission"
import { COLLECTION } from "../../../database/arango/collection.enum"
import loginSample from "../../../helper/swagger/data/auth/loginRes.json"
import registerSample from "../../../helper/swagger/data/auth/registerRes.json"
import refreshTokenSample from "../../../helper/swagger/data/auth/refreshTokenRes.json"

const AuthRouteMatrices: Array<IRouteMatrix> = [
    {
        method: HTTP.POST,
        path:"/auth/register",
        permission:{roles:[],useJwt:false},
        access:[],
        transactions:[COLLECTION.PROFILES,COLLECTION.ACCOUNTS],
        validator: RegisterReq,
        controller: registerHandler,
        description: "register new user",
        sampleResponse: registerSample

    },{
        method: HTTP.POST,
        path:"/auth/login",
        permission:{roles:[],useJwt:false},
        access:[],
        validator: LogInReq,
        controller: loginHandler,
        description: "user login",
        sampleResponse: loginSample
    },{
        method: HTTP.POST,
        path:"/auth/refresh/token",
        permission:{roles:PERMISSION.MEMBERS_ONLY,useJwt:true},
        access:[{tokenPath: "_id", payloadPath:"profileID"}],
        validator: RefreshTokenReq,
        controller: refreshTokenHandler,
        description: "refresh token",
        sampleResponse: refreshTokenSample
    }
]

export default AuthRouteMatrices