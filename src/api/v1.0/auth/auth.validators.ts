import joi from "joi"
import { profileField,profileID } from "../../../models/profile.model";

/***********************
 * Interface
 ***********************/
export interface IRegisterReq {
    name:string
    email:string
    password:string
    confirmPassword:string
    role:string
}

export interface ILoginReq {
    email: string
    password: string
}


/***********************
 * validator
 ***********************/
export const openToAll = joi.allow();

export const RegisterReq = joi.object().keys({
    ...profileField,
    password: joi.string().required(),
    confirmPassword: joi.string().required().valid(joi.ref("password")).messages({'any.only':'confirmPassword must match with password'})
});

export const LogInReq = joi.object().keys({
    email:profileField.email,
    password: joi.string().required(),
});

export const RefreshTokenReq = joi.object().keys({
    profileID: profileID.required()
})

