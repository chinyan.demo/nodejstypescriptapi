import {HTTP} from "../../../constant/http.constant"
import  {createGroupHandler,listConversationHandler} from "./conversation.controllers"
import { createGroupReq,listConversationReq } from "./conversation.validators"
import { IRouteMatrix } from "../../../models/base"
import { PERMISSION } from "../../../constant/permission"
import createRes from "../../../helper/swagger/data/conversations/createRes.json"
import listRes from "../../../helper/swagger/data/conversations/listRes.json"

const ConversationRouteMatrices: Array<IRouteMatrix> = [
    {
        method: HTTP.POST,
        path:"/conversation/group/create",
        permission:{roles:PERMISSION.MEMBERS_ONLY,useJwt:true},
        access:[{tokenPath:"_id",payloadPath:"profileIDs"}],
        validator: createGroupReq,
        controller: createGroupHandler,
        description: "create new chat group",
        sampleResponse: createRes

    },{
        method:HTTP.GET,
        path:"/conversation/list",
        permission:{roles:PERMISSION.MEMBERS_ONLY,useJwt:true},
        access:[{tokenPath:"_id",payloadPath:"profileID"}],
        validator: listConversationReq,
        controller: listConversationHandler,
        description: "list conversation by profileID",
        sampleResponse:listRes

    }
]

export default ConversationRouteMatrices