import { Next, Context } from "koa";
import { IConversation } from "../../../models/conversation.model";
import conversationRepo from "../../../repository/conversation";
import { JwtData } from "../../../models/jwt.model";
import { docHelper } from "../../../helper/document";
import { COLLECTION } from "../../../database/arango/collection.enum";


export const createGroupHandler = async(ctx:Context,next:Next) => {
    try{
        const validatedPayload:IConversation = ctx.validatedPayload;
        const jwtData:JwtData = ctx.jwtData;
        const {_id:senderProfileID} = jwtData
        validatedPayload.unreadMessagesCount = {};
        const unreadUsers = validatedPayload.profileIDs.filter((profileID:string)=>profileID!=senderProfileID)
        unreadUsers.forEach(profileID=>{
            if(!validatedPayload.unreadMessagesCount[profileID]) validatedPayload.unreadMessagesCount[profileID] = 0;
            validatedPayload.unreadMessagesCount[profileID]++;
        })
        const {err,res} = await conversationRepo.create(docHelper(validatedPayload,COLLECTION.CONVERSATIONS,senderProfileID))
        if(err) throw err;

        return {res,err:undefined}
    }catch(error){
        return {err:error, res:undefined}
    }
}

export const listConversationHandler = async(ctx:Context,next:Next) => {
    try{
        const {profileID}= ctx.validatedPayload as {profileID:string};
   
        const {res,err} = await conversationRepo.list(profileID)
        if(err) throw err

        return {res,err:undefined}
    }catch(error){
        return {err:error, res:undefined}
    }
}