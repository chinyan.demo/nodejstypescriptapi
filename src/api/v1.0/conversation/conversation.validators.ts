import joi from "joi"
import { conversationFields } from "../../../models/conversation.model"
import { profileID } from "../../../models/profile.model";

export const createGroupReq = joi.object().keys({
    ...conversationFields,
    ...{type:joi.string().valid("group_message").required()}
});

export const listConversationReq = joi.object().keys({
    profileID: profileID.required()
})
