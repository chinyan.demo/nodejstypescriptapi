import {HTTP} from "../../../constant/http.constant"
import  {fetchHandler} from "./profile.controllers"
import { fetchReq } from "./profile.validators"
import { IRouteMatrix } from "../../../models/base"
import { PERMISSION } from "../../../constant/permission"
import fetchRes from "../../../helper/swagger/data/profile/fetchRes.json"
const ProfileRouteMatrices: Array<IRouteMatrix> = [
    {
        method: HTTP.GET,
        path:"/profile/fetch",
        permission:{roles:PERMISSION.MEMBERS_ONLY,useJwt:true},
        access:[{tokenPath:"_id",payloadPath:"profileID"}],
        validator: fetchReq,
        controller: fetchHandler,
        description: "fetch user profile",
        sampleResponse: fetchRes

    }
]

export default ProfileRouteMatrices