import joi from "joi"
import { profileID } from "../../../models/profile.model";

export const fetchReq = joi.object().keys({
    profileID: profileID.required()
})

export interface IfetchReq {
    profileID: string
}