import { Context, Next } from "koa"
import { IfetchReq } from "./profile.validators";
import profileRepo from "../../../repository/profile";

export const fetchHandler = async(ctx:Context,next:Next):Promise<{res:any,err:any}> => {
    try{
        const payload:IfetchReq = ctx.validatedPayload;
        const { profileID } = payload;
        const {res:profile,err:fetchErr} = await profileRepo.fetch(profileID);
        if(fetchErr) throw fetchErr

        return {res:profile ,err:undefined}
    }catch(error){
        return {res : undefined, err: error}
    }
}