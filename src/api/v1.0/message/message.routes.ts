import {HTTP} from "../../../constant/http.constant"
import  {createTestMessageHandler, createMessageHandler, listOnlineUserHandler, updateMessageReadBy,listMessageByConversationID} from "./message.controllers"
import { createMessageReq,createTestMessageReq,getOnlineUserReq, updateReadByReq,listMessageByConversationIDReq } from "./message.validators"
import { IRouteMatrix } from "../../../models/base"
import { PERMISSION } from "../../../constant/permission"
import { COLLECTION } from "../../../database/arango/collection.enum"
import createRes from "../../../helper/swagger/data/messages/createRes.json"
import listRes from "../../../helper/swagger/data/messages/listRes.json"
import updateRes from "../../../helper/swagger/data/messages/listRes.json"

const MessageRouteMatrices: Array<IRouteMatrix> = [
    {
        method: HTTP.POST,
        path:"/message/test/create",
        permission:{roles:PERMISSION.MEMBERS_ONLY,useJwt:true},
        access:[{tokenPath:"_id",payloadPath:"senderProfileID"}],
        validator: createTestMessageReq,
        controller: createTestMessageHandler,
        description: "test web socket message db no store",

    }, {
        method: HTTP.POST,
        path:"/message/create",
        permission:{roles:PERMISSION.MEMBERS_ONLY,useJwt:true},
        transactions:[COLLECTION.CONVERSATIONS,COLLECTION.MESSAGES],
        access:[{tokenPath:"_id",payloadPath:"senderProfileID"}],
        validator: createMessageReq,
        controller: createMessageHandler,
        description: "create messages",
        sampleResponse: createRes

    }, {
        method: HTTP.PUT,
        path:"/message/readby/update",
        permission:{roles:PERMISSION.MEMBERS_ONLY,useJwt:true},
        transactions:[COLLECTION.CONVERSATIONS,COLLECTION.MESSAGES],
        access:[{tokenPath:"_id",payloadPath:"profileID"}],
        validator: updateReadByReq,
        controller: updateMessageReadBy,
        description: "update messages",
        sampleResponse: updateRes

    }, {
        method: HTTP.GET,
        path:"/message/online/user/list",
        permission:{roles:PERMISSION.MEMBERS_ONLY,useJwt:true},
        access:[{tokenPath:"_id",payloadPath:"profileID"}],
        validator: getOnlineUserReq,
        controller: listOnlineUserHandler,
        description: "list online user (get from socket)",
        sampleResponse: [
            {profileID:"profiles/ce6e6581-69a4-4245-b6ca-c46ce271ffd2",name:"Nurul Ain"},
            {profileID:"profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",name:"Hong Wei"}
        ]

    },{
        method: HTTP.POST,
        path:"/message/by/conversationID/list",
        permission:{roles:PERMISSION.MEMBERS_ONLY,useJwt:true},
        access:[{tokenPath:"_id", fetched:{fetchedDataCollection:COLLECTION.CONVERSATIONS, path:"profileIDs"}}],
        validator: listMessageByConversationIDReq,
        controller: listMessageByConversationID,
        description: "list message base on conversationID with pagination",
        sampleResponse: listRes

    }
]

export default MessageRouteMatrices