import Joi from "joi"
import { profileID } from "../../../models/profile.model";
import { messageFields } from "../../../models/message.model";
import { conversationID } from "../../../models/conversation.model";

export const createTestMessageReq = Joi.object().keys({
    senderProfileID:profileID.required(),
    senderName:Joi.string().required(),
    text:Joi.string().required(),
    receiverProfileID:profileID.required(),
    receiverName:Joi.string().required()
});

export const createMessageReq = Joi.object().keys(messageFields)


export const getOnlineUserReq = Joi.object().keys({
    profileID: profileID.required()
})

export const listMessageByConversationIDReq = Joi.object().keys({
    conversationID: conversationID.required(),
    pagination: Joi.object().keys({
        offset: Joi.number().default(0),
        limit: Joi.number().default(100)
    }).required()
})

export const updateReadByReq = Joi.object().keys({
    conversationID:conversationID.required(),
    profileID:profileID.required()
})
export interface ICreateMessageReq{
    senderProfileID:string,
    senderName:string,
    text:string,
    receiverProfileID:string,
    receiverName:string,
}

export interface IgetOnlineUserReq {
    profileID:string
}

export interface IUpdateReadByReq{
    conversationID:string,
    profileID:string
}

