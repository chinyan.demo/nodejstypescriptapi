import { Context, Next } from "koa";
import { serverWebsocketSend } from "../../../helper/websocket/client";
import {ICreateMessageReq, IUpdateReadByReq, IgetOnlineUserReq} from "./message.validators"
import { TOPIC } from "../../../helper/websocket/topics";
import onlineUsersTracker from "../../../helper/websocket/handlers/onlineUser";
import { IMessage } from "../../../models/message.model";
import { IConversation } from "../../../models/conversation.model";
import messageRepo from "../../../repository/message";
import conversationRepo from "../../../repository/conversation";
import { docHelper } from "../../../helper/document";
import { COLLECTION } from "../../../database/arango/collection.enum";
import { Transaction } from "arangojs/transaction";
import { IProfile } from "../../../models/profile.model";

export const createTestMessageHandler = async (ctx:Context, next:Next) => {
    try{
        const payload:ICreateMessageReq = ctx.validatedPayload;
        const {receiverProfileID} = payload;

        const {success,error} = serverWebsocketSend(
            TOPIC.PRIVATE_MESSAGE,
            receiverProfileID,
            payload
        )
        if(!success) throw error
        return {res : "message created", err: undefined}
    }catch(error){
        return {res : undefined, err: error}
    }
}

export const createMessageHandler = async (ctx:Context, next:Next) => {
    try{
        const txn:Transaction = ctx.txn
        const message:IMessage&{unreadMessagesCount?:{[profileID:string]:number}} = ctx.validatedPayload;
        const {conversationID,senderProfileID,content} = message;

        const fetched = ctx.fetched;
        const conversation:IConversation = fetched.find((doc:object&{_id:string})=>doc._id == conversationID);
        const senderProfile:IProfile = fetched.find((doc:object&{_id:string})=>doc._id == senderProfileID)
        const receiverProfileIDs = conversation?.profileIDs.filter(pid=>pid!=senderProfileID);

        message.senderName = senderProfile.name;
        const {res:createMessageRes,err:createMessageErr} = await messageRepo.create(docHelper(message,COLLECTION.MESSAGES,senderProfileID),txn)
        if(createMessageErr) throw createMessageErr;
        message.messageID = createMessageRes?._id;
        
        if(!conversation.unreadMessagesCount) conversation.unreadMessagesCount = {}
        const unreadUsers = conversation.profileIDs.filter((profileID:string)=>profileID!=senderProfile._id)
        unreadUsers.forEach(profileID=>{
            if(!conversation.unreadMessagesCount[profileID]) conversation.unreadMessagesCount[profileID] = 0;
            conversation.unreadMessagesCount[profileID]++;
        })
        message.unreadMessagesCount = conversation.unreadMessagesCount;
        const {res:updateConversationRes,err:updateConversationErr} = await conversationRepo.update(conversationID,{lastMessage:{sender:senderProfile.name, text:content, datetime: new Date().toISOString()}, unreadMessagesCount: conversation.unreadMessagesCount},txn);
        if(updateConversationErr) throw updateConversationErr;

        const errorStack = []
        for(const receiverProfileID of receiverProfileIDs){
            const {success,error} = serverWebsocketSend(
                TOPIC.FORWARD_PRIVATE_MESSAGE,
                receiverProfileID,
                message
            )
            if(!success) errorStack.push(error)
        }
        return {res :{message:createMessageRes,conversation:updateConversationRes}, err: undefined, info:errorStack}
    }catch(error){
        return {res : undefined, err: error}
    }
}

export const listMessageByConversationID = async(ctx:Context, next:Next) => {
    try{
        const {conversationID,pagination} = ctx.validatedPayload as {conversationID:string,pagination:{offset:number,limit:number}}

        const {res,err} = await messageRepo.listMessageByConversationID(conversationID,pagination)
        if(err) throw err

        return {res, err:undefined}
    }catch(error){
        return {res : undefined, err: error}
    }
}

export const updateMessageReadBy = async(ctx:Context, next:Next) => {
    try{
        const txn:Transaction = ctx.txn
        const payload:IUpdateReadByReq = ctx.validatedPayload
        const {conversationID, profileID} = payload;
        
        const fetched = ctx.fetched;
        const conversation:IConversation = fetched.find((doc:any&{_id:string})=>doc._id == conversationID)
        if(conversation.unreadMessagesCount&&conversation.unreadMessagesCount[profileID]){ 
            conversation.unreadMessagesCount[profileID] = 0
            conversation.updatedBy = profileID
            conversation.updatedAt = new Date().toISOString()
            const { err:updateConversationErr} = await conversationRepo.update(conversationID,conversation,txn);
            if(updateConversationErr) throw updateConversationErr;
        }
        const {res,err} = await messageRepo.updateReadBy(conversationID, profileID,txn)
        if(err) throw err;

        return {res,err:undefined}
    }catch(error){
        return {err:error, res:undefined}
    }
}

export const listOnlineUserHandler = async (ctx:Context, next:Next) =>{
    try{
        const payload:IgetOnlineUserReq = ctx.validatedPayload;
        const {profileID} = payload;
        
        const {success,error} = serverWebsocketSend(
            TOPIC.ONLINE,
            profileID,
            onlineUsersTracker.getAllOnlineUser()
        )
        if(!success) throw error

        return {res:onlineUsersTracker.getAllOnlineUser(), err:undefined}
    }catch(error){
        return {res : undefined, err: error}
    }
}