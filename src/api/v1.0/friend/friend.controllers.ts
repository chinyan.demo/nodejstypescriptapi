import { Context, Next } from "koa";
import { useTransaction } from "../../../database/arango/transaction";
import {
    COLLECTION,
    EDGE_COLLECTION,
} from "../../../database/arango/collection.enum";
import friendRepo from "../../../repository/friend";
import { docHelper } from "../../../helper/document";
import conversationRepo from "../../../repository/conversation";

export const createFriend = async (
    ctx: Context,
    next: Next
): Promise<{ res: any; err: any }> => {
    try {
        const payload = ctx.validatedPayload;
        const { currentID, friendID } = payload;

        const { res, err } = await friendRepo.fetch(payload);

        if (res.length != 0)  throw "This person has already been added as a friend."
          

        const { res: createFriendRes, err: transactionErr } =
            await useTransaction(
                [EDGE_COLLECTION.FRIENDS, COLLECTION.CONVERSATIONS],
                async (txn) => {
                    const {res: createFriendTo,err: createFriendToError,} = await friendRepo.create(
                        docHelper(
                            { _from: currentID, _to: friendID },
                            EDGE_COLLECTION.FRIENDS,
                            currentID
                        ),txn);
                    if (createFriendToError) throw createFriendToError;
                    const { res: createFriendFrom, err: createFriendFromError,} = await friendRepo.create(
                        docHelper(
                            { _from: friendID, _to: currentID },
                            EDGE_COLLECTION.FRIENDS,
                            currentID
                        ),txn);
                    if (createFriendFromError) throw createFriendFromError;
                    const { res: createdConversation, err: createConversationError } = await conversationRepo.create(
                        docHelper(
                            {type: "private_message", profileIDs: [currentID, friendID], unreadMessagesCount:{[currentID]:0,[friendID]:0}},
                            COLLECTION.CONVERSATIONS,
                            currentID
                        ),txn);
                    if (createConversationError) throw createConversationError
                    return {
                        res: { createFriendTo, createFriendFrom, createdConversation },
                    };
                }
            );
        if (transactionErr) throw transactionErr;

        return { res: createFriendRes, err: undefined };
      
    } catch (error) {
        return { res: undefined, err: error };
    }
};

export const listFriend = async (
    ctx: Context,
    next: Next
): Promise<{ res: any; err: any }> => {
    try {
        const payload = ctx.validatedPayload;
        const filters = { profileID: payload?.profileID };

        const { res: listFriendRes, err: fetchErr } =
            await friendRepo.fetchManyWithFilters(filters);

        if (fetchErr) throw fetchErr;

        return { res: listFriendRes, err: undefined };
    } catch (error) {
        return { res: undefined, err: error };
    }
};

export const deleteFriend = async (
    ctx: Context,
    next: Next
): Promise<{ res: any; err: any }> => {
    try {
        const payload = ctx.validatedPayload;

        const { res: deleteFriendRes, err: transactionErr } =
            await useTransaction([EDGE_COLLECTION.FRIENDS], async (txn) => {
                const friendFromDoc = await friendRepo.fetch(payload);

                if (friendFromDoc.res.length === 0)
                    throw { message: "Unable to find document." };

                const { res: deleteFriendTo } = await friendRepo.delete(
                    friendFromDoc.res[0]._id,
                    txn
                );

                const invertPayload = {
                    currentID: payload.friendID,
                    friendID: payload.currentID,
                };

                const friendToDoc = await friendRepo.fetch(invertPayload);

                if (friendToDoc.res.length === 0)
                    throw { message: "Unable to find document (invert)." };

                const { res: deleteFriendFrom } = await friendRepo.delete(
                    friendToDoc.res[0]._id,
                    txn
                );

                return {
                    res: {
                        message: "Successfully removed friend.",
                    },
                };
            });

        if (transactionErr) throw transactionErr;

        return { res: deleteFriendRes, err: undefined };
    } catch (error) {
        return { res: undefined, err: error };
    }
};
