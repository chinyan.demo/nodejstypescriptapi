import { HTTP } from "../../../constant/http.constant";
import { IRouteMatrix } from "../../../models/base";
import { PERMISSION } from "../../../constant/permission";
import {
    createFriendRequestSchema,
    listFriendRequestSchema,
} from "./friend.validators";
import { createFriend, listFriend, deleteFriend } from "./friend.controllers";

const PostRouteMatrices: Array<IRouteMatrix> = [
    {
        method: HTTP.POST,
        path: "/friend/create",
        permission: { roles: PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: createFriendRequestSchema,
        controller: createFriend,
        description: "create a friend",
    },
    {
        method: HTTP.POST,
        path: "/friend/list",
        permission: { roles: PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: listFriendRequestSchema,
        controller: listFriend,
        description: "list a friend",
    },
    {
        method: HTTP.POST,
        path: "/friend/delete",
        permission: { roles: PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: createFriendRequestSchema,
        controller: deleteFriend,
        description: "remove a friend",
    },
];

export default PostRouteMatrices;
