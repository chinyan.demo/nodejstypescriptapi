import joi from "joi";
import {  IPosts } from "../../../models/post.model";
import { profileID } from "../../../models/profile.model";

export const createFriendRequestSchema = joi.object().keys({
    currentID: profileID.required(),
    friendID: profileID.required()
});

export const listFriendRequestSchema = joi.object().keys({
    profileID: profileID.required()
});

export interface IcreateReq {
    post: IPosts
}
