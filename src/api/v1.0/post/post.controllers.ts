import { Context, Next } from "koa";
import { IcreateReq } from "./post.validators";
import postRepo from "../../../repository/post";
import { docHelper } from "../../../helper/document";
import { COLLECTION } from "../../../database/arango/collection.enum";
import { JwtData } from "../../../models/jwt.model";

export const createPost = async (
    ctx: Context,
    next: Next
): Promise<{ res: any; err: any }> => {
    try {
        const payload: IcreateReq = ctx.validatedPayload;
        const post  = payload;
        const jwtData:JwtData = ctx.jwtData;
        const { _id:profileID } = jwtData;
        const { err: createErr } = await postRepo.create(docHelper(post,COLLECTION.POSTS,profileID));
        if (createErr) throw createErr;

        return { res: payload, err: undefined };
    } catch (error) {
        return { res: undefined, err: error };
    }
};

export const listPost = async (
    ctx: Context,
    next: Next
): Promise<{ res: any; err: any }> => {
    try {
        const payload = ctx.validatedPayload;
        const { _id } = payload;

        const { res: result, err: fetchErr } =
            await postRepo.fetchManyWithFilters(_id);
        if (fetchErr) throw fetchErr;

        return { res: result, err: undefined };
    } catch (error) {
        return { res: undefined, err: error };
    }
};
