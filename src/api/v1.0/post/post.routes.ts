import { HTTP } from "../../../constant/http.constant";
import { IRouteMatrix } from "../../../models/base";
import { PERMISSION } from "../../../constant/permission";
import {
    createPostRequestSchema,
    listPostRequestSchema,
} from "./post.validators";
import { createPost, listPost } from "./post.controllers";

const PostRouteMatrices: Array<IRouteMatrix> = [
    {
        method: HTTP.POST,
        path: "/post/create",
        permission: { roles: PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: createPostRequestSchema,
        controller: createPost,
        description: "create a post",
    },
    {
        method: HTTP.POST,
        path: "/post/list",
        permission: { roles: PERMISSION.MEMBERS_ONLY, useJwt: false },
        access: [],
        validator: listPostRequestSchema,
        controller: listPost,
        description: "list all posts",
    },
];

export default PostRouteMatrices;
