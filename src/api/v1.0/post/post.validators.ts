import joi from "joi";
import { postModel,postID,IPosts, postFields } from "../../../models/post.model";

export const createPostRequestSchema = joi.object().keys(postFields);

export const listPostRequestSchema = joi.object().keys({
    _id:postID.optional(),
});

export interface IcreateReq {
    post: IPosts;
}
