import fs from "fs"
import path from "path"
import { IRouteMatrix } from "../models/base";

export const getRouteMatricesV1 = ():Array<IRouteMatrix> => {
    const v1Path = path.resolve(__dirname,"./v1.0");
    const routeMatricesV1:Array<IRouteMatrix> = []

    const v1Dir = fs.readdirSync(v1Path);

    v1Dir.forEach(async dir => {
        const files = fs.readdirSync(path.resolve(__dirname,`./v1.0/${dir}`))
        files.forEach(async file => {
                if(/.routes./.test(file)) {
                    let routerPath =  path.resolve(__dirname,`./v1.0/${dir}/${file}`)
                    const routeMatrices:Array<IRouteMatrix> = require(routerPath)["default"]
                    routeMatricesV1.push(...routeMatrices)
                }
        });
    });
    return routeMatricesV1
}