import fs from "fs";
import path from "path"
import { seedData } from "./src/database/arango/setup";

// let app:Server; 
beforeAll(async () => {
	const files = fs.readdirSync(path.resolve(__dirname,"./test/seed"));
    files.forEach(async (file:string )=>{
        if(/.seed./i.test(file)){
            const data = require(`./test/seed/${file}`).default
            await seedData(data)
        }
    });
})

beforeEach(() => {
    jest.useFakeTimers()
});

// afterAll(() => {
//     setTimeout(()=>{httpServer.close()},20000)
// })

