FROM node:18
# RUN apk update && apk add git

WORKDIR /chatbot2

# USER node

COPY package.json .
COPY . .

RUN npm install yarn

RUN yarn

RUN yarn build

RUN scp ./sample.env ./dist/.env

EXPOSE 3333

CMD [ "node", "dist" ]