import type {Config} from 'jest';

const jestConfig:Config = {
  preset: 'ts-jest',
  testEnvironment: 'jest-environment-jsdom',
  verbose:true,
  transformIgnorePatterns: [
    "/dist/"
	],
  coveragePathIgnorePatterns: [
    "/dist/",
    "/src/helper/swagger/**",
    "/src/database/arango/seed/**"
  ],
  transform: {
    '^.+\\.ts?$': 'ts-jest',
  },
  collectCoverageFrom: [
    '**/*.{ts}',
    '!**/node_modules/**',
    '!**/dist/**',
    "!/src/helper/swagger/**",
    "!/src/database/arango/seed/**"
  ],
  setupFilesAfterEnv: ["./jest.setup.ts"]
};

export default jestConfig




// "jest": {
//   "moduleFileExtensions": [
//     "js",
//     "jsx",
//     "json",
//     "ts",
//     "tsx"
//   ],
//   "collectCoverage": true,
//   "collectCoverageFrom": [
//     "**/*.{ts,js}",
//     "!**/node_modules/**",
//     "!**/build/**",
//     "!**/coverage/**",
//     "!**/dist/**"
//   ],
//   "transform": {
//     "\\.ts$": "<rootDir>/node_modules/ts-jest/preprocessor.js"
//   },
//   "coverageThreshold": {
//     "global": {
//       "branches": 100,
//       "functions": 100,
//       "lines": 100,
//       "statements": 100
//     }
//   },
//   "coverageReporters": [
//     "text",
//     "text-summary"
//   ],
//   "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.(js|ts)x?$",
//   "testPathIgnorePatterns": [
//     "/node_modules/",
//     "/build/",
//     "/coverage/",
//     "/dist/"
//   ]
// },