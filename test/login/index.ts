import superagent from "superagent";
import supertest from "supertest";
import config from "../../src/constant/config"
import {base64String, hash} from "../../src/helper/cryptography"
import { Server } from "http";

export const useLogin = async(func:(token:string)=>Promise<any>) => {
    const {PROTOCOL, DOMAIN,PORT} = config
    const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/auth/login`

    const password = base64String("Password@123!")
    const payload = {
        email: "chinyan@lifelinelab.io",
        password,
    }
    const res = await superagent.post(url).set("Content-Type","application/json").send(payload)
    const accessToken = res.body.data.token.accessToken;
    await func(accessToken)

}

export const getLogin = async(httpServer:Server) => {
    const res = await supertest(httpServer).post("/v1.0/auth/login").set({"Content-Type": "application/json"}).send({
        email:"lucifer@lifelinelab.io",
        password:hash("Password@123!")
    })
    const TOKEN = JSON.parse(res.text).data.token.accessToken
    const profileID = JSON.parse(res.text).data.profile._id
    return {TOKEN, profileID}
}

export const getOnnoLogin = async(httpServer:Server) => {
    const res = await supertest(httpServer).post("/v1.0/auth/login").set({"Content-Type": "application/json"}).send({
        email:"dev@diolko.com",
        password:hash("Password@123!")
    })
    const TOKEN = JSON.parse(res.text).data.token.accessToken
    const profileID = JSON.parse(res.text).data.profile._id
    return {TOKEN, profileID}
}