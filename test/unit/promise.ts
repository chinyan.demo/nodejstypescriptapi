const sum = (n1:number,n2:number,cb:(ans:number)=>void) => { cb(n1+n2) }

const plus = (n1:number,n2:number):Promise<any> =>{
    return new Promise((res,rej)=>{
        try{
            sum(n1,n2,(ans)=>{return res({res:ans, err:undefined})})
        }catch(error){
            rej({err:error,res:undefined})
        }
    })
}

const main = async() => {
    const result = await plus(2,5)
    console.log(result) 
}

main()