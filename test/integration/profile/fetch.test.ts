import supertest from "supertest";
import getServer from "../../../src/server";
import { getLogin } from "../../login";
import fs from "fs";
import path from "path";
import { Server } from "http";

describe("PROFILE",()=>{
    let TOKEN:string
    let profileID:string
    let httpServer:Server

    beforeAll(async()=>{
        httpServer = await getServer()
        const res = await getLogin(httpServer);
        TOKEN = res.TOKEN
        profileID = res.profileID
    })
    test("fetch profile",async()=>{
        const res = await supertest(httpServer).get(`/v1.0/profile/fetch?profileID=${profileID}`).set({
            "Content-Type": "application/json",
            "Authorization":`Bearer ${TOKEN}`
        }).send()
        const response = JSON.parse(res.text)
        expect(res.status).toBe(200)
        expect(response.data._id).toMatch(/profile/i)
        expect(response.data.name).toMatch(/Lucifer Morningstar/)
        expect(response.data.email).toMatch(/lucifer@lifelinelab.io/)
        expect(response.data.role).toMatch(/MEMBERS/)
        expect(response.data.active).toBe(true)
        const found = fs.readdirSync(path.resolve("./src/helper/swagger/data")).find(dir=>new RegExp(/profile/,"i").test(dir))
        if(!found) fs.mkdirSync(path.resolve("./src/helper/swagger/data/profile"))
        fs.writeFileSync(path.resolve("./src/helper/swagger/data/profile/fetchRes.json"),JSON.stringify(response.data,undefined,2))
    })
})