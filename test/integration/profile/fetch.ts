import { useLogin } from "../../login"
import superagent from "superagent"
import config from "../../../src/constant/config"

useLogin(async token => {
    try{
    const {PROTOCOL, DOMAIN,PORT} = config
    const   profileID = "profiles/f26e92e9-aefb-4c89-a4c4-bcbc7d59fa29"
    const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/profile/fetch?profileID=${profileID}`
    const res = await superagent.get(url)
    .set("Content-Type","application/json")
    .set("Authorization",`Bearer ${token}`).send()
    if(res.status != 200) throw res.text
    if(res.status == 200) console.log(res.text)
    }catch(error){
        console.log(JSON.stringify(error,undefined,2))
    }
})
