import getServer from "../../src/server"
import request from "supertest";

describe('GET / health check', () => {
    test("server up and running", async ()=> {
        const httpServer = await getServer()
        const res = await request(httpServer).get("/");
        const data = JSON.parse(res.text)
        expect(res.statusCode).toEqual(200)
        // expect(data).toEqual({"code":200,"msg":"up and running ..."})
    })
});