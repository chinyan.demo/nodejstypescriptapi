import superagent from "superagent";
import "dotenv/config";
import { base64String } from "../../../src/helper/cryptography";

const testLogin = async () => {
    try {
        const { PROTOCOL, DOMAIN, PORT } = process.env;
        const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/auth/login`;

        const password = base64String("Password@123!");
        const payload = {
            email: "chinyan@lifelinelab.io",
            password,
        };
        const res = await superagent
            .post(url)
            .set("Content-Type", "application/json")
            .send(payload);

        console.log(JSON.stringify(res.body, undefined, 2));
        console.table({ accessToken: res.body.data.token.accessToken });
        return res.body.data.token.accessToken;
    } catch (error) {
        console.log(JSON.stringify(error, undefined, 2));
    }
};

const testCreatePost = async () => {
    try {
        const { PROTOCOL, DOMAIN, PORT } = process.env;
        const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/post/list`;
        const accessToken = await testLogin();
        console.log(accessToken);
        const payload = { _id: "posts/24b98e09-527e-4b19-ac0d-b045ebf83ade" };
        const res = await superagent
            .post(url)
            .set({
                "Content-Type": "application/json",
                Authorization: accessToken,
            })
            .set("accept", "application/json")
            .send(payload);

        console.log(JSON.stringify(res.body, undefined, 2));
    } catch (error) {
        console.log(JSON.stringify(error, undefined, 2));
    }
};
testCreatePost();
