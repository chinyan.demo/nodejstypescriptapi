import superagent from "superagent";
import "dotenv/config";
import { base64String } from "../../../src/helper/cryptography";

const testLogin = async () => {
    try {
        const { PROTOCOL, DOMAIN, PORT } = process.env;
        const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/auth/login`;

        const password = base64String("Password@123!");
        const payload = {
            email: "chinyan@lifelinelab.io",
            password,
        };
        const res = await superagent
            .post(url)
            .set("Content-Type", "application/json")
            .send(payload);

        // console.log(JSON.stringify(res.body, undefined, 2));
        console.table({ accessToken: res.body.data.token.accessToken });
        return res.body.data.token.accessToken;
    } catch (error) {
        console.log(JSON.stringify(error, undefined, 2));
    }
};

const testCreateFriend = async () => {
    try {
        const { PROTOCOL, DOMAIN, PORT } = process.env;
        const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/friend/create`;
        const accessToken = await testLogin();

        const payload = {
            currentID: "profiles/30cee3e5-df86-41b0-a606-3f8779980f55",
            friendID: "profiles/aa4ce041-b019-4050-9a3f-eb9787a5ee61",
        };
        const res = await superagent
            .post(url)
            .set({
                "Content-Type": "application/json",
                Authorization: accessToken,
            })
            .set("accept", "application/json")
            .send(payload);

        console.log(JSON.stringify(res.body, undefined, 2));
    } catch (error) {
        console.log(JSON.stringify(error, undefined, 2));
    }
};
testCreateFriend();
