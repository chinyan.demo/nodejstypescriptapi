import superagent from "superagent";
import "dotenv/config";
import { base64String } from "../../../src/helper/cryptography";

const testLogin = async () => {
    try {
        const { PROTOCOL, DOMAIN, PORT } = process.env;
        const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/auth/login`;

        const password = base64String("Password@123!");
        const payload = {
            email: "chinyan@lifelinelab.io",
            password,
        };
        const res = await superagent
            .post(url)
            .set("Content-Type", "application/json")
            .send(payload);

        return res.body.data.token.accessToken;
    } catch (error) {
        console.log(JSON.stringify(error, undefined, 2));
    }
};

const testDeleteFriend = async () => {
    try {
        const { PROTOCOL, DOMAIN, PORT } = process.env;
        const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/friend/delete`;
        const accessToken = await testLogin();

        const payload = {
            currentID: "profiles/2924541b-f98c-4d98-8860-fc656ca3b516",
            friendID: "profiles/9d2479c6-ee93-46a9-80f9-d2e1cfe02bb6",
        };
        const res = await superagent
            .post(url)
            .set({
                "Content-Type": "application/json",
                Authorization: accessToken,
            })
            .set("accept", "application/json")
            .send(payload);

        console.log(JSON.stringify(res.body, undefined, 2));
    } catch (error) {
        console.log(JSON.stringify(error, undefined, 2));
    }
};
testDeleteFriend();
