import getServer from "../../../src/server"
import supertest from "supertest"
import { hash } from "../../../src/helper/cryptography"
import accountRepo from "../../../src/repository/account"
import profileRepo from "../../../src/repository/profile"
import fs from "fs"
import path from "path"
import { Server } from "http"


const registerPayload = {
    email: "nurul@lifelinelab.io",
    name: "Nurul Ain",
    password: hash("Password@123!"),
    confirmPassword: hash("Password@123!")

}

const loginPayload = {
    email: "nurul@lifelinelab.io",
    password: hash("Password@123!"),
}


describe("TEST AUTH : Test Register and Login Then Clean Up",()=>{
    let httpServer:Server;
    beforeAll(async()=>{  httpServer = await getServer() })
    afterAll(async()=>{
        // Clean Up after test
        const delAcc = await accountRepo.deleteByFilter({email:"nurul@lifelinelab.io"}) 
        const delProfile = await profileRepo.deleteByFilter({email:"nurul@lifelinelab.io"}) 
    })
    test("register user",async()=>{
        const res = await supertest(httpServer).post("/v1.0/auth/register").set({"Content-Type": "application/json"}).send(registerPayload);
        const response = res.body || JSON.parse(res.text);
        expect(res.status).toBe(200)
        expect(response.success).toBe(true)
        expect(response.data._id).toMatch(/profiles/i)
        const found = fs.readdirSync(path.resolve("./src/helper/swagger/data")).find(dir=>new RegExp(/auth/,"i").test(dir))
        if(!found) fs.mkdirSync(path.resolve("./src/helper/swagger/data/auth"))
        fs.writeFileSync(path.resolve("./src/helper/swagger/data/auth/registerRes.json"),JSON.stringify(response.data,undefined,2))
    })
    test("user login",async()=>{
        const res = await supertest(httpServer).post("/v1.0/auth/login").set({"Content-Type": "application/json"}).send(loginPayload);
        const response = res.body || JSON.parse(res.text);
        expect(res.status).toBe(200)
        expect(response.success).toBe(true)
        expect(response.data.profile._id).toMatch(/profiles/i)
        expect(typeof response.data.token.accessToken).toBe("string")
        expect(typeof response.data.token.refreshToken).toBe("string")
        const found = fs.readdirSync(path.resolve("./src/helper/swagger/data")).find(dir=>new RegExp(/auth/,"i").test(dir))
        if(!found) fs.mkdirSync(path.resolve("./src/helper/swagger/data/auth"))
        fs.writeFileSync(path.resolve("./src/helper/swagger/data/auth/loginRes.json"),JSON.stringify(response.data,undefined,2))
        
    })

    test("refresh token",async()=>{
        const res = await supertest(httpServer).post("/v1.0/auth/login").set({"Content-Type": "application/json"}).send({
            email:"lucifer@lifelinelab.io",
            password:hash("Password@123!")
        })
        const refreshToken = JSON.parse(res.text).data.token.accessToken
        
        const refreshTokenPayload = {
            profileID:JSON.parse(res.text).data.profile._id
        }
        const refreshRes= await supertest(httpServer).post("/v1.0/auth/refresh/token").set({
            "Content-Type": "application/json",
            "Authorization":`Bearer ${refreshToken}`
        }).send(refreshTokenPayload);
        const refreshResponse = refreshRes.body || JSON.parse(refreshRes.text);
        expect(res.status).toBe(200)
        expect(refreshResponse.success).toBe(true)
        expect(typeof refreshResponse.data.token.accessToken).toBe("string")
        expect(typeof refreshResponse.data.token.refreshToken).toBe("string")
        const found = fs.readdirSync(path.resolve("./src/helper/swagger/data")).find(dir=>new RegExp(/auth/,"i").test(dir))
        if(!found) fs.mkdirSync(path.resolve("./src/helper/swagger/data/auth"))
        fs.writeFileSync(path.resolve("./src/helper/swagger/data/auth/refreshTokenRes.json"),JSON.stringify(refreshResponse.data,undefined,2))
    })
   
})