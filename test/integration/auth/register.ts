import superagent from "superagent";
import config from "../../../src/constant/config";
import { base64String } from "../../../src/helper/cryptography";

const nameList = {
    hongwai: {
        email: "hongwai@lifelinelab.io",
        name: "Hong Wai",
    },
    chinyan: {
        email: "chinyan@lifelinelab.io",
        name: "Chin Yan",
    },
    adam: {
        email: "adam@lifelinelab.io",
        name: "Adam",
    },
    samuel: {
        email: "samuel@lifelinelab.io",
        name: "Sam",
    },
};

const testRegister = async () => {
    try {
        const { PROTOCOL, DOMAIN, PORT } = config;
        const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/auth/register`;

        const password = base64String("Password@123!");
        const payload = {
            ...nameList.samuel,
            password,
            confirmPassword: password,
        };
        const res = await superagent
            .post(url)
            .set("Content-Type", "application/json")
            .send(payload);

        console.log(JSON.stringify(res.body, undefined, 2));
    } catch (error) {
        console.log(JSON.stringify(error, undefined, 2));
    }
};
testRegister();
