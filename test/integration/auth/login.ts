import superagent from "superagent";
import config from "../../../src/constant/config";
import { base64String } from "../../../src/helper/cryptography";

const testLogin = async () => {
    try {
        const { PROTOCOL, DOMAIN, PORT } = config;
        const url = `${PROTOCOL}://${DOMAIN}:${PORT}/v1.0/auth/login`;

        const password = base64String("Password@123!");
        const payload = {
            email: "chinyan@lifelinelab.io",
            password,
        };
        const res = await superagent
            .post(url)
            .set("Content-Type", "application/json")
            .send(payload);

        console.log(JSON.stringify(res.body, undefined, 2));
        console.table(res.body.data.token.accessToken);
    } catch (error) {
        console.log(JSON.stringify(error, undefined, 2));
    }
};
testLogin();
