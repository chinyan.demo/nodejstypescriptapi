import supertest from "supertest";
import getServer from "../../../src/server";
import { getLogin } from "../../login";
import { COLLECTION } from "../../../src/database/arango/collection.enum";
import conversationRepo from "../../../src/repository/conversation"
import fs from "fs";
import path from "path";
import { Server } from "http";

describe(COLLECTION.CONVERSATIONS,()=>{
    let TOKEN:string
    let profileID:string
    let httpServer:Server;

    beforeAll(async()=>{
        //Setup Global Variable
        httpServer = await getServer()
        const res = await getLogin(httpServer);
        TOKEN = res.TOKEN
        profileID = res.profileID
    })

    afterAll(async()=>{
        await conversationRepo.deleteByFilter({ name:"Logistics Developer" })
    })

    test("Create Conversation",async()=>{
        const payload = {
            type:"group_message",
            profileIDs:[
                "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
                "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
                "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8", 

            ],
            name:"Logistics Developer",
            adminProfileIDs:[
                "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8"
            ]
        }
        const res = await supertest(httpServer).post(`/v1.0/conversation/group/create`).set({
            "Content-Type": "application/json",
            "Authorization":`Bearer ${TOKEN}`
        }).send(payload);
        // console.log(res.text);
        const response = JSON.parse(res.text)
        expect(res.status).toBe(200)
        expect(response.success).toBe(true)
        expect(response.data._id).toMatch(/conversations/i)
        const found = fs.readdirSync(path.resolve("./src/helper/swagger/data")).find(dir=>new RegExp(/conversations/,"i").test(dir))
        if(!found) fs.mkdirSync(path.resolve("./src/helper/swagger/data/conversations"))
        fs.writeFileSync(path.resolve("./src/helper/swagger/data/conversations/createRes.json"),JSON.stringify(response.data,undefined,2))
        
    });
})