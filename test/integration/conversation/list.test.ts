import supertest from "supertest";
import getServer from "../../../src/server";
import { getLogin } from "../../login";
import { COLLECTION } from "../../../src/database/arango/collection.enum";
import fs from "fs";
import path from "path";
import { Server } from "http";

describe(COLLECTION.CONVERSATIONS,()=>{
    let TOKEN:string
    let profileID:string
    let httpServer:Server;

    beforeAll(async()=>{
        //Setup Global Variable
        httpServer = await getServer()
        const res = await getLogin(httpServer)
        TOKEN = res.TOKEN
        profileID = res.profileID
        
    })

    test("list Conversation",async()=>{
        const res = await supertest(httpServer).get(`/v1.0/conversation/list?profileID=${profileID}`).set({
            "Content-Type": "application/json",
            "Authorization":`Bearer ${TOKEN}`
        }).send();

        const response = JSON.parse(res.text)
        expect(res.status).toBe(200)
        expect(response.success).toBe(true)
        expect(response.data.length>=3).toBe(true)
        const found = fs.readdirSync(path.resolve("./src/helper/swagger/data")).find(dir=>new RegExp(/conversations/,"i").test(dir))
        if(!found) fs.mkdirSync(path.resolve("./src/helper/swagger/data/conversations"))
        fs.writeFileSync(path.resolve("./src/helper/swagger/data/conversations/listRes.json"),JSON.stringify(response.data,undefined,2))
        
    });
})