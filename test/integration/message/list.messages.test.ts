import supertest from "supertest";
import getServer from "../../../src/server";
import { getLogin } from "../../login";
import { COLLECTION } from "../../../src/database/arango/collection.enum";
import fs from "fs";
import path from "path";
import { Server } from "http";

describe(COLLECTION.MESSAGES,()=>{
    let TOKEN:string
    let profileID:string
    let httpServer:Server

    beforeAll(async()=>{
        //Setup Global Variable
        httpServer = await getServer()
        const res = await getLogin(httpServer)
        TOKEN = res.TOKEN
        profileID = res.profileID  
    })

    test("test list messages", async()=>{
        const payload ={
            conversationID : `${COLLECTION.CONVERSATIONS}/6b359d1b-9cb3-4cc8-9951-736353928d1a`,
            pagination: {
                offset: 0,
                limit: 100
            }
        }
          
        const res = await supertest(httpServer).post(`/v1.0/message/by/conversationID/list`).set({
            "Content-Type": "application/json",
            "Authorization":`Bearer ${TOKEN}`
        }).send(payload);
        expect(res.statusCode).toBe(200)
        const response = JSON.parse(res.text)
        expect(response.success).toBe(true)
        expect(response.data.length>=2).toBe(true)
        const found = fs.readdirSync(path.resolve("./src/helper/swagger/data")).find(dir=>new RegExp(/messages/,"i").test(dir))
        if(!found) fs.mkdirSync(path.resolve("./src/helper/swagger/data/messages"))
        fs.writeFileSync(path.resolve("./src/helper/swagger/data/messages/listRes.json"),JSON.stringify(response.data,undefined,2))
    })

})