import supertest from "supertest";
import getServer from "../../../src/server";
import { getLogin } from "../../login";
import { COLLECTION } from "../../../src/database/arango/collection.enum";
import messageRepo from "../../../src/repository/message";
import fs from "fs";
import path from "path";
import { Server } from "http";

describe(COLLECTION.MESSAGES,()=>{
    let TOKEN:string
    let profileID:string
    let httpServer:Server

    beforeAll(async()=>{
        //Setup Global Variable
        httpServer = await getServer()
        const res = await getLogin(httpServer)
        TOKEN = res.TOKEN
        profileID = res.profileID  
    })

    afterAll(async()=>{
        await messageRepo.deleteByFilter({ content: "Onno want to have meeting next Friday" })
    })

    test("create message", async()=>{
        const payload = {
            conversationID: `${COLLECTION.CONVERSATIONS}/d762568e-ca8d-42f0-9df1-5a5f3f29756c`,
            content: "Onno want to have meeting next Friday",
            senderProfileID: profileID,
            readBy: [profileID]
        }
        const res = await supertest(httpServer).post("/v1.0/message/create").set({
            "Content-Type": "application/json",
            "Authorization":`Bearer ${TOKEN}`
        }).send(payload);
        expect(res.statusCode).toBe(200)
        const response = JSON.parse(res.text)
        expect(response.success).toBe(true)
        expect(response.data.message._id).toMatch(/messages/i)
        expect(response.data.conversation._id).toMatch(/conversations/i)
        const found = fs.readdirSync(path.resolve("./src/helper/swagger/data")).find(dir=>new RegExp(/messages/,"i").test(dir))
        if(!found) fs.mkdirSync(path.resolve("./src/helper/swagger/data/messages"))
        fs.writeFileSync(path.resolve("./src/helper/swagger/data/messages/createRes.json"),JSON.stringify(response.data,undefined,2))
    })

})