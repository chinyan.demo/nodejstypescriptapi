import supertest from "supertest";
import getServer from "../../../src/server";
import { getOnnoLogin } from "../../login";
import { COLLECTION } from "../../../src/database/arango/collection.enum";
import messageRepo from "../../../src/repository/message";
import fs from "fs";
import path from "path";
import { Server } from "http";

describe(COLLECTION.MESSAGES,()=>{
    let TOKEN:string
    let profileID:string
    let httpServer:Server

    beforeAll(async()=>{
        //Setup Global Variable
        httpServer = await getServer()
        const res = await getOnnoLogin(httpServer)
        TOKEN = res.TOKEN
        profileID = res.profileID  
    })

    afterAll(async()=>{
        await messageRepo.deleteByFilter({ content: "Onno want to have meeting next Friday" })
    })

    test("test update message read by", async()=>{
        const payload = {
            conversationID: `${COLLECTION.CONVERSATIONS}/6b359d1b-9cb3-4cc8-9951-736353928d1a`,
            profileID: profileID
        }
        const res = await supertest(httpServer).put("/v1.0/message/readby/update").set({
            "Content-Type": "application/json",
            "Authorization":`Bearer ${TOKEN}`
        }).send(payload);
        expect(res.statusCode).toBe(200)
        // console.log(res.text)
        const response = JSON.parse(res.text)
        expect(response.success).toBe(true)
        const found = fs.readdirSync(path.resolve("./src/helper/swagger/data")).find(dir=>new RegExp(/messages/,"i").test(dir))
        if(!found) fs.mkdirSync(path.resolve("./src/helper/swagger/data/messages"))
        fs.writeFileSync(path.resolve("./src/helper/swagger/data/messages/updateRes.json"),JSON.stringify(response.data,undefined,2))
    })
})