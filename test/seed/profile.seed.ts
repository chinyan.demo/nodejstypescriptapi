import { COLLECTION } from "../../src/database/arango/collection.enum";

const profileSeed:{collection:string,documents:Array<{[key:string]:any}>} = {
    collection:COLLECTION.PROFILES,
    documents:[  {
        "_key": "faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "_id": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "_rev": "_ggqLBsO---",
        "name": "Onno",
        "email": "dev@diolko.com",
        "role": "MEMBERS",
        "active": true,
        "createdBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "createdAt": "2023-08-25T04:56:30.669Z",
        "updatedBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "updatedAt": "2023-08-25T04:56:30.670Z"
      },
      {
        "_key": "4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "_id": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "_rev": "_ggqNfre---",
        "name": "Lucifer Morningstar",
        "email": "lucifer@lifelinelab.io",
        "role": "MEMBERS",
        "active": true,
        "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "createdAt": "2023-08-25T04:59:12.433Z",
        "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "updatedAt": "2023-08-25T04:59:12.433Z"
      },{
        "_key": "f49442c4-04b1-4984-ba96-0cb0e99f5c4d",
        "_id": "profiles/f49442c4-04b1-4984-ba96-0cb0e99f5c4d",
        "_rev": "_gisbuv-_a5",
        "name": "Hong Wai",
        "email": "hongwai@lifelinelab.io",
        "role": "MEMBERS",
        "active": true,
        "createdBy": "profiles/f49442c4-04b1-4984-ba96-0cb0e99f5c4d",
        "createdAt": "2023-08-31T06:21:13.232Z",
        "updatedBy": "profiles/f49442c4-04b1-4984-ba96-0cb0e99f5c4d",
        "updatedAt": "2023-08-31T06:21:13.232Z"
      },
      {
        "_key": "fd1b694b-8521-450f-9e3e-fe0ccea49e78",
        "_id": "profiles/fd1b694b-8521-450f-9e3e-fe0ccea49e78",
        "_rev": "_gisbuv-_a6",
        "name": "Sam",
        "email": "samuel@lifelinelab.io",
        "role": "MEMBERS",
        "active": true,
        "createdBy": "profiles/fd1b694b-8521-450f-9e3e-fe0ccea49e78",
        "createdAt": "2023-08-31T06:21:13.239Z",
        "updatedBy": "profiles/fd1b694b-8521-450f-9e3e-fe0ccea49e78",
        "updatedAt": "2023-08-31T06:21:13.239Z"
      },
      {
        "_key": "00735ebc-befe-4e58-87f8-7333655e4d09",
        "_id": "profiles/00735ebc-befe-4e58-87f8-7333655e4d09",
        "_rev": "_gisbuv-_a8",
        "name": "Chin Yan",
        "email": "chinyan@lifelinelab.io",
        "role": "MEMBERS",
        "active": true,
        "createdBy": "profiles/00735ebc-befe-4e58-87f8-7333655e4d09",
        "createdAt": "2023-08-31T06:21:13.235Z",
        "updatedBy": "profiles/00735ebc-befe-4e58-87f8-7333655e4d09",
        "updatedAt": "2023-08-31T06:21:13.235Z"
      },
      {
        "_key": "e8e81b1a-6643-41f7-8492-fc8b4575fff8",
        "_id": "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",
        "_rev": "_gisbuv-_a9",
        "name": "Adam",
        "email": "adam@lifelinelab.io",
        "role": "MEMBERS",
        "active": true,
        "createdBy": "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",
        "createdAt": "2023-08-31T06:21:13.237Z",
        "updatedBy": "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",
        "updatedAt": "2023-08-31T06:21:13.237Z"
      }]
}

export default profileSeed;