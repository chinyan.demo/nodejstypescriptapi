import { COLLECTION } from "../../src/database/arango/collection.enum";

const messageSeed:{collection:string,documents:Array<{[key:string]:any}>} = {
    collection: COLLECTION.MESSAGES,
    documents: [ {"_key": "eb2b56b4-7b9b-4291-8b8a-3845b3d4c6c7",
    "_id": "messages/eb2b56b4-7b9b-4291-8b8a-3845b3d4c6c7",
    "_rev": "_gi7_eM6---",
    "conversationID": `${COLLECTION.CONVERSATIONS}/6b359d1b-9cb3-4cc8-9951-736353928d1a`,
    "content": "shall we schedule a meeting this Friday",
    "senderProfileID": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "readBy": [
      "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04"
    ],
    "active": true,
    "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "createdAt": "2023-09-01T05:40:07.533Z",
    "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "updatedAt": "2023-09-01T05:40:07.533Z"
  }, {
    "_key": "eb2b56b4-7b9b-4291-8b8a-3845b3d4c6c7",
    "_id": "messages/eb2b56b4-7b9b-4291-8b8a-3845b3d4c6c7",
    "_rev": "_gpXP8ZC---",
    "conversationID": `${COLLECTION.CONVERSATIONS}/6b359d1b-9cb3-4cc8-9951-736353928d1a`,
    "content": "shall we schedule a meeting this Friday",
    "senderProfileID": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "readBy": [
      "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
      "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca"
    ],
    "active": true,
    "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "createdAt": "2023-09-01T05:40:07.533Z",
    "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "updatedAt": "2023-09-01T05:40:07.533Z"
  },
  {
    "_key": "eb2b56b4-7b9b-4291-8b8a-3845b3d4c6c8",
    "_id": "messages/eb2b56b4-7b9b-4291-8b8a-3845b3d4c6c8",
    "_rev": "_gpXP8ZC--_",
    "conversationID": `${COLLECTION.CONVERSATIONS}/6b359d1b-9cb3-4cc8-9951-736353928d1a`,
    "content": "Friday 3pm in office",
    "senderProfileID": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "readBy": [
      "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
      "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca"
    ],
    "active": true,
    "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "createdAt": "2023-09-01T05:40:07.533Z",
    "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "updatedAt": "2023-09-01T05:40:07.533Z"
  },
  {
    "_key": "24f9ee86-488d-4b57-a139-280fd2a01490",
    "_id": "messages/24f9ee86-488d-4b57-a139-280fd2a01490",
    "_rev": "_gpXP8ZC--A",
    "conversationID": `${COLLECTION.CONVERSATIONS}/6b359d1b-9cb3-4cc8-9951-736353928d1a`,
    "content": "hi",
    "senderProfileID": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "readBy": [
      "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
      "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca"
    ],
    "active": true,
    "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "createdAt": "2023-09-11T08:12:14.467Z",
    "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
    "updatedAt": "2023-09-11T08:12:14.467Z"
  },]
}

export default messageSeed;