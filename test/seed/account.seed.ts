import { COLLECTION } from "../../src/database/arango/collection.enum";

const accountSeed:{collection:string,documents:Array<{[key:string]:any}>} = {
    collection:COLLECTION.ACCOUNTS,
    documents:[{
        "_key": "946c8f09-9cee-4320-9053-3306a6806bcf",
        "_id": "accounts/946c8f09-9cee-4320-9053-3306a6806bcf",
        "_rev": "_ggqLBsy---",
        "email": "dev@diolko.com",
        "password": {
          "hash": "841b6ce609e6cb3a3ecd99cdfb2c6056a4e2d2a74702f305467f4cef0fc7a428",
          "salt": "MDA1OQ=="
        },
        "profileID": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "active": true,
        "createdBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "createdAt": "2023-08-25T04:56:30.670Z",
        "updatedBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "updatedAt": "2023-08-25T04:56:30.670Z"
      },
      {
        "_key": "e47d6e3b-c51e-4953-9dc8-7f8aee84ecb8",
        "_id": "accounts/e47d6e3b-c51e-4953-9dc8-7f8aee84ecb8",
        "_rev": "_ggqNfta---",
        "email": "lucifer@lifelinelab.io",
        "password": {
          "hash": "c658bdeb1fbd5c7295b0af6cdcaf528fe39c683493aa2634c94e4be206fd44b9",
          "salt": "MDk2MA=="
        },
        "profileID": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "active": true,
        "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "createdAt": "2023-08-25T04:59:12.433Z",
        "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "updatedAt": "2023-08-25T04:59:12.433Z"
      }, {
        "_key": "4f5e6e95-a1d9-4e99-955e-160b7da938aa",
        "_id": "accounts/4f5e6e95-a1d9-4e99-955e-160b7da938aa",
        "_rev": "_gisbuv-_a7",
        "email": "hongwai@lifelinelab.io",
        "password": {
          "hash": "69e2afecaae0e1af53c1cf3fccd6c32fa8e5491947f158241174e6e00006f7ae",
          "salt": "MDcyMw=="
        },
        "profileID": "profiles/f49442c4-04b1-4984-ba96-0cb0e99f5c4d",
        "active": true,
        "createdBy": "profiles/f49442c4-04b1-4984-ba96-0cb0e99f5c4d",
        "createdAt": "2023-08-31T06:21:13.232Z",
        "updatedBy": "profiles/f49442c4-04b1-4984-ba96-0cb0e99f5c4d",
        "updatedAt": "2023-08-31T06:21:13.232Z"
      },
      {
        "_key": "0612a1e5-87de-4a8f-952c-83eab8b7d77b",
        "_id": "accounts/0612a1e5-87de-4a8f-952c-83eab8b7d77b",
        "_rev": "_gisbuv-_b-",
        "email": "samuel@lifelinelab.io",
        "password": {
          "hash": "888c78e627ae4dc64beb585abcca1eb51a7c5383b99d2ce6b080a8db85381aa5",
          "salt": "MDczMQ=="
        },
        "profileID": "profiles/fd1b694b-8521-450f-9e3e-fe0ccea49e78",
        "active": true,
        "createdBy": "profiles/fd1b694b-8521-450f-9e3e-fe0ccea49e78",
        "createdAt": "2023-08-31T06:21:13.239Z",
        "updatedBy": "profiles/fd1b694b-8521-450f-9e3e-fe0ccea49e78",
        "updatedAt": "2023-08-31T06:21:13.239Z"
      },
      {
        "_key": "0405a6aa-5313-49cb-b078-ec3bf215bfca",
        "_id": "accounts/0405a6aa-5313-49cb-b078-ec3bf215bfca",
        "_rev": "_gisbuv-_b_",
        "email": "chinyan@lifelinelab.io",
        "password": {
          "hash": "064d40277899baa8cfe7792b13ce21d882a23d1a1e3f15175f6cb08d73ee21ca",
          "salt": "MDc2Mg=="
        },
        "profileID": "profiles/00735ebc-befe-4e58-87f8-7333655e4d09",
        "active": true,
        "createdBy": "profiles/00735ebc-befe-4e58-87f8-7333655e4d09",
        "createdAt": "2023-08-31T06:21:13.235Z",
        "updatedBy": "profiles/00735ebc-befe-4e58-87f8-7333655e4d09",
        "updatedAt": "2023-08-31T06:21:13.235Z"
      },
      {
        "_key": "841040bc-6a17-4b43-82f2-9e9bd6863396",
        "_id": "accounts/841040bc-6a17-4b43-82f2-9e9bd6863396",
        "_rev": "_gisbuv-_bA",
        "email": "adam@lifelinelab.io",
        "password": {
          "hash": "0daa8c62033a676a0fbb37a467cac493bae875bf1210726417a283eadc5e1351",
          "salt": "MDQ2MA=="
        },
        "profileID": "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",
        "active": true,
        "createdBy": "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",
        "createdAt": "2023-08-31T06:21:13.237Z",
        "updatedBy": "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",
        "updatedAt": "2023-08-31T06:21:13.237Z"
      }]
}

export default accountSeed;