import {  EDGE_COLLECTION } from "../../src/database/arango/collection.enum";

const friendSeed:{collection:string,documents:Array<{[key:string]:any}>} = {
    collection:EDGE_COLLECTION.FRIENDS,
    documents:[{
        "_id":`${EDGE_COLLECTION.FRIENDS}/36b435fa-1ee9-4584-b734-702aed2a7ce2`,
        "_key":"36b435fa-1ee9-4584-b734-702aed2a7ce2",
        "_from":"profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "_to":"profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "createdBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "createdAt": "2023-08-25T04:56:30.670Z",
        "updatedBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "updatedAt": "2023-08-25T04:56:30.670Z"
    },{
        "_id":`${EDGE_COLLECTION.FRIENDS}/d1c44970-160c-484a-af2c-a7058e7e49e6`,
        "_key":"d1c44970-160c-484a-af2c-a7058e7e49e6",
        "_from":"profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "_to":"profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "createdBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "createdAt": "2023-08-25T04:56:30.670Z",
        "updatedBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
        "updatedAt": "2023-08-25T04:56:30.670Z"
    },{ 
        "_id":`${EDGE_COLLECTION.FRIENDS}/c17f2744-d5cf-4267-98cb-401d34655ebc`,
        "_key":"c17f2744-d5cf-4267-98cb-401d34655ebc" ,
        "_from":"profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "_to":"profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",
        "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "createdAt": "2023-08-25T04:56:30.670Z",
        "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "updatedAt": "2023-08-25T04:56:30.670Z"
    },{  
        "_id":`${EDGE_COLLECTION.FRIENDS}/7572c8c8-200f-4d8e-b2aa-4d5a67c0d6e1`,
        "_key":"7572c8c8-200f-4d8e-b2aa-4d5a67c0d6e1",
        "_from":"profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",
        "_to":"profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "createdAt": "2023-08-25T04:56:30.670Z",
        "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
        "updatedAt": "2023-08-25T04:56:30.670Z"
    }]
}

export default friendSeed;