import { COLLECTION } from "../../src/database/arango/collection.enum"

const conversationSeed = {
    collection:COLLECTION.CONVERSATIONS,
    documents:[
        {
            "_id":`${COLLECTION.CONVERSATIONS}/6b359d1b-9cb3-4cc8-9951-736353928d1a`,
            "_key":"6b359d1b-9cb3-4cc8-9951-736353928d1a",
            "type": "private_message",
            "profileIDs": ["profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca","profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04"],
            "createdBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
            "createdAt": "2023-08-25T04:56:30.670Z",
            "updatedBy": "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
            "updatedAt": "2023-08-25T04:56:30.670Z"
        },{
            "_id":`${COLLECTION.CONVERSATIONS}/9c06b07d-8bcf-4684-94f1-5ccf96831f97`,
            "_key":"9c06b07d-8bcf-4684-94f1-5ccf96831f97",
            "type": "private_message",
            "profileIDs": ["profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8","profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04"],
            "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
            "createdAt": "2023-08-25T04:56:30.670Z",
            "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
            "updatedAt": "2023-08-25T04:56:30.670Z"
        },{
            "_id": `${COLLECTION.CONVERSATIONS}/d762568e-ca8d-42f0-9df1-5a5f3f29756c`,
            "_key": "d762568e-ca8d-42f0-9df1-5a5f3f29756c",
            "_rev": "_gisbuv-AgU",
            "type": "group_message",
            "profileIDs": [
              "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
              "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
              "profiles/e8e81b1a-6643-41f7-8492-fc8b4575fff8",
              "profiles/f49442c4-04b1-4984-ba96-0cb0e99f5c4d"
            ],
            "name": "Logistics Diolko",
            "adminProfileIDs": [
              "profiles/faaa8385-cd17-4f28-9b6d-17461471b1ca",
              "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04"
            ],
            "active": true,
            "createdBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
            "createdAt": "2023-08-31T07:46:38.586Z",
            "updatedBy": "profiles/4f872a2d-3100-419b-83c4-1636b4bf6b04",
            "updatedAt": "2023-08-31T07:46:38.586Z"
          }
    ]
}

export default conversationSeed