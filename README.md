# Typescript KOA API
#### - Please rename sample.env file as .env file to run. Because .env not suppose to goes to git repo

## Instructions
> - setup project, install dependency.
>```bash
>yarn 
>```

> - running regression test
>```bash
>yarn test
>```

> - Run starting dev server with hot reload without transpile
>```bash
> yarn dev
>```

> - Transpile to javascript, a dist file will be create
>```bash
>yarn build
>```

> - Running a built source code
>```bash
>yarn start
>```

> - Running cluster mode 
>```bash
>yarn start cluster
>```

# Dockerize
> - Start Database
>```bash
> docker-compose up -d
>```




## Notes
Swagger link is shown below.

[Swagger](http://localhost:3333/swagger)



