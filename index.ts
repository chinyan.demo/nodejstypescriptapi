import getServer, { startServer, startServerWithCluster } from "./src/server"
import cluster, {Worker} from "cluster"
import {cpus} from "os"
import { argv } from "process";
import {setupMaster} from "@socket.io/sticky";
import { setupPrimary } from "@socket.io/cluster-adapter";

const isCluster = argv.find((argument:string)=>argument.trim()=="cluster") == "cluster"

if(isCluster){

    if(cluster.isPrimary){
        void (async()=>{  
            const httpServer = await getServer() 
            setupMaster(httpServer, {
                loadBalancingMethod: "least-connection", // either "random", "round-robin" or "least-connection"
            });
            setupPrimary()

            cpus().forEach(() =>{ cluster.fork()});

            cluster.setupPrimary({
                serialization: "advanced",
            });

            cluster.on('online', (worker:Worker) => console.log(`Worker ${worker.process.pid} is online`))
            cluster.on('exit', (worker:Worker, exitCode) => {
                console.log(`Worker ${worker.process.pid} exited with code ${exitCode}`)
                console.log(`Starting a new worker`)
                cluster.fork()
            })
        })() 

    }else if(cluster.isWorker){
        void (async()=>{await startServerWithCluster(); })()
    }

}else{
   void (async()=>{await startServer(); })()
}
